(function( $ ) {
	'use strict';

	$(function() {

		// Grab the wrapper for the Navigation Tabs
		var navTabs = $( '#authors-commentary-navigation').children( '.nav-tab-wrapper' ),
			tabIndex = null;

		/* Whenever each of the navigation tabs is clicked, check to see if it has the 'nav-tab-active'
		 * class name. If not, then mark it as active; otherwise, don't do anything (as it's already
		 * marked as active.
		 *
		 * Next, when a new tab is marked as active, the corresponding child view needs to be marked
		 * as visible. We do this by toggling the 'hidden' class attribute of the corresponding variables.
		 */
		navTabs.children().each(function() {

			$( this ).on( 'click', function( evt ) {

				evt.preventDefault();

				// If this tab is not active...
				if ( ! $( this ).hasClass( 'nav-tab-active' ) ) {

					// Unmark the current tab and mark the new one as active
					$( this ).siblings( '.nav-tab-active' ).removeClass( 'nav-tab-active' );
					$( this ).addClass( 'nav-tab-active' );

					// Save the index of the tab that's just been marked as active. It will be 0 - 3.
					tabIndex = $( this ).index();

					// Hide the old active content
					$( '#authors-commentary-navigation' )
						.children( 'div:not( .inside.hidden )' )
						.addClass( 'hidden' );

					$( '#authors-commentary-navigation' )
						.children( 'div:nth-child(' + ( tabIndex ) + ')' )
						.addClass( 'hidden' );

					// And display the new content
					$( '#authors-commentary-navigation' )
						.children( 'div:nth-child( ' + ( tabIndex + 2 ) + ')' )
						.removeClass( 'hidden' );


				}


			});
		});
		
		//
		// Pixel fields
		//
		
		jQuery( '.regular-text.pixel-field' ).after( '<span class="pixel-field-label">px</span>' );
		
		var currentTab;
		var reduxFooter = $( '#redux-footer' );
		
		//
		// Redux Quick Nav
		//
		
		$( '.redux-group-tab-link-a' ).on( 'click', function() {
			addReduxQuickNav( $(this) );
		});
		
		// On tab open
		
		if ( $( '.redux-group-tab-link-li.active' ).length > 0 ) {
			addReduxQuickNav( $( '.redux-group-tab-link-li.active' ).children( 'a' ) );
		}
		
		function addReduxQuickNav( $navElement ) {
		
			$( '#redux-quick-nav' ).remove();
			
			var $this = $navElement;
			var $parent = $this.closest( 'li' );
			var tabID = $this.data( 'key' );
			var sections = [];
			
			// Check if clicked menu parent nav element has subsections
			
			if ( $parent.hasClass( 'hasSubSections' ) ) {
				tabID++;
			}
			
			currentTab = tabID;
			
			// If has then need to add
			
			var tabSelector = '#' + tabID + '_section_group';
			var $content = $( tabSelector );
			
			if ( $content.find( '.redux-section-indent-start' ).length > 0 ) { // has subsections
				
				// Check if main section has any content, if yes then include it in nav
				
				sections[ $content.attr( 'id' ) ] = $content.children( 'h2' ).text();
				
				$content.find( '.redux-section-indent-start' ).each( function() { // Add other subsections
					if ( $(this).attr( 'id' ) ) {
						sections[ $(this).attr( 'id' ) ] = $(this).children( 'h3' ).text();
					}
				});
				
				// Get titles
				
				var $reduxNav = '<ul id="redux-quick-nav"><li><span>Jump to:</span></li>';
				
				for ( var key in sections ) {
					$reduxNav += '<li><a href="#' + key + '">' + sections[key] + '</a></li>';
				}
				
				reduxFooter.prepend( $reduxNav );
				
			}
			
		}
		
		// Redux quick nav smooth scroll
		
		$( '#redux-footer').delegate( '#redux-quick-nav a', 'click', function( event ) {
			
			jQuery('html,body').stop().animate({ 
				scrollTop: jQuery( $(this).attr('href') ).offset().top - 32 + "px"
			}, 400);
			
			event.preventDefault();
		
		});
		
		//
		
		$(document).on( "vc-full-width-row", function( event ) {
		
		});
		
		//$(window).on( 'load', function() {
		
			var $addTemplateBtn = $( '<div id="vntd-vc-add-template"><i class="vc-composer-icon vc-c-icon-add_template"></i> Add Template</div>' );
			
			var $vcAdd = $( '#vc_not-empty-add-element' );
			
			$vcAdd.after( $addTemplateBtn );
			
			$vcAdd.append( $vcAdd.attr( 'title' ) );
		//});
	});

})( jQuery );