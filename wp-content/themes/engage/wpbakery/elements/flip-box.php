<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/* Call to action
 * @since 4.5
 */
 
require_once vc_path_dir( 'CONFIG_DIR', 'content/vc-custom-heading-element.php' );

$params = array_merge( array(
	array(
		'type' => 'attach_image',
		'heading' => __( 'Background Image', 'engage' ),
		'param_name' => 'image',
		'value' => '',
		'description' => __( 'Select image from media library.', 'engage' ),
		'admin_label' => true,
	),
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => esc_html__( "Background Image Overlay", 'engage' ),
		"param_name" => "bg_overlay",
		"value" => $bg_overlay_arr,
		'dependency' => array(
			'element' => 'image',
			'not_empty' => true,
		),
		"description" => esc_html__( "Set a background overlay to darken or lighten the background image and improve the text visibility.", "engage" ) 
	),
	array(
		'type' => 'dropdown',
		'heading' => __( 'Background Color', 'engage' ),
		'param_name' => 'main_bg_color',
		'value' => array(
			esc_html__( "Theme Defaults", 'engage' ) => '',
			esc_html__( "Accent Color", 'engage' ) => 'accent',
			esc_html__( "Accent Color 2", 'engage' ) => 'accent-2',
			esc_html__( "Accent Color 3", 'engage' ) => 'accent-3',
			esc_html__( "Predefined Gradient 1", 'engage' ) => 'gradient-1',
			esc_html__( "Predefined Gradient 2", 'engage' ) => 'gradient-2',
			esc_html__( "Predefined Background 1", 'engage' ) => '1',
			esc_html__( "Predefined Background 2", 'engage' ) => '2',
			esc_html__( "Custom", 'engage' ) => 'custom',
		),
		'description' => __( 'Select the box background color.', 'engage' ),
	),
	array(
		'type' => 'colorpicker',
		'heading' => __( 'Custom Background Color', 'engage' ),
		'param_name' => 'main_bg_color_custom',
		'description' => __( 'Select a custom box background color.', 'engage' ),
		'dependency' => array(
			'element' => 'main_bg_color',
			'value' => array( 'custom' ),
		),
	),
	array(
		'type' => 'textfield',
		'heading' => __( 'Primary title', 'engage' ),
		'admin_label' => true,
		'param_name' => 'primary_title',
		'value' => __( 'Hover Box Element', 'engage' ),
		'description' => __( 'Enter text for heading line.', 'engage' ),
		'edit_field_class' => 'vc_col-sm-9',
	),
), 
//$h2_custom_heading, 
array(
	array(
		'type' => 'textfield',
		'heading' => __( 'Hover title', 'engage' ),
		'param_name' => 'hover_title',
		'value' => 'Hover Box Element',
		'description' => __( 'Hover Box Element', 'engage' ),
		'group' => __( 'Hover Block', 'engage' ),
		'edit_field_class' => 'vc_col-sm-9',
	),
	array(
		'type' => 'dropdown',
		'heading' => __( 'Hover title alignment', 'engage' ),
		'param_name' => 'hover_align',
		'value' => getVcShared( 'text align' ),
		'std' => 'center',
		'group' => __( 'Hover Block', 'engage' ),
		'description' => __( 'Select text alignment for hovered title.', 'engage' ),
	),
	array(
		'type' => 'textarea_html',
		'heading' => __( 'Hover text', 'engage' ),
		'param_name' => 'content',
		'value' => __( 'Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'engage' ),
		'group' => __( 'Hover Block', 'engage' ),
		'description' => __( 'Hover part text.', 'engage' ),
	),
),
array(
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => esc_html__( "Front Box Color Scheme", "engage" ),
		"param_name" => "color_scheme",
		"value" => array(
			esc_html__( "Theme Defaults", 'engage' ) => "",
			esc_html__( "White Scheme", 'engage' ) => "white",
			esc_html__( "Dark Scheme", 'engage' ) => "dark"
		),
		"description" => esc_html__( "White Scheme - all text styled to white color, recommended for dark backgrounds.", "engage" ),
		"group" => esc_html__( "Front Styling", 'engage' )
	),
	array(
		"type" => "colorpicker",
		"heading" => esc_html__( "Primary title color", "engage" ),
		"param_name" => "primary_color",
		"class" => "hidden-label",
		"description" => esc_html__( "Choose your primary title font color.", "engage" ),
	),
	array(
		'type' => 'textfield',
		'heading' => __( 'Heading Font Size', 'engage' ),
		'param_name' => 'primary_fs',
		'value' => '',
		'description' => __( 'Enter the Primary Title font size in pixels, like: 16px or 20px', 'engage' ),
		'group' => __( 'Front Styling', 'engage' ),
	),
	array(
		'type' => 'textfield',
		'heading' => __( 'Heading Line Height', 'engage' ),
		'param_name' => 'primary_lh',
		'value' => '',
		'description' => __( 'Enter the Primary Title line height in pixels, like: 16px or 20px', 'engage' ),
		'group' => __( 'Front Styling', 'engage' ),
	),
),
array(
	array(
		"type" => "dropdown",
		"class" => "",
		"heading" => esc_html__( "Hover State Color Scheme", "engage" ),
		"param_name" => "hover_color_scheme",
		"value" => array(
			esc_html__( "Theme Defaults", 'engage' ) => "",
			esc_html__( "White Scheme", 'engage' ) => "white",
			esc_html__( "Dark Scheme", 'engage' ) => "dark"
		),
		"description" => esc_html__( "White Scheme - all text styled to white color, recommended for dark backgrounds.", "engage" ),
		"group" => esc_html__( "Hover Styling", 'engage' )
	),
	array(
		'type' => 'textfield',
		'heading' => __( 'Heading Font Size', 'engage' ),
		'param_name' => 'hover_fs',
		'value' => '',
		'description' => __( 'Enter the Hover Title font size in pixels, like: 16px or 20px', 'engage' ),
		'group' => __( 'Hover Styling', 'engage' ),
	),
	array(
		'type' => 'textfield',
		'heading' => __( 'Heading Line Height', 'engage' ),
		'param_name' => 'hover_lh',
		'value' => '',
		'description' => __( 'Enter the Hover Title line height in pixels, like: 16px or 20px', 'engage' ),
		'group' => __( 'Hover Styling', 'engage' ),
	),
	array(
		"type" => "colorpicker",
		"heading" => esc_html__( "Hover title color", "engage" ),
		"param_name" => "hover_color",
		"class" => "hidden-label",
		"description" => esc_html__( "Choose your hover title font color.", "engage" ),
		'group' => __( 'Hover Styling', 'engage' ),
	),
),
array(
	array(
		'type' => 'dropdown',
		'heading' => __( 'Shape', 'engage' ),
		'param_name' => 'shape',
		'std' => 'rounded',
		'value' => array(
			__( 'Theme Defaults', 'engage' ) => '',
			__( 'Square', 'engage' ) => 'square',
			__( 'Rounded', 'engage' ) => 'rounded',
			__( 'Round', 'engage' ) => 'round',
		),
		'description' => __( 'Select block shape.', 'engage' ),
	),
	array(
		'type' => 'dropdown',
		'heading' => __( 'Hover Background Color', 'engage' ),
		'param_name' => 'hover_background_color',
		'value' => array(
			esc_html__( "Theme Defaults", 'engage' ) => '',
			esc_html__( "Accent Color", 'engage' ) => 'accent',
			esc_html__( "Accent Color 2", 'engage' ) => 'accent-2',
			esc_html__( "Accent Color 3", 'engage' ) => 'accent-3',
			esc_html__( "Predefined Gradient 1", 'engage' ) => 'gradient-1',
			esc_html__( "Predefined Gradient 2", 'engage' ) => 'gradient-2',
			esc_html__( "Predefined Background 1", 'engage' ) => '1',
			esc_html__( "Predefined Background 2", 'engage' ) => '2',
			esc_html__( "Custom", 'engage' ) => 'custom',
		),
		'description' => __( 'Select color schema.', 'engage' ),
		'group' => __( 'Hover Block', 'engage' ),
	),
	array(
		'type' => 'colorpicker',
		'heading' => __( 'Background color', 'engage' ),
		'param_name' => 'hover_custom_background',
		'description' => __( 'Select custom background color.', 'engage' ),
		'group' => __( 'Hover Block', 'engage' ),
		'dependency' => array(
			'element' => 'hover_background_color',
			'value' => array( 'custom' ),
		),
		'edit_field_class' => 'vc_col-sm-6',
	),
	array(
		'type' => 'dropdown',
		'heading' => __( 'Width', 'engage' ),
		'param_name' => 'el_width',
		'value' => array(
			'100%' => '100',
			'90%' => '90',
			'80%' => '80',
			'70%' => '70',
			'60%' => '60',
			'50%' => '50',
			'40%' => '40',
			'30%' => '30',
			'20%' => '20',
			'10%' => '10',
		),
		'description' => __( 'Select block width (percentage).', 'engage' ),
	),
	array(
		'type' => 'dropdown',
		'heading' => __( 'Alignment', 'engage' ),
		'param_name' => 'align',
		'description' => __( 'Select block alignment.', 'engage' ),
		'value' => array(
			__( 'Left', 'engage' ) => 'left',
			__( 'Right', 'engage' ) => 'right',
			__( 'Center', 'engage' ) => 'center',
		),
		'std' => 'center',
	),
	array(
		"type" => "checkbox",
		"heading" => esc_html__( "Add Icon?", "engage" ),
		"param_name" => "add_icon",
		"class" => "hidden-label",
		"value" => array(
			esc_html__( "Yes", "engage" ) => "true",
		),
		"description" => esc_html__( "Display icon above the special heading.", "engage" ) 
	),
), 
engage_vc_icon_params( 'add_icon' ),
array(
	array(
		'type' => 'checkbox',
		'heading' => __( 'Reverse blocks', 'engage' ),
		'param_name' => 'reverse',
		'description' => __( 'Reverse hover and primary block.', 'engage' ),
	),
	vc_map_add_css_animation(),
	array(
		'type' => 'el_id',
		'heading' => __( 'Element ID', 'engage' ),
		'param_name' => 'el_id',
		'description' => sprintf( __( 'Enter element ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'engage' ), 'http://www.w3schools.com/tags/att_global_id.asp' ),
	),
	array(
		'type' => 'textfield',
		'heading' => __( 'Extra class name', 'engage' ),
		'param_name' => 'el_class',
		'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'engage' ),
	),
	array(
		'type' => 'css_editor',
		'heading' => __( 'CSS box', 'engage' ),
		'param_name' => 'css',
		'group' => __( 'Design Options', 'engage' ),
	),
) );

return array(
	'name' => esc_html__( 'Flip Box', 'engage' ),
	'base' => 'engage_flip_box',
	'icon' => 'vc_icon-vc-hoverbox',
	'category' => array( esc_html__( 'Content', 'engage' ) ),
	'description' => __( 'Animated flip box with image and text.', 'engage' ),
	'params' => $params,
);