<?php

if ( comments_open() || get_comments_number() ) {	
	
?>
	<h5 class="comments-heading"><?php comments_number('0 ' . engage_translate( 'comments' ), '1 '  . engage_translate( 'comment' ), '% ' . engage_translate( 'comments' ) ); ?></h5>
    <div id="comments" class="single-blog-post-comments">
		
		<ul class="comments">
		<?php 
			wp_list_comments( array(
				'callback' => 'engage_comment',
				'type' => 'all',
			) );
		?>				
		</ul>
		
		<div class="pagination">
		
			<div class="pagination-inner">
			<?php
			 
			paginate_comments_links(array(
				'prev_text' => '',
				'next_text' => ''
			)); 
			
			?>
			</div>
			
		</div>	
	
	</div>		
	
	<?php } // Comments list end ?>		
	
	<?php if ( comments_open() ) { ?>
	
	<div class="leave-comment">
		
		<h5 class="comments-heading"><?php echo engage_translate( 'leave-comment' ); ?></h5>
		
		<div class="comment-form">
			
		<?php 
		
		$args = array(
			 'title_reply'	=> '',
			 
			 
			 'fields' => apply_filters( 'comment_form_default_fields', array(
			 
			 	'author' 	=> '<div class="row comment-name-email"><div class="col-md-6"><input id="author" name="author" placeholder="' . engage_translate( 'name' ) . '" type="text" required="required" class="input-lg form-control"></div>',
			 	
			 	'email' 	=> '<div class="col-md-6"><input id="email" name="email" type="email" placeholder="' . engage_translate( 'email' ) . '" required="required" class="input-lg form-control"></div></div>',
			   )),
			   
			   'comment_field' =>  '<div class="comment-form-text"><textarea name="comment" id="comment" placeholder="' . engage_translate( 'comment' ) . '..." class="form-control" rows="10" aria-required="true"></textarea></div>',
		);
					
		comment_form($args); 		
		
		?>			   

		</div>
		
	</div>
	
	<?php } ?>