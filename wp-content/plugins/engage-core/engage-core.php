<?php

	/*
	
	Plugin Name: 	Engage Core
	Plugin URI: 	http://themeforest.net/user/Veented
	Description: 	Core functionalities for Engage Theme.
	Version: 		2.8.1
	Author: 		Veented
	Author URI: 	http://themeforest.net/user/Veented
	License: 		GPL2
	
	*/

	// Redux Extensions
	
	$theme = wp_get_theme();
	
	if ( 'Engage' == $theme->name || 'Engage' == $theme->parent_theme ) {
	
		define( 'ENGAGE_CORE_PATH', dirname( __FILE__ ) . '/' );
		define( 'ENGAGE_CORE_URI' , plugin_dir_url( __FILE__ ) );
        
	   	// Load Custom Post Types
	
	   	require_once( 'custom-post-types/portfolio/portfolio-functions.php' ); 		// Portfolio Post Type
	   	require_once( 'custom-post-types/team/team-functions.php' ); 					// Team Member Post Type

        require_once( 'custom-post-types/testimonials/testimonials-functions.php' ); 	// Testimonial Post Type
	   
	    // Demo Content
	    		
	    require_once( 'admin/dashboard/theme-dashboard.php' ); 
	    	
	   	// Demo Content
	   		
	   	require_once( 'demo-content/demo-main.php' ); 
	   	
	   	// Shortcodes
	   
	   	require_once( 'shortcodes/shortcodes-config.php' );
	   	
	   	// Engage Slider
	   	
	    require_once( 'veented-slider/slider-config.php' );
	   	
	   	// Post Likes
	   	
	   	require_once( 'post-likes/post-like.php' ); 
	   
	    // Post Order
	    
	    require_once( 'plugins/custom-post-order/simple-custom-post-order.php' );
	    
	    // Menu item meta fields
	    
	    require_once( 'plugins/nav-menu-custom-fields/nav-menu-custom-fields.php' );
	    
	    // Templatera
	    
	    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	    
	    if ( is_plugin_active( 'js_composer/js_composer.php' ) && !function_exists( 'wpb_templatera_render_row_action' ) ) {
	    	require_once( 'templates/templates.php' );
	    }
   		
   		// Functions and styles
   		
   		function engage_core_scripts() {

   		    wp_register_script( 'engage-contact', plugins_url( '/shortcodes/lib/contact-form/public/js/contact-form.js', __FILE__ ) );

   		}
   		
   		add_action( 'wp_enqueue_scripts', 'engage_core_scripts' );
	   	
	   	// Engage Core Class
	   	
	   	class Engage_Core {
	   		
	   		public static function color_array( $custom = false ) {
	   			
	   			$color_array = array(
	   				'white' => 'White',
	   				'accent' => 'Accent',
	   				'dark' => 'Dark',
	   			);
	   			
	   			if( $custom == true ) {
	   				$color_array['custom'] = 'Custom';
	   			}
	   			
	   			return $color_array;
	   			
	   		}
	   		
	   		public static function hover_color_array() {
	   				
				$color_array = array(
					'white' => 'White',
					'dark' => 'Dark',
					'accent' => 'Accent'
				);
				
				return $color_array;
				
			}
			
			public static function social_icons( $email = true ) {
				
				$social_icons_array = array( 
					'facebook' => 'facebook',
					'twitter' => 'twitter',
					'google-plus' => 'google',
					'pinterest' => 'pinterest-o'
				);
				
				return $social_icons_array;
				
			}
					
			public static function get_animated_class() {
				return ' animated vntd-animated';
			}
			
			public static function get_portfolio_defaults( $option_name ) {
				
				$portfolio_defaults = array(
					"layout_type" => 'grid',
					"item_style" => 'caption',
					"item_caption_style" => 'visible',
					"item_caption_align" => 'left',
					"item_caption_content" => 'title_categories',
					"item_caption_position" => 'center',
					"item_caption_categories" => 'no',
					"caption_border" => 'on',
					"item_hover_style" => 'zoom_link',
					"image_hover_effect" => 'zoom',
					"image_hover_overlay" => 'dark',
					"thumb_space" => 'yes',
					"love" => 'yes',
					"pagination" => 'disable',
					"more_button_style" => 'normal',
					"filter" => 'yes',
					"filter_align" => 'center',
					"filter_orderby" => 'slug',
					"animation" => 'quicksand',
					"order" => 'DESC',
					"orderby" => 'date',
				);
				
				if ( engage_option( 'portfolio_' . $option_name ) ) {
					return engage_option( 'portfolio_' . $option_name );
				} else if ( array_key_exists( $option_name, $portfolio_defaults ) ) {
					return $portfolio_defaults[ $option_name ];
				} else {
					return false;
				}
				
			}
			
			public static function portfolio_defaults() {
				
				$portfolio_defaults = array(
					"layout_type" => 'grid',
					"item_style" => 'caption',
					"item_caption_style" => 'visible',
					"item_caption_align" => 'left',
					"item_caption_content" => 'title_categories',
					"item_caption_position" => 'center',
					"item_caption_categories" => 'no',
					"caption_border" => 'on',
					"item_hover_style" => 'zoom_link',
					"image_hover_effect" => 'zoom',
					"image_hover_overlay" => 'dark',
					"thumb_space" => 'yes',
					"love" => 'yes',
					"pagination" => 'disable',
					"more_button_style" => 'normal',
					"filter" => 'yes',
					"filter_align" => 'center',
					"filter_orderby" => 'slug',
					"animation" => 'quicksand',
					"order" => 'DESC',
					"orderby" => 'date',
				);
				
				foreach ( $portfolio_defaults as $key => $value ) {
					if ( engage_option( 'portfolio_' . $key ) ) {
						$portfolio_defaults[ $key ] = engage_option( 'portfolio_' . $key );
					}
				}
				
				return $portfolio_defaults;
				
			}
			
			public static function get_element_defaults( $el_name ) {
				
				if ( $el_name == 'button' ) {
					$prefix = 'el_btn_';
					$defaults = array(
						"color" => 'accent',
						"color_hover" => 'dark',
						"style" => 'solid',
						"border_radius" => 'round',
						"shadow" => 'no'
					);
				} else {
					return false;
				}
				
				foreach ( $defaults as $key => $value ) {
					if ( engage_option( $prefix . $key ) ) {
						$defaults[ $key ] = engage_option( $prefix . $key );
					}
				}
				
				return $defaults;
				
			}
	   		
	   	}
	   	
	   	
	   	if ( !function_exists( 'engage_vc_google_font' ) ) {
	   		function engage_vc_google_font( $google_font ) {
	   			
	   			$italic = '';
	   			
	   			if ( strpos( $google_font, 'italic' ) !== false ) {
	   				$italic = 'i';
	   			}
	   			
	   			$google_font = explode( '|', $google_font );
	   			
	   			$font_family = explode( ':', $google_font[0] ); $font_family = explode( '%3', $font_family[1] ); $font_family = $font_family[0];
	   			
	   			$font_style = explode( ':', $google_font[1] ); $font_style = explode( '%20', $font_style[1] );
	 
	   			$font_style = $font_style[0];
	   			
	   			wp_enqueue_style( 'vntd_font_' . $font_family, '//fonts.googleapis.com/css?family=' . $font_family . ':' . $font_style . $italic );
	   			
	   			return array(
	   				'font-family' => str_replace( '%20', ' ', $font_family ),
	   				'font-style' => $font_style
	   			);
	   		}
	   	}
	   	
	   	if ( !function_exists( 'engage_build_link' ) ) {
   	  		function engage_build_link( $label, $link, $classes ) {


                if ( strpos( $link, 'url:') !== false) {
                    $link = vc_build_link( $link );
                    if ( is_array( $link ) ) {
                        $url = $link['url'];
                    } else {
                        $url = $link;
                    }
                } else {
                    $url = $link;
                }
   	  			
   	  			$anchor = '<a href="' . $url . '"';
   	  			
   	  			if ( is_array( $link ) ) {
   	  				if ( $link['target'] != '' ) $anchor .= ' target="' . $link['target'] . '"';
   	  				if ( $link['title'] != '' ) $anchor .= ' title="' . $link['title'] . '"';
   	  			}
   	  			
   	  			$anchor .= ' class="' . esc_attr( $classes ) . '">' . esc_html( $label ) . '</a>';
   	  			
   	  			return $anchor;
   	  			
   	  		}
   	  	}
   	
   	}
   
?>