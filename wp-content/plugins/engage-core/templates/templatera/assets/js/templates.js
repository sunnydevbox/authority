(function ($, vc) {
	vc_reloadTemplateList = function (data) {
		$.ajax({
			type: 'POST',
			url: window.ajaxurl,
			data: data
		}).done(function (html) {
			$('[data-vc-template=list]').html(html);

		});

	};
	$(document).ready(function () {
		if ( ! window.vc || ! window.vc.storage ) {
			return;
		}
		
		$('[data-vc-template=list]').on('click', '[data-templatera_id]', function (e) {
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: window.ajaxurl,
				data: {
					action: 'templatera_plugin_load',
					template_id: $(this).data('templatera_id'),
					_vcnonce: window.vcAdminNonce
				},
				dataType: 'html'
			}).done(function (shortcodes) {
				if (_.isEmpty(shortcodes)) return false;
				_.each(vc.filters.templates, function (callback) {
					shortcodes = callback(shortcodes);
				});
				vc.storage.append($.trim(shortcodes));
				vc.shortcodes.fetch({reset: true});
			});
		});
		$('#templatera_save_button').click(function (e) {
			e.preventDefault();
			var name = window.prompt(window.VcTemplateI18nLocale.please_enter_templates_name, ''),
				shortcodes = '',
				data;
			if (_.isString(name) && name.length) {
				shortcodes = vc.storage.getContent();
				data = {
					action: 'templatera_plugin_save',
					content: shortcodes,
					title: name,
					post_id: $('#post_ID').val(),
					_vcnonce: window.vcAdminNonce
				};
				vc_reloadTemplateList(data);
			}
		});
		// Engage Templates
		$( '#engage-template-btn' ).click( function(e) {
		
			//alert( 'Works!' );
			//console.log( vc );
			//console.log( vc.TemplatesPanelViewBackend );
			
			//var TemplateraPanelEditorBackend = vc.TemplatesPanelViewBackend;
			
			//vc.TemplatesPanelViewBackend( '[vc_row]Text[/vc_row]' );
			
			html = '[vc_row][vc_column][vc_column_text]Kielbaskaa.[/vc_column_text][/vc_column][/vc_row]';
			
			var modele = vc.storage.parseContent({}, html);
			
			_.each(modele, function(model) {
			    vc.shortcodes.create(model)
			});
			
			vc.closeActivePanel();
		
		});
		
		// VC Veented Templates
		
		$( '.vntd-load-template,#template-preview-load' ).click( function(e) {
			
			html = '[vc_row][vc_column][vc_column_text]Something went wrong.[/vc_column_text][/vc_column][/vc_row]';
			
			var $parent = $(this).closest( 'li' );
			
			if ( $(this).hasClass( 'load-from-preview' ) ) {
				$parent = $grid.find( '#' + $( '.vntd-template-preview' ).attr( 'data-template-id' ) );
				console.log( 'ID: ' + $( '.vntd-template-preview' ).attr( 'data-template-id' ) );
			} 
			
			if ( $parent.find( '.vntd-template-sc' ).html() != '' ) {
				html = $parent.find( '.vntd-template-sc' ).html();
			}
			
			var modele = vc.storage.parseContent({}, html);
			
			_.each(modele, function(model) {
			    vc.shortcodes.create(model)
			});
			
			vc.closeActivePanel();
		
		});
		
		// Preview img
		
		var $preview = $( '#vntd-template-preview' );
		var $grid = $( '#vntd-templates-main' );
		var $templatesList = $( '.vntd-templates-list' );
		
		$( '.vntd-template-img' ).on( 'click', function() {
		
			var $parent = $(this).closest('li');
			
			var title = $parent.find( 'h5' ).html();
			var imgUrl = $parent.find( '.vntd-template-img' ).data( 'preview-img' );
			var id = $parent.attr( 'id' );
			
			$preview.attr( 'data-template-id', id );
			$preview.find( '#template-preview-title' ).text( title );
			$preview.find( '#template-preview-desc' ).text( $parent.find( 'p.template-desc' ).html() );
			$preview.find( '#template-preview-info' ).hide();
			if ( $parent.find( 'p.template-info' ).html() ) {
				$preview.find( '#template-preview-info' ).text( $parent.find( 'p.template-info' ).html() ).show();
			}
			
			$preview.find( '#template-preview-img' ).attr( 'src', imgUrl );
			
			$grid.hide();
			$preview.fadeIn();
		});
		
		$( '.vntd-template-close,.vntd-preview-back' ).on( 'click', function(event) {
			event.preventDefault();
			$preview.hide();
			$grid.fadeIn();
		});
		
		// Filtering menu
		
		$( '#vntd-templates-filter li button' ).on( 'click', function(e) {
		
			var $activeParent = $(this).closest( 'li' );
			
			$( '#vntd-templates-filter li' ).removeClass( 'vntd-active' );
			$activeParent.addClass( 'vntd-active' );
			
			var filter = $(this).data( 'filter' );
			
			if ( filter == 'all' ) {
				$( '.vntd-templates-list li' ).removeClass('vntd-hidden');
			} else {
				$( '.vntd-templates-list li' ).removeClass('vntd-hidden').addClass( 'vntd-hidden' );
				$( '.vntd-templates-list li.cat-' + filter ).removeClass('vntd-hidden');
			}
			
		});
		
		function openTemplatePreview( templateID ) {
			
			var $template = $grid.find( '#' + templateID );
			
			var title = $template.find( 'h5' ).html();
			var imgUrl = $template.find( '.vntd-template-img' ).data( 'preview-img' );
			var id = $template.attr( 'id' );
			
			$preview.attr( 'data-template-id', id );
			$preview.find( '#template-preview-title' ).text( title );
			$preview.find( '#template-preview-desc' ).text( $template.find( 'p.template-desc' ).html() );
			$preview.find( '#template-preview-info' ).hide();
			if ( $template.find( 'p.template-info' ).html() ) {
				$preview.find( '#template-preview-info' ).text( $template.find( 'p.template-info' ).html() ).show();;
			}
			$preview.find( '#template-preview-img' ).attr( 'src', imgUrl );
			
		}
		
		// Prev / Next buttons
		
		$( '.vntd-template-next' ).on( 'click', function() {
		
			var currentID = $( '#vntd-template-preview' ).attr( 'data-template-id' );
			//var nextID = $templatesList.find( '#' + currentID ).next( 'li' ).attr( 'id' );
			var nextID = $templatesList.find( '#' + currentID + ' ~ li:not(.vntd-hidden)' ).attr( 'id' );
			
			if ( !nextID ) {
				var nextID = $templatesList.find( 'li:not(.vntd-hidden):first' ).attr( 'id' );
			}
			openTemplatePreview( nextID );
			
		});
		
		// Prev button
		
		$( '.vntd-template-prev' ).on( 'click', function() {
		
			var currentID = $( '#vntd-template-preview' ).attr( 'data-template-id' );
			//var nextID = $templatesList.find( '#' + currentID ).prev( 'li' ).attr( 'id' );
			var nextID = $templatesList.find( '#' + currentID ).prevAll("li:not(.vntd-hidden):first").attr( 'id' );
			if ( !nextID ) {
				var nextID = $templatesList.find( 'li:not(.vntd-hidden):last' ).attr( 'id' );
			}
			openTemplatePreview( nextID );
			
		});
		
		// VC Single Image
		
//		if ( $( '.wpb_vc_single_image' ).length > 0 ) {
//			console.log( 'Found' );
//		} else {
//			console.log( 'Not' );
//		}
		
//		vc.events.on( 'vc:backend_editor:switch', function() {
//			console.log( 'Backend switch' );
//		});
		
		vc.events.on( 'shortcodes:vc_single_image:sync', function() {
			init_vntd_images();
		});
		
		function init_vntd_images() {
			if ( $( '.wpb_vc_single_image:not(.vntd-external-image)' ).length > 0 ) {
				
				$( '.wpb_vc_single_image:not(.vntd-external-image)' ).each( function() {
				
					var value = $(this).find( '.wpb_vc_param_value' ).attr( 'value' );
					
					if ( value.indexOf( 'unsplash' ) !== -1 ) {
					
						value = value.substring(0, value.lastIndexOf('/') + 1) + '150x150';
						
						$(this).find( '.attachment-thumbnail' ).attr( 'src', value );
						$(this).find( '.attachment-thumbnail' ).clone().attr( 'style', '' ).addClass( 'vntd-img-preview' ).insertBefore( $(this).find( '.attachment-thumbnail' ) );
						$(this).find( '.no_image_image' ).addClass( 'image-exists' );
						$(this).find( '.no_image_image' ).hide();
						
						$(this).addClass( 'vntd-external-image' );
						
					}
					
				});
			}
		}
		
		$('#vc_ui-panel-edit-element:not(.vc_active)').on( "change", function( event ){
			
			if ( $( '.wpb_el_type_attach_image:not(.vntd-gallery-ready)' ).length > 0 ) {
				
				$( '.wpb_el_type_attach_image:not(.vntd-gallery-ready)' ).each( function() {
				
					var $gallery = $(this);
					
					var value = $gallery.find( 'input.attach_image' ).attr( 'value' );
					
					if ( value.indexOf( 'http' ) !== -1 ) {
						
						//var URL = value.substring( 0, value.lastIndexOf('/') + 1 ) + '150x150';
						
						// Get URL of a smaller, thumbnail version of the image
						
						var URL = value;
						
						if ( value.indexOf( 'veented.com' ) !== -1 ) {
							URL = URL.replace( '.jpg', '-150x150.jpg' );
							URL = URL.replace( '.png', '-150x150.png' );
							URL = URL.replace( '.jpeg', '-150x150.jpeg' );
						}
						
						var imagePreview = '<li class="added ui-sortable-handle vntd-added-image"><img src="' + URL + '"><a href="#" class="vntd-image-remove" style="position:absolute;top:0;left:0;right:0;bottom:0;color:red;text-decoration:none;"><i class="vc-composer-icon vc-c-icon-close"></i></a></li>';
						
						$gallery.find( '.gallery_widget_attached_images_list' ).append( imagePreview );
						
						$gallery.delegate( '.vntd-image-remove', 'click', function() {
							$gallery.find( '.vntd-added-image' ).remove();
							$gallery.find( 'input.attach_image' ).attr( 'value', '' );
							
						});
						
					}
					
					$gallery.addClass( 'vntd-gallery-ready' );
				});
				
			}
			
		});
		
		if ( typeof templatera_editor != "undefined" ) {
			templatera_editor.on( 'render', function() {
				
				var $engageTemplates = $( '.vntd-templates-list' );
				
				if ( $engageTemplates.length > 0 && !$engageTemplates.hasClass( 'vntd-images-loaded' ) ) {
					$engageTemplates.find( '.vntd-template-img img' ).each( function() {
						$(this).attr( 'src', $(this).data( 'img-src' ) );
					});
					$engageTemplates.addClass( 'vntd-images-loaded' );
				}
				
			});
		}
		
//		vc.events.on( 'shortcodes:vc_single_image:add', function() {
//			console.log( 'Image added' );
//			
//			init_vntd_images();
//		});
//		
//		vc.events.on( 'vc:access:initialize', function() {
//			console.log( 'Access initialized' );
//		});
//		
//		vc.events.on( 'vc:backend_editor:show', function() {
//			console.log( 'Backend show' );
//		});
//		
//		vc.events.on( 'vc:access:backend:ready', function() {
//			console.log( 'Backend ready' );
//		});
//		
//		vc.events.on( 'app.render', function() {
//			console.log( 'app.render' );
//		});
//		
//		vc.events.on( 'click:media_editor:add_image', function() {
//			console.log( 'click:media_editor:add_image' );
//		});
//		
//		vc.events.on( 'shortcodes:add', function() {
//			console.log( 'shortcodes:add' );
//			init_vntd_images();
//		});
//		
//		vc.events.on( 'presets.apply', function() {
//			console.log( 'presets.apply' );
//		});
//		
//		vc.events.on( 'shortcodes:sync', function() {
//			console.log( 'shortcodes:sync' );
//		});
//		
//		vc.events.on( 'vcPointer:close', function() {
//			console.log( 'vcPointer:close' );
//		});
//		
//		vc.events.on( 'vcPointer:show', function() {
//			console.log( 'vcPointer:show' );
//		});
//		
//		vc.events.on( 'backendEditor.show', function() {
//			console.log( 'backendEditor.show' );
//		});
		
		
		
		
		
//		vc.events.on( 'shortcodeView:ready', function() {
//			console.log( 'shortcodeView:ready' );
//		});
//		
//		vc.events.on( 'backend.shortcodeViewChangeParams', function() {
//			console.log( 'backend.shortcodeViewChangeParams' );
//		});
		
		
		
		
		
		
		
		//console.log( );
		
		//$( '.wpb_vc_single_image' ).addClass( 'red' );
		
//		$( '.vntd-templates li' ).on( 'mouseover', function() {
//			console.log( 'active' );
//			$(this).find( '.vntd-template-preview' ).addClass( 'preview-active' );
//		}).on( 'mouseout', function() {
//			console.log( 'out' );
//			$(this).find( '.vntd-template-preview' ).removeClass( 'preview-active' );
//		});
		
	});
	if ( ! window.vc || ! window.vc.shortcode_view ) {
		return;
	}
	if ( ! vc || ! vc.shortcode_view ) {
		return;
	}
	window.VcTemplateraModel = Backbone.Model.extend({
		getParam: function (key) {
			return _.isObject(this.get('params')) && !_.isUndefined(this.get('params')[key]) ? this.get('params')[key] : '';
		}
	});
	window.VcTemplatera = vc.shortcode_view.extend({
		render: function () {
			window.VcTemplatera.__super__.render.call(this);
			this.$wrapper = this.$el.find('> .wpb_element_wrapper');
			$('<div class="vct_cover"/>').insertBefore(this.$wrapper);
			return this;
		},
		changeShortcodeParams: function (model) {
			var params = model.get('params');
			window.VcTemplatera.__super__.changeShortcodeParams.call(this, model);
			if (_.isObject(params) && _.isString(params.id)) {
				this.$wrapper.html('<img src="images/wpspin_light.gif" title="Loading..." class="templatera_loader">');
				$.ajax({
					type: 'post',
					url: window.ajaxurl,
					data: {
						action: 'wpb_templatera_load_html',
						id: params.id,
						_vcnonce: window.vcAdminNonce
					},
					context: this
				}).done(function (data_string) {
					// var parent_model = vc.shortcodes.get(this.model.get('parent_id')),
					// row = parent_model.get('parent_id') ? vc.app.views[parent_model.get('parent_id')] : false;
					this.$wrapper.html('');
					var data = vc.storage.parseContent({}, data_string);
					_.each(data, function (shortcode) {
						var model = new VcTemplateraModel(shortcode);
						this.appendShortcode(model);
					}, this);
					// row && row.changedContent();
				});
			} else {
				this.$wrapper.html('');
			}
		},
		appendShortcode: function (model) {
			var view = this.getView(model),
				position = model.get('order'),
				$element_to_add = model.get('parent_id') !== false ?
					vc.app.views[model.get('parent_id')].$content : this.$wrapper;
			vc.app.views[model.id] = view;
			if (model.get('parent_id')) {
				var parent_view = vc.app.views[model.get('parent_id')];
				parent_view.unsetEmpty();

			}
			$element_to_add.append(view.render().el);
			view.ready();

			view.changeShortcodeParams(model); // Refactor
			view.checkIsEmpty();
		},
		getView: function (model) {
			var view;
			if (_.isObject(vc.map[model.get('shortcode')]) && _.isString(vc.map[model.get('shortcode')].js_view) && vc.map[model.get('shortcode')].js_view.length) {
				view = new window[window.vc.map[model.get('shortcode')].js_view]({model: model});
			} else {
				view = new vc.shortcode_view({model: model});
			}
			return view;
		},
		changedContent: function () {
			this.$wrapper.find('.templatera_loader').remove();
		}
	});
})(window.jQuery, window.vc);