<?php
/**
 * Main object for controls
 *
 * @package vas_map
 */
if ( ! class_exists( 'VcTemplateManager' ) ) {
	/**
	 * Class VcTemplateManager
	 */
	class VcTemplateManager {
		/**
		 * @var string
		 */
		protected $dir;
		/**
		 * @var string
		 */
		protected static $post_type = 'templatera';
		/**
		 * @var string
		 */
		protected static $meta_data_name = 'templatera';
		/**
		 * @var string
		 */
		protected $settings_tab = 'templatera';
		/**
		 * @var string
		 */
		protected $filename = 'templatera';
		/**
		 * @var bool
		 */
		protected $init = false;
		/**
		 * @var bool
		 */
		protected $current_post_type = false;
		/**
		 * @var string
		 */
		protected static $template_type = 'templatera_templates';
		/**
		 * @var array
		 */
		protected $settings = array(
			'assets_dir' => 'assets',
			'templates_dir' => 'templates',
			'template_extension' => 'tpl.php',
		);
		/**
		 * @var string
		 */
		protected static $vcWithTemplatePreview = '4.8';

		/**
		 * VcTemplateManager constructor.
		 * @param $dir
		 */
		function __construct( $dir ) {
			$this->dir = empty( $dir ) ? dirname( dirname( __FILE__ ) ) : $dir; // Set dir or find by current file path.
			$this->plugin_dir = basename( $this->dir ); // Plugin directory name required to append all required js/css files.
			add_filter( 'wpb_vc_js_status_filter', array(
				$this,
				'setJsStatusValue',
			) );
		}

		/**
		 * @static
		 * Singleton
		 *
		 * @param string $dir
		 *
		 * @return VcTemplateManager
		 */
		public static function getInstance( $dir = '' ) {
			static $instance = null;
			if ( null === $instance ) {
				$instance = new VcTemplateManager( $dir );
			}

			return $instance;
		}

		/**
		 * @static
		 * Install plugins.
		 * Migrate default templates into templatera
		 * @return void
		 */
		public static function install() {
			$migrated = get_option( 'templatera_migrated_templates' ); // Check is migration already performed
			if ( 'yes' !== $migrated ) {
				$templates = (array) get_option( 'wpb_js_templates' );
				foreach ( $templates as $template ) {
					self::create( $template['name'], $template['template'] );
				}
				update_option( 'templatera_migrated_templates', 'yes' );
			}
		}

		/**
		 * @return string
		 */
		public static function postType() {
			return self::$post_type;
		}

		/**
		 * Initialize plugin data
		 * @return VcTemplateManager
		 */
		function init() {
			if ( $this->init ) {
				return $this;
			} // Disable double initialization.
			$this->init = true;

			if ( current_user_can( 'manage_options' ) && isset( $_GET['action'] ) && 'export_templatera' === $_GET['action'] ) {
				$id = ( isset( $_GET['id'] ) ? $_GET['id'] : null );
				add_action( 'wp_loaded', array_map( array(
					$this,
					'export',
				), array( $id ) ) );
			}
			$this->createPostType();
			$this->initPluginLoaded(); // init filters/actions and hooks
			// Add vc template post type into the list of allowed post types for visual composer.
			if ( $this->isValidPostType() ) {
				$pt_array = get_option( 'wpb_js_content_types' );
				if ( ! is_array( $pt_array ) || empty( $pt_array ) ) {
					$pt_array = array(
						self::postType(),
						'page',
					);
					update_option( 'wpb_js_content_types', $pt_array );
				} elseif ( ! in_array( self::postType(), $pt_array ) ) {
					$pt_array[] = self::postType();
					update_option( 'wpb_js_content_types', $pt_array );
				}
				vc_set_default_editor_post_types( array(
					'page',
					'templatera',
				) );
				vc_editor_set_post_types( vc_editor_post_types() + array( 'templatera' ) );
				add_action( 'admin_init', array(
					$this,
					'createMetaBox',
				), 1 );
				add_filter( 'vc_role_access_with_post_types_get_state', '__return_true' );
				add_filter( 'vc_role_access_with_backend_editor_get_state', '__return_true' );
				add_filter( 'vc_role_access_with_frontend_editor_get_state', '__return_true' );
				add_filter( 'vc_check_post_type_validation', '__return_true' );
			}
			add_action( 'wp_loaded', array(
				$this,
				'createShortcode',
			) );

			return $this; // chaining.
		}

		/**
		 * Create tab on VC settings page.
		 *
		 * @param $tabs
		 *
		 * @return array
		 */
		public function addTab( $tabs ) {
			if ( $this->isUserRoleAccessVcVersion() && ! vc_user_access()->part( 'templates' )->can()->get() ) {
				return $tabs;
			}
			$tabs[ $this->settings_tab ] = __( 'Templatera', 'templatera' );

			return $tabs;
		}

		/**
		 * Create tab fields. in Visual composer settings page options-general.php?page=vc_settings
		 *
		 * @param Vc_Settings $settings
		 */
		public function buildTab( Vc_Settings $settings ) {
			$settings->addSection( $this->settings_tab );
			add_filter( 'vc_setting-tab-form-' . $this->settings_tab, array(
				$this,
				'settingsFormParams',
			) );
			$settings->addField( $this->settings_tab, __( 'Export VC Templates', 'templatera' ), 'export', array(
				$this,
				'settingsFieldExportSanitize',
			), array(
				$this,
				'settingsFieldExport',
			) );
			$settings->addField( $this->settings_tab, __( 'Import VC Templates', 'templatera' ), 'import', array(
				$this,
				'settingsFieldImportSanitize',
			), array(
				$this,
				'settingsFieldImport',
			) );
		}

		/**
		 * Custom attributes for tab form.
		 * @see VcTemplateManager::buildTab
		 *
		 * @param $params
		 *
		 * @return string
		 */
		public function settingsFormParams( $params ) {
			$params .= ' enctype="multipart/form-data"';

			return $params;
		}

		/**
		 * Sanitize export field.
		 * @return bool
		 */
		public function settingsFieldExportSanitize() {
			return false;
		}

		/**
		 * Builds export link in settings tab.
		 */
		public function settingsFieldExport() {
			echo '<a href="export.php?page=wpb_vc_settings&action=export_templatera" class="button">' . __( 'Download Export File', 'templatera' ) . '</a>';
		}

		/**
		 * Convert template/post to xml for export
		 *
		 * @param stdClass $template
		 *
		 * @return string
		 */
		private function templateToXml( $template ) {
			$id = $template->ID;
			$meta_data = get_post_meta( $id, self::$meta_data_name, true );
			$post_types = isset( $meta_data['post_type'] ) ? $meta_data['post_type'] : false;
			$user_roles = isset( $meta_data['user_role'] ) ? $meta_data['user_role'] : false;
			$xml = '';
			$xml .= '<template>';
			$xml .= '<title>' . apply_filters( 'the_title_rss', $template->post_title ) . '</title>' . '<content>' . $this->wxr_cdata( apply_filters( 'the_content_export', $template->post_content ) ) . '</content>';
			if ( false !== $post_types ) {
				$xml .= '<post_types>';
				foreach ( $post_types as $t ) {
					$xml .= '<post_type>' . $t . '</post_type>';
				}
				$xml .= '</post_types>';
			}
			if ( false !== $user_roles ) {
				$xml .= '<user_roles>';
				foreach ( $user_roles as $u ) {
					$xml .= '<user_role>' . $u . '</user_role>';
				}
				$xml .= '</user_roles>';
			}

			$xml .= '</template>';

			return $xml;
		}

		/**
		 * Export existing template in XML format.
		 *
		 * @param int $id (optional) Template ID. If not specified, export all templates
		 */
		public function export( $id = null ) {
			if ( $id ) {
				$template = get_post( $id );
				$templates = $template ? array( $template ) : array();
			} else {
				$templates = get_posts( array(
					'post_type' => self::postType(),
					'numberposts' => - 1,
				) );
			}

			$xml = '<?xml version="1.0"?><templates>';
			foreach ( $templates as $template ) {
				$xml .= $this->templateToXml( $template );
			}
			$xml .= '</templates>';
			header( 'Content-Description: File Transfer' );
			header( 'Content-Disposition: attachment; filename=' . $this->filename . '_' . date( 'dMY' ) . '.xml' );
			header( 'Content-Type: text/xml; charset=' . get_option( 'blog_charset' ), true );
			echo $xml;
			die();
		}

		/**
		 * Import templates from file to the database by parsing xml file
		 * @return bool
		 */
		public function settingsFieldImportSanitize() {
			$file = isset( $_FILES['import'] ) ? $_FILES['import'] : false;
			if ( false === $file || ! file_exists( $file['tmp_name'] ) ) {
				return false;
			} else {
				$post_types = get_post_types( array( 'public' => true ) );
				$roles = get_editable_roles();
				$templateras = simplexml_load_file( $file['tmp_name'] );
				foreach ( $templateras as $template ) {
					$template_post_types = $template_user_roles = $meta_data = array();
					$content = (string) $template->content;
					$id = $this->create( (string) $template->title, $content );
					$this->contentMediaUpload( $id, $content );
					foreach ( $template->post_types as $type ) {
						$post_type = (string) $type->post_type;
						if ( in_array( $post_type, $post_types ) ) {
							$template_post_types[] = $post_type;
						}
					}
					if ( ! empty( $template_post_types ) ) {
						$meta_data['post_type'] = $template_post_types;
					}
					foreach ( $template->user_roles as $role ) {
						$user_role = (string) $role->user_role;
						if ( in_array( $user_role, $roles ) ) {
							$template_user_roles[] = $user_role;
						}
					}
					if ( ! empty( $template_user_roles ) ) {
						$meta_data['user_role'] = $template_user_roles;
					}
					update_post_meta( (int) $id, self::$meta_data_name, $meta_data );
				}
				@unlink( $file['tmp_name'] );
			}

			return false;
		}

		/**
		 * Build import file input.
		 */
		public function settingsFieldImport() {
			echo '<input type="file" name="import">';
		}

		/**
		 * Upload external media files in a post content to media library.
		 *
		 * @param $post_id
		 * @param $content
		 *
		 * @return bool
		 */
		protected function contentMediaUpload( $post_id, $content ) {
			preg_match_all( '/<img|a[^>]* src|href=[\'"]?([^>\'" ]+)/', $content, $matches );
			foreach ( $matches[1] as $match ) {
				$extension = pathinfo( $match, PATHINFO_EXTENSION );
				if ( ! empty( $match ) && ! empty( $extension ) ) {
					$file_array = array();
					$file_array['name'] = basename( $match );
					$tmp_file = download_url( $match );
					$file_array['tmp_name'] = $tmp_file;
					if ( is_wp_error( $tmp_file ) ) {
						@unlink( $file_array['tmp_name'] );
						$file_array['tmp_name'] = '';

						return false;
					}
					$desc = $file_array['name'];
					$id = media_handle_sideload( $file_array, $post_id, $desc );
					if ( is_wp_error( $id ) ) {
						@unlink( $file_array['tmp_name'] );

						return false;
					} else {
						$src = wp_get_attachment_url( $id );
					}
					$content = str_replace( $match, $src, $content );
				}
			}
			wp_update_post( array(
				'ID' => $post_id,
				'post_content' => $content,
			) );

			return true;
		}

		/**
		 * CDATA field type for XML
		 *
		 * @param $str
		 *
		 * @return string
		 */
		function wxr_cdata( $str ) {
			if ( seems_utf8( $str ) == false ) {
				$str = utf8_encode( $str );
			}

			// $str = ent2ncr(esc_html($str));
			$str = '<![CDATA[' . str_replace( ']]>', ']]]]><![CDATA[>', $str ) . ']]>';

			return $str;
		}

		/**
		 * Create post type "templatera" and item in the admin menu.
		 * @return void
		 */
		function createPostType() {
			register_post_type( self::postType(), array(
				'labels' => self::getPostTypesLabels(),
				'public' => false,
				'has_archive' => false,
				'show_in_nav_menus' => true,
				'exclude_from_search' => true,
				'publicly_queryable' => false,
				'show_ui' => true,
				'query_var' => true,
				'capability_type' => 'post',
				'hierarchical' => false,
				'menu_position' => null,
				'menu_icon' => $this->assetUrl( 'images/icon.gif' ),
				'show_in_menu' => ! WPB_VC_NEW_MENU_VERSION,
			) );
		}

		/**
		 * @return array
		 */
		public static function getPostTypesLabels() {
			return array(
				'add_new_item' => __( 'Add template', 'templatera' ),
				'name' => __( 'Templates', 'templatera' ),
				'singular_name' => __( 'Template', 'templatera' ),
				'edit_item' => __( 'Edit Template', 'templatera' ),
				'view_item' => __( 'View Template', 'templatera' ),
				'search_items' => __( 'Search Templates', 'templatera' ),
				'not_found' => __( 'No Templates found', 'templatera' ),
				'not_found_in_trash' => __( 'No Templates found in Trash', 'templatera' ),
			);
		}

		/**
		 * Init filters / actions hooks
		 */
		function initPluginLoaded() {
			load_plugin_textdomain( 'templatera', false, basename( $this->dir ) . '/locale' );

			// Check for nav controls
			add_filter( 'vc_nav_controls', array(
				$this,
				'createButtonFrontBack',
			) );
			add_filter( 'vc_nav_front_controls', array(
				$this,
				'createButtonFrontBack',
			) );

			// add settings tab in visual composer settings
			add_filter( 'vc_settings_tabs', array(
				$this,
				'addTab',
			) );
			// build settings tab @ER
			add_action( 'vc_settings_tab-' . $this->settings_tab, array(
				$this,
				'buildTab',
			) );

			add_action( 'wp_ajax_vc_templatera_save_template', array(
				$this,
				'saveTemplate',
			) );
			add_action( 'wp_ajax_vc_templatera_delete_template', array(
				$this,
				'delete',
			) );
			add_filter( 'vc_templates_render_category', array(
				$this,
				'renderTemplateBlock',
			), 10, 2 );
			add_filter( 'vc_templates_render_template', array(
				$this,
				'renderTemplateWindow',
			), 10, 2 );

			if ( $this->getPostType() !== 'vc_grid_item' ) {
				add_filter( 'vc_get_all_templates', array(
					$this,
					'replaceCustomWithTemplateraTemplates',
				) );
			}
			add_filter( 'vc_templates_render_frontend_template', array(
				$this,
				'renderFrontendTemplate',
			), 10, 2 );
			add_filter( 'vc_templates_render_backend_template', array(
				$this,
				'renderBackendTemplate',
			), 10, 2 );
			add_action( 'vc_templates_render_backend_template_preview', array(
				$this,
				'getTemplateContentPreview',
			), 10, 2 );
			add_filter( 'vc_templates_show_save', array(
				$this,
				'addTemplatesShowSave',
			) );
			add_action( 'wp_ajax_wpb_templatera_load_html', array(
				$this,
				'loadHtml',
			) ); // used in changeShortcodeParams in templates.js, todo make sure we need this?
			add_action( 'save_post', array(
				$this,
				'saveMetaBox',
			) );

			add_action( 'vc_frontend_editor_enqueue_js_css', array(
				$this,
				'assetsFe',
			) );
			add_action( 'vc_backend_editor_enqueue_js_css', array(
				$this,
				'assetsBe',
			) );

		}

		/**
		 * This used to detect what version of nav_controls use, and panels/modals js/template
		 *
		 * @param string $version
		 *
		 * @return bool
		 */
		function isNewVcVersion( $version = '4.4' ) {
			return defined( 'WPB_VC_VERSION' ) && version_compare( WPB_VC_VERSION, $version ) >= 0;
		}

		/**
		 * Removes save block if we editing templatera page
		 * In add templates panel window
		 * @since 4.4
		 * @param $show_save
		 * @return bool
		 */
		public function addTemplatesPanelShowSave( $show_save ) {
			if ( $this->isSamePostType() ) {
				$show_save = false; // we don't need "save" block if we editing templatera page.
			}

			return $show_save;
		}

		/**
		 * @since 4.4 we implemented new panel windows
		 * @return bool
		 */
		function isPanelVcVersion() {
			return $this->isNewVcVersion( '4.7' );
		}

		/**
		 * @since 4.8 we implemented new user roles part checks
		 * @return bool
		 */
		function isUserRoleAccessVcVersion() {
			return $this->isNewVcVersion( '4.8' );
		}

		/**
		 * Used to render template for backend
		 * @since 4.4
		 *
		 * @param $template_id
		 * @param $template_type
		 *
		 * @return string|int
		 */
		public function renderBackendTemplate( $template_id, $template_type ) {
			if ( self::$template_type === $template_type ) {
				WPBMap::addAllMappedShortcodes();
				// do something to return output of templatera template
				$post = get_post( $template_id );
				if ( $this->isSamePostType( $post->post_type ) ) {
					echo $post->post_content;
					die();
				}
			}

			return $template_id;
		}

		/**
		 * Get template content for preview.
		 * @since 4.5
		 *
		 * @param $template_id
		 * @param $template_type
		 *
		 * @return string
		 */
		public function getTemplateContentPreview( $template_id, $template_type ) {
			if ( self::$template_type === $template_type ) {
				WPBMap::addAllMappedShortcodes();
				// do something to return output of templatera template
				$post = get_post( $template_id );
				if ( $this->isSamePostType( $post->post_type ) ) {
					return $post->post_content;
				}
			}

			return $template_id;
		}

		/**
		 * Used to render template for frontend
		 * @since 4.4
		 *
		 * @param $template_id
		 * @param $template_type
		 *
		 * @return string|int
		 */
		public function renderFrontendTemplate( $template_id, $template_type ) {
			if ( self::$template_type === $template_type ) {
				WPBMap::addAllMappedShortcodes();
				// do something to return output of templatera template
				$post = get_post( $template_id );
				if ( $this->isSamePostType( $post->post_type ) ) {
					vc_frontend_editor()->enqueueRequired();
					vc_frontend_editor()->setTemplateContent( $post->post_content );
					vc_frontend_editor()->render( 'template' );
					die();
				}
			}

			return $template_id;
		}

		/**
		 * @param $category
		 * @return mixed
		 */
		public function renderTemplateBlock( $category ) {
			
			if ( self::$template_type === $category['category'] ) {
				if ( ! $this->isUserRoleAccessVcVersion() || ( $this->isUserRoleAccessVcVersion() && vc_user_access()->part( 'templates' )->checkStateAny( true, null )->get() ) ) {
					$category['output'] = '
				<div class="vc_column vc_col-sm-12" data-vc-hide-on-search="true">
					<div class="vc_element_label">' . esc_html__( 'Save current layout as a template', 'js_composer' ) . '</div>
					<div class="vc_input-group">
						<input name="padding" class="vc_form-control wpb-textinput vc_panel-templates-name" type="text" value=""
						       placeholder="' . esc_attr( 'Template name', 'js_composer' ) . '">
						<span class="vc_input-group-btn"> <button class="vc_btn vc_btn-primary vc_btn-sm vc_template-save-btn">' . esc_html__( 'Save template', 'js_composer' ) . '</button></span>
					</div>
					<span class="vc_description">' . esc_html__( 'Save your layout and reuse it on different sections of your website', 'js_composer' ) . '</span>
				</div>';
				}
				$category['output'] .= '<div class="vc_col-md-12">';
				if ( isset( $category['category_name'] ) ) {
					$category['output'] .= '<h3>' . esc_html( $category['category_name'] ) . '</h3>';
				}
				if ( isset( $category['category_description'] ) ) {
					$category['output'] .= '<p class="vc_description">' . esc_html( $category['category_description'] ) . '</p>';
				}
				$category['output'] .= '</div>';
				$category['output'] .= '
			<div class="vc_column vc_col-sm-12">
			<ul class="vc_templates-list-my_templates">';
				if ( ! empty( $category['templates'] ) ) {
					foreach ( $category['templates'] as $template ) {
						$category['output'] .= visual_composer()->templatesPanelEditor()->renderTemplateListItem( $template );
					}
				}
				$category['output'] .= '</ul></div>';
			} elseif ( 'engage_templates' === $category['category'] ) { // Engage Templates
				
				$category['output'] = '<div class="vc_column vc_col-sm-12 vc_column_engage" data-vc-hide-on-search="true">';
				
				$templates = array(
//					'img_text_1_right' => array(
//						'title' => __( 'Image + Text 1', 'engage' ),
//						'desc' => __( 'Content section with image and text.', 'engage' ), 
//						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
//						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493822260304{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_single_image image="2003" img_size="700x600" css_animation="none" css=".vc_custom_1493822366452{margin-bottom: 0px !important;}"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_column_text font_size="medium"]
//						<h3>Learn about us</h2>
//						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_separator][vc_column_text]I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][/vc_row]',
//					),
					'img_text_1_right' => array(
						'title' => __( 'Image + Text 1', 'engage' ),
						'desc' => __( 'Content section with image and text.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493822260304{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="700x600" css_animation="none" css=".vc_custom_1493822366452{margin-bottom: 0px !important;}"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_column_text font_size="medium"]
						<h3>Learn about us</h3>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_separator][vc_column_text]I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][/vc_row]',
					),
					'img_text_btn_right' => array(
						'title' => __( 'Image + Text + Btn', 'engage' ),
						'desc' => __( 'Content section with image, text and button.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493822260304{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="700x600" css_animation="none" css=".vc_custom_1493822366452{margin-bottom: 0px !important;}"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_column_text]
						<h3>Learn about us</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_button label="Learn More" icon_enabled="yes" icon_fontawesome="fa fa-long-arrow-right" icon_style="right_side_hover" url="#"][/vc_column][/vc_row]',
					),
					'img_text_1_left' => array(
						'title' => __( 'Text + Image 1', 'engage' ),
						'desc' => __( 'Content section with image and text.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493822310034{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_column_text font_size="medium"]
						<h3>Learn about us</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_separator][vc_column_text]I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="700x600" css_animation="none" css=".vc_custom_1493822376054{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'img_text_btn_left' => array(
						'title' => __( 'Text + Btn + Image', 'engage' ),
						'desc' => __( 'Content section with image, text and button.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493822260304{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_column_text]
						<h3>Learn about us</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_button label="Learn More" icon_enabled="yes" icon_fontawesome="fa fa-long-arrow-right" icon_style="right_side_hover" url="#"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="700x600" css_animation="none" css=".vc_custom_1493822366452{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'img_text_fullwidth' => array(
						'title' => __( 'Image + Text Fullwidth', 'engage' ),
						'desc' => __( 'Fullwidth content section with image and text.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" equal_height="yes" content_placement="middle" css=".vc_custom_1493830181567{padding-top: 0px !important;padding-bottom: 0px !important;}"][vc_column width="1/2" col_padding="7" css=".vc_custom_1493831432964{padding-top: 20px !important;padding-bottom: 20px !important;}"][vc_column_text font_size="medium"]
						<h3>Learn about us</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_separator][vc_column_text css=".vc_custom_1493830901966{margin-bottom: 0px !important;}"]I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][vc_column width="1/2" col_padding_side="left" css=".vc_custom_1493831622616{background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-10-1600x1200.jpg) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][/vc_column][/vc_row][vc_row full_width="stretch_row_content_no_spaces" equal_height="yes" content_placement="middle" css=".vc_custom_1493830181567{padding-top: 0px !important;padding-bottom: 0px !important;}"][vc_column width="1/2" css=".vc_custom_1493831227322{background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-9-1600x1200.jpg) !important;}"][/vc_column][vc_column width="1/2" col_padding="7" css=".vc_custom_1493831485949{padding-top: 20px !important;padding-bottom: 20px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column_text font_size="medium"]<h3>Learn about us</h2>We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_separator][vc_column_text css=".vc_custom_1493830901966{margin-bottom: 0px !important;}"]I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][/vc_row]',
					),
					'img_text_btn_fullwidth' => array(
						'title' => __( 'Image, Text Fullwidth 2', 'engage' ),
						'desc' => __( 'Fullwidth content section with image and text.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" equal_height="yes" content_placement="middle" css=".vc_custom_1493830181567{padding-top: 0px !important;padding-bottom: 0px !important;}"][vc_column width="1/2" col_padding="7" css=".vc_custom_1493831472285{padding-top: 20px !important;padding-bottom: 20px !important;}"][vc_column_text]
						<h3>Learn about us</h2>
						[/vc_column_text][vc_column_text]We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_button label="Learn More" icon_enabled="yes" icon_fontawesome="fa fa-long-arrow-right" icon_style="right_side_hover" url="#"][/vc_column][vc_column width="1/2" col_padding_side="left" css=".vc_custom_1493831129794{background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-9-1600x1200.jpg) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][/vc_column][/vc_row][vc_row full_width="stretch_row_content_no_spaces" equal_height="yes" content_placement="middle" css=".vc_custom_1493830181567{padding-top: 0px !important;padding-bottom: 0px !important;}"][vc_column width="1/2" css=".vc_custom_1493831638175{background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-10-1600x1200.jpg) !important;}"][/vc_column][vc_column width="1/2" col_padding="7" css=".vc_custom_1493831479492{padding-top: 20px !important;padding-bottom: 20px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column_text]<h3>Learn about us</h2>[/vc_column_text][vc_column_text]We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_button label="Learn More" icon_enabled="yes" icon_fontawesome="fa fa-long-arrow-right" icon_style="right_side_hover" url="#"][/vc_column][/vc_row]',
					),
					'img_text_img_iso' => array(
						'title' => __( 'Text + Isolated Image', 'engage' ),
						'desc' => __( 'Text with an isolated image.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494859213358{padding-top: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_column_text font_size="medium"]
						<h3>Learn about us</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_separator][vc_column_text]I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/isolated-happy-man.jpg" css_animation="none" css=".vc_custom_1494859204969{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'img_text_right_img_iso' => array(
						'title' => __( 'Isolated Image', 'engage' ),
						'desc' => __( 'Isolated image with a text.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494859213358{padding-top: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/isolated-happy-man2.jpg" css_animation="none" css=".vc_custom_1494859417064{margin-bottom: 0px !important;}"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_column_text font_size="medium"]
						<h3>Learn about us</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_button label="Learn More" icon_enabled="yes" icon_fontawesome="fa fa-long-arrow-right" icon_style="right_side_hover" url="#"][/vc_column][/vc_row]',
					),
					'img_text_right_list' => array(
						'title' => __( 'Image with Text and List', 'engage' ),
						'desc' => __( 'Image with text and a list.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" bg_color_pre="bg-color-1" css=".vc_custom_1494860929308{padding-top: 90px !important;padding-bottom: 60px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right" css=".vc_custom_1494861025506{padding-top: 0px !important;}"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-6.jpg" img_size="700x660"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left" css=".vc_custom_1494861039724{padding-top: 20px !important;}"][vc_column_text css=".vc_custom_1487872588606{margin-bottom: 25px !important;}"]
						<h3>Who we are?</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house. We are so happy with this theme. Everyday it make our lives better.[/vc_column_text][vntd_icon_list icons_color="accent" elements="%5B%7B%22icon_fontawesome%22%3A%22fa%20fa-envira%22%2C%22text%22%3A%22We%20care%20about%20environment.%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-users%22%2C%22text%22%3A%22We are%20trusted%20by%20hundreds%20of%20clients.%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-heart%22%2C%22text%22%3A%22Social%20media%20loves%20us!%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-check%22%2C%22text%22%3A%22This%20list%20is%20super%20easy%20to%20create.%22%7D%5D" size="large" border="off"][/vc_column][/vc_row]',
					),
					'img_text_list' => array(
						'title' => __( 'Text with a list and image', 'engage' ),
						'desc' => __( 'Text with a list and image.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" parallax="content-moving" parallax_speed_bg="1.3" css=".vc_custom_1494860920894{padding-top: 90px !important;padding-bottom: 90px !important;background-position: center;background-repeat: no-repeat;background-size: cover !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right" css=".vc_custom_1494861044583{padding-top: 20px !important;}"][vc_column_text]
						<h3>Great Results.</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house. We are so happy with this theme. Everyday it make our lives better.[/vc_column_text][vntd_list items="Aliquam fermentum lorem quis posuere mattis.,Sed mollis sapien erat id pellentesque libero.,Pellentesque nisl id semper bibendum." icon="fa fa-thumbs-o-up"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="700x600" css=".vc_custom_1494861073284{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'img_text_btn_icon' => array(
						'title' => __( 'Aligned text with image', 'engage' ),
						'desc' => __( 'Text block with an image.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494844972270{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="7/12" col_padding="2" col_padding_side="right"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" img_size="800x660" css_animation="none" css=".vc_custom_1494844610700{margin-bottom: 0px !important;}"][/vc_column][vc_column width="5/12" col_padding="2" col_padding_side="left"][vc_icon type="linecons" icon_linecons="vc_li vc_li-paperplane" css=".vc_custom_1494845054727{margin-bottom: 15px !important;margin-left: -13px !important;}"][vc_custom_heading text="We rock at optimisation." google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:400%20regular%3A400%3Anormal"][vc_column_text font_size="medium"]Nunc id ante quis tellus faucibus dictum in eget metus. Duis suscipit elit sem, sed mattis tellus accumsan eget. Quisque consequat venenatis rutrum. Quisque posuere enim augue, in rhoncus diam dictum non.[/vc_column_text][vntd_button label="Learn More" style="solid" color="blue" icon_enabled="yes" icon_fontawesome="fa fa-angle-right" icon_style="right_side_hover" url="#"][/vc_column][/vc_row]',
					),
					'img_text_center_icon' => array(
						'title' => __( 'Centered text with image', 'engage' ),
						'desc' => __( 'Centered text block with an image.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494845225544{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="1/2" col_padding="1" col_padding_side="right"][vc_icon type="linecons" icon_linecons="vc_li vc_li-paperplane" align="center" css=".vc_custom_1494845317040{margin-bottom: 15px !important;}"][vc_custom_heading text="We rock at optimisation." font_container="tag:h3|font_size:28px|text_align:center" google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:400%20regular%3A400%3Anormal"][vc_column_text]
						<p style="text-align: center;">Nunc id ante quis tellus faucibus dictum in eget metus. Duis suscipit elit sem, sed mattis tellus accumsan eget. Quisque consequat venenatis rutrum. Quisque posuere enim augue, in rhoncus diam dictum non.</p>
						[/vc_column_text][vntd_button label="Learn More" style="solid" color="blue" align="center" icon_enabled="yes" icon_fontawesome="fa fa-angle-right" icon_style="right_side_hover" url="#"][/vc_column][vc_column width="1/2" col_padding="1" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-6.jpg" img_size="700x600" css_animation="none" css=".vc_custom_1494845385184{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'img_mockup_text' => array(
						'title' => __( 'Text block + Mockup Image', 'engage' ),
						'desc' => __( 'Text block with buttons and mockup image.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image', 'Hero Section' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" color_scheme="white" css=".vc_custom_1494846045404{padding-top: 100px !important;padding-bottom: 70px !important;background-color: #2196f3 !important;}"][vc_column width="5/12" col_padding="1" col_padding_side="right"][vc_custom_heading text="Hello! We are the best company in the area." font_container="tag:h2|font_size:36px|text_align:left|line_height:1.44em" use_theme_fonts="yes" css=".vc_custom_1494846002664{margin-bottom: 20px !important;}"][vc_column_text font_size="medium"]
						<p style="text-align: left;">Nunc id ante quis tellus faucibus dictum in eget metus. Duis suscipit elit sem, sed mattis tellus accumsan eget. Quisque consequat venenatis rutrum.</p>
						[/vc_column_text][vntd_button label="Learn More" style="solid" color="white" color_hover="accent" display="inline" icon_fontawesome="fa fa-angle-right" url="#"][vntd_button style="outline" color="white" color_hover="white" display="inline"][/vc_column][vc_column width="7/12" col_padding="1" col_padding_side="left"][vc_single_image image="https://s3.us-east-2.amazonaws.com/veented.com/laptop-floating.png" css_animation="none" css=".vc_custom_1494845485998{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'img_text_btn_icon' => array(
						'title' => __( 'Aligned text with image', 'engage' ),
						'desc' => __( 'Text block with an image.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494844972270{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="7/12" col_padding="2" col_padding_side="right"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" img_size="800x660" css_animation="none" css=".vc_custom_1494844610700{margin-bottom: 0px !important;}"][/vc_column][vc_column width="5/12" col_padding="2" col_padding_side="left"][vc_icon type="linecons" icon_linecons="vc_li vc_li-paperplane" css=".vc_custom_1494845054727{margin-bottom: 15px !important;margin-left: -13px !important;}"][vc_custom_heading text="We rock at optimisation." google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:400%20regular%3A400%3Anormal"][vc_column_text font_size="medium"]Nunc id ante quis tellus faucibus dictum in eget metus. Duis suscipit elit sem, sed mattis tellus accumsan eget. Quisque consequat venenatis rutrum. Quisque posuere enim augue, in rhoncus diam dictum non.[/vc_column_text][vntd_button label="Learn More" style="solid" color="blue" icon_enabled="yes" icon_fontawesome="fa fa-angle-right" icon_style="right_side_hover" url="#"][/vc_column][/vc_row]',
					),
					'heading_img_center' => array(
						'title' => __( 'Centered Image', 'engage' ),
						'desc' => __( 'Heading with a big, centered image.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row css=".vc_custom_1493981930868{padding-top: 80px !important;padding-bottom: 70px !important;}"][vc_column][special_heading title="Big Image" subtitle="This is an example of a big, centered image and a heading. Perfect for showcasing something outstanding and awesome!" c_margin_bottom="60px" font_size="28px" subtitle_fa="16px"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/full-5.jpg" img_size="960x500" alignment="center" style="vc_box_rounded" css_animation="fadeIn"][/vc_column][/vc_row]',
					),
					'text_img_icon_boxes' => array(
						'title' => __( 'Text, Image, Icon Boxes', 'engage' ),
						'desc' => __( 'Multiple text content.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image', 'Icon Boxes' ),
						'sc' => '[vc_row css=".vc_custom_1494849446355{padding-top: 80px !important;padding-bottom: 45px !important;}"][vc_column][vc_row_inner equal_height="yes" content_placement="middle"][vc_column_inner width="1/2" css=".vc_custom_1494849276341{padding-right: 1% !important;}"][vc_column_text css=".vc_custom_1494848815057{margin-bottom: 15px !important;}"]
						<h3>Our incredible features.</h2>
						[/vc_column_text][vc_column_text font_size="large"]Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_single_image image="https://s3.us-east-2.amazonaws.com/veented.com/ipad.png" alignment="center" css=".vc_custom_1494849416688{margin-bottom: 0px !important;}"][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1494849123858{margin-top: 40px !important;}"][vc_column_inner width="1/4"][icon_box title="Powerful Options" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Responsive Design" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Satisfied Clients" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-heart"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Premium Support" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-study"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'text_slider_icon_boxes' => array(
						'title' => __( 'Image Slider, Text, Icon Boxes', 'engage' ),
						'desc' => __( 'Text with icon boxes and image slider.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image', 'Icon Boxes', 'Slider' ),
						'sc' => '[vc_row css=".vc_custom_1494849953524{padding-top: 90px !important;padding-bottom: 50px !important;}"][vc_column][vc_row_inner equal_height="yes" content_placement="middle" css=".vc_custom_1494851068829{margin-bottom: 40px !important;}"][vc_column_inner width="7/12" css=".vc_custom_1494849698699{padding-right: 5% !important;}"][engage_image_slider images="http://media.veented.com/wp-content/uploads/2017/08/full-8.jpeg,http://media.veented.com/wp-content/uploads/2017/09/full-16.jpg" img_size="700x420"][/vc_column_inner][vc_column_inner width="5/12" css=".vc_custom_1494849694618{padding-left: 5% !important;}"][vc_column_text css=".vc_custom_1494851089676{margin-bottom: 16px !important;}"]
						<h3>Incredible Office.</h2>
						[/vc_column_text][vc_column_text]Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna. Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner width="1/3"][icon_box title="Powerful Options" text="Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt. Aenean vehicula, dolor eget posuere luctus." style="centered-circle" icon_align="left" icon_size="sm" icon_type="linecons" link_label="Learn More" icon_linecons="vc_li vc_li-params" url="#"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Responsive Design" text="Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt. Aenean vehicula, dolor eget posuere luctus." style="centered-circle" icon_align="left" icon_size="sm" icon_type="linecons" link_label="Learn More" icon_linecons="vc_li vc_li-display" url="#"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Page Builder" text="Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt. Aenean vehicula, dolor eget posuere luctus." style="centered-circle" icon_align="left" icon_size="sm" icon_type="linecons" link_label="Learn More" icon_linecons="vc_li vc_li-pen" url="#"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'text_multi_image' => array(
						'title' => __( 'Text Multi and Image', 'engage' ),
						'desc' => __( 'Text with icon boxes and image slider.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image', 'Icon Boxes' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494851871081{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="7/12" col_padding="2" col_padding_side="right"][vc_column_text css=".vc_custom_1494851946125{margin-bottom: 18px !important;}"]
						<h3>What is our goal?</h2>
						[/vc_column_text][vc_column_text font_size="medium"]
						<p class="p1">Aliquam imperdiet egestas nisi ultrices tristique. Maecenas vestibulum rutrum luctus. Quisque at viverra felis. Aliquam pharetra lacus non sem ullamcorper tempus. Donec quis commodo ligula, eu aliquet elit.</p>
						[/vc_column_text][vc_row_inner css=".vc_custom_1494852056203{padding-top: 20px !important;}"][vc_column_inner width="1/2"][vc_column_text css=".vc_custom_1494852050796{margin-bottom: 12px !important;}"]
						<h5>Premium Support</h5>
						[/vc_column_text][vc_column_text]
						<p class="p1">Curabitur in ante odio. Quisque facilisis elementum ipsum id tincidunt. Cras auctor consectetur pharetra.</p>
						[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text css=".vc_custom_1494852136754{margin-bottom: 12px !important;}"]
						<h5>Powerful Features</h5>
						[/vc_column_text][vc_column_text]
						<p class="p1">Curabitur in ante odio. Quisque facilisis elementum ipsum id tincidunt. Cras auctor consectetur pharetra.</p>
						[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1494852056203{padding-top: 20px !important;}"][vc_column_inner width="1/2"][vc_column_text css=".vc_custom_1494852172479{margin-bottom: 12px !important;}"]
						<h5>Satisfied Clients</h5>
						[/vc_column_text][vc_column_text]
						<p class="p1">Curabitur in ante odio. Quisque facilisis elementum ipsum id tincidunt. Cras auctor consectetur pharetra.</p>
						[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text css=".vc_custom_1494852189991{margin-bottom: 12px !important;}"]
						<h5>Flexible Options</h5>
						[/vc_column_text][vc_column_text]
						<p class="p1">Curabitur in ante odio. Quisque facilisis elementum ipsum id tincidunt. Cras auctor consectetur pharetra.</p>
						[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="5/12" col_padding="1" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" img_size="700x760" css=".vc_custom_1494852267097{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'img_text_mockup' => array(
						'title' => __( 'Mockup image + Text', 'engage' ),
						'desc' => __( 'Mockup image with text.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" bg_color_pre="bg-color-1" css=".vc_custom_1494852565780{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_single_image image="https://s3.us-east-2.amazonaws.com/veented.com/ipad-with-cover-2.png" css=".vc_custom_1494852495509{margin-bottom: 0px !important;}"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_column_text font_size="medium"]
						<h3>Learn about us</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_separator][vc_column_text]I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][/vc_row]',
					),
					'img_bg_text' => array(
						'title' => __( 'Background Image with Text', 'engage' ),
						'desc' => __( 'Aligned text with bg image.', 'engage' ), 
						'cat' => array( 'Text + Image', 'With Image', 'Hero Section' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" bg_overlay="dark30" color_scheme="white" css=".vc_custom_1494836841979{padding-top: 0px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-19.jpg) !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right" css=".vc_custom_1494836926761{padding-top: 155px !important;padding-bottom: 140px !important;}"][vc_column_text font_size="medium"]
						<h1>Hello there.</h1>
						<p class="p1">Donec quis rhoncus augue. In fermentum eget neque tristique scelerisque. Morbi at elementum nisi. Quisque pellentesque at enim vitae bibendum. Aliquam imperdiet egestas nisi. Aliquam pharetra lacus non sem ullamcorper tempus.</p>
						[/vc_column_text][vntd_button label="Learn More" style="outline" color="white" color_hover="white" icon_enabled="yes" icon_fontawesome="fa fa-long-arrow-right" icon_style="right_side_hover" url="#"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][/vc_column][/vc_row]',
					),
					'img_text_mockup_gd' => array(
						'title' => __( 'Mockup image + Text', 'engage' ),
						'desc' => __( 'Mockup image with text over gradient.', 'engage' ), 
						'cat' => array( 'Content', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" color_scheme="white" bg_gradient1="#b06ab3" bg_gradient2="#4568dc" css=".vc_custom_1494852488594{padding-top: 90px !important;padding-bottom: 90px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_column_text font_size="medium"]
						<h3>Learn about us</h2>
						We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house and commercial builds and we are registered NHBC house builders.[/vc_column_text][vntd_separator][vc_column_text]I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_single_image image="https://s3.us-east-2.amazonaws.com/veented.com/ipad-with-cover-2.png" css=".vc_custom_1494852495509{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'hero_img_1' => array(
						'title' => __( 'Hero Section Bg Img 1', 'engage' ),
						'desc' => __( 'Hero Section with a background image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row bg_overlay="dark40" parallax="content-moving" css=".vc_custom_1494853158030{padding-top: 220px !important;padding-bottom: 220px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-16.jpg) !important;}"][vc_column][vc_custom_heading text="Smaller Text" font_container="tag:h5|font_size:18px|text_align:center|color:rgba(255%2C255%2C255%2C0.8)" google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:600%20bold%20regular%3A600%3Anormal" css=".vc_custom_1494853239264{margin-bottom: 16px !important;}"][vc_custom_heading text="WELCOME TO BING" font_container="tag:h1|font_size:66px|text_align:center|color:%23ffffff" google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:700%20bold%20regular%3A700%3Anormal"][vntd_button label="Learn More" style="solid" color="accent" color_hover="white" border_radius="circle" align="center"][/vc_column][/vc_row]',
					),
					'hero_img_2' => array(
						'title' => __( 'Hero Section Bg Img 2', 'engage' ),
						'desc' => __( 'Hero Section with a background image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row bg_overlay="dark40" parallax="content-moving" css=".vc_custom_1494853541940{padding-top: 220px !important;padding-bottom: 220px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-20.jpg) !important;}"][vc_column][vc_custom_heading text="INCREDIBLE TALENTS." font_container="tag:h5|font_size:18px|text_align:center|color:rgba(255%2C255%2C255%2C0.8)" google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:400%20regular%3A400%3Anormal" css=".vc_custom_1494853571240{margin-bottom: 20px !important;}"][vc_custom_heading text="We are the largest design agency in Los Angeles." font_container="tag:h1|font_size:66px|text_align:center|color:%23ffffff" google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:400%20regular%3A400%3Anormal" css=".vc_custom_1494853549887{padding-right: 10% !important;padding-left: 10% !important;}"][vntd_button label="View our Work" style="outline" color="white" color_hover="white" align="center" url="#" margin_top="16px"][/vc_column][/vc_row]',
					),
					'hero_img_3' => array(
						'title' => __( 'Hero Section Bg Img 3', 'engage' ),
						'desc' => __( 'Hero Section with a background image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row parallax="content-moving" color_scheme="white" bg_overlay="dark30" css=".vc_custom_1494854239991{padding-top: 190px !important;padding-bottom: 190px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-15.jpg) !important;}"][vc_column][special_heading title="Incredible Section" subtitle="Some simple description of the section." c_margin_bottom="0px" font_size="52px" font_weight="300" text_transform="none" subtitle_fs="20px"][/vc_column][/vc_row]',
					),
					'hero_img_4' => array(
						'title' => __( 'Hero Section Bg Img 4', 'engage' ),
						'desc' => __( 'Hero Section with a background image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row][vc_column][vntd_hero_section scroll_btn="true" images="http://media.veented.com/wp-content/uploads/2017/09/full-16.jpg" bg_overlay="dark40" height="custom" height_custom="800px" content_width="narrow" parallax="no"][/vc_column][/vc_row]',
					),
					'hero_video_1' => array(
						'title' => __( 'Hero Video Background', 'engage' ),
						'desc' => __( 'Fullscreen Hero Section with a background image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row][vc_column][vntd_hero_section scroll_btn="true" media_type="youtube" youtube_id="https://www.youtube.com/watch?v=2QKQX7fbjnA" video_img="http://media.veented.com/wp-content/uploads/2017/09/full-14.jpg" bg_overlay="dark40" content_width="narrow" parallax="no"][/vc_column][/vc_row]',
					),
					'hero_ximg_1' => array(
						'title' => __( 'Hero with Image 1', 'engage' ),
						'desc' => __( 'Fullscreen Hero Section with an extra image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row full_height="yes" equal_height="yes" content_placement="middle" parallax="content-moving-fade" color_scheme="white" bg_gradient1="#eecda3" bg_gradient2="#ef629f"][vc_column width="7/12" col_padding="3" col_padding_side="right"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="700x700" alignment="right" style="vc_box_rounded" css=".vc_custom_1494346325598{margin-bottom: 0px !important;}"][/vc_column][vc_column width="5/12" col_padding="3" col_padding_side="left"][vc_custom_heading text="Hello there" font_container="tag:h2|font_size:56px|text_align:left" use_theme_fonts="yes" css=".vc_custom_1494343480109{margin-bottom: 10px !important;}"][vc_column_text font_size="large"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbinus mattis, turpis.[/vc_column_text][vntd_icon_list icons_color="transparent-white" elements="%5B%7B%22icon_fontawesome%22%3A%22fa%20fa-heart%22%2C%22text%22%3A%22Created%20with%20pure%20love.%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-envira%22%2C%22text%22%3A%22Perfect%20responsive%20design.%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-flag%22%2C%22text%22%3A%22Fully%20retina%20ready.%22%7D%5D" size="medium" border="off"][vntd_button color="white" color_hover="accent" style="solid" display="inline"][vntd_button color="white" color_hover="accent" style="outline" display="inline"][/vc_column][/vc_row]',
					),
					'hero_ximg_2' => array(
						'title' => __( 'Hero with Image 2', 'engage' ),
						'desc' => __( 'Fullscreen Hero Section with a mockup image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row full_height="yes" equal_height="yes" content_placement="middle" color_scheme="white" bg_gradient1="#b06ab3" bg_gradient2="#4568dc"][vc_column width="1/2" col_padding="3" col_padding_side="right"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/iphone-white.png" img_size="320x654" alignment="right"][/vc_column][vc_column width="1/2" col_padding="3" col_padding_side="left"][vc_custom_heading text="Hello there" font_container="tag:h2|font_size:56px|text_align:left" use_theme_fonts="yes" css=".vc_custom_1494343480109{margin-bottom: 10px !important;}"][vc_column_text font_size="large"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa wlacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbinus mattis, turpis.[/vc_column_text][vntd_button style="solid" color="white" color_hover="accent" display="inline"][vntd_button style="outline" color="white" color_hover="accent" display="inline"][/vc_column][/vc_row]',
					),
					'hero_ximg_3' => array(
						'title' => __( 'Hero with Image 3', 'engage' ),
						'desc' => __( 'Fullscreen Hero Section with a mockup image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row full_height="yes" equal_height="yes" content_placement="middle" color_scheme="white" bg_gradient1="#eecda3" bg_gradient2="#ef629f"][vc_column width="1/2" col_padding="3" col_padding_side="right"][vc_single_image image="https://s3.us-east-2.amazonaws.com/veented.com/iphone-white.png" img_size="397x800" alignment="right" css=".vc_custom_1494344656778{margin-bottom: 0px !important;}"][/vc_column][vc_column width="1/2" col_padding="3" col_padding_side="left"][vc_custom_heading text="Hello there" font_container="tag:h2|font_size:56px|text_align:left" use_theme_fonts="yes" css=".vc_custom_1494343480109{margin-bottom: 10px !important;}"][vc_column_text font_size="large"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbinus mattis, turpis.[/vc_column_text][vntd_icon_list icons_color="transparent-white" elements="%5B%7B%22icon_fontawesome%22%3A%22fa%20fa-heart%22%2C%22text%22%3A%22Created%20with%20pure%20love.%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-envira%22%2C%22text%22%3A%22Perfect%20responsive%20design.%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-flag%22%2C%22text%22%3A%22Fully%20retina%20ready.%22%7D%5D" size="medium" border="off"][vntd_button style="solid" color="white" color_hover="accent" display="inline"][vntd_button style="outline" color="white" color_hover="accent" display="inline"][/vc_column][/vc_row]',
					),
					'hero_ximg_4' => array(
						'title' => __( 'Hero with Image 4', 'engage' ),
						'desc' => __( 'Fullscreen Hero Section with a mockup image.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row full_height="yes" equal_height="yes" content_placement="middle" color_scheme="white" bg_overlay="dark30" css=".vc_custom_1494343711653{background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-23.jpg) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column width="1/2" col_padding="3" col_padding_side="right"][vc_custom_heading text="Hello there" font_container="tag:h2|font_size:64px|text_align:left" use_theme_fonts="yes" css=".vc_custom_1494343644235{margin-bottom: 10px !important;}"][vc_column_text font_size="large"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbinus mattis, turpis.[/vc_column_text][vntd_button color="white" color_hover="accent" style="solid" display="inline"][vntd_button color="white" color_hover="accent" style="outline" display="inline"][/vc_column][vc_column width="1/2" col_padding="3" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/iphone-black.png" img_size="320x654" alignment="center" css=".vc_custom_1494344651524{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'hero_ximg_5' => array(
						'title' => __( 'Hero with Image 5', 'engage' ),
						'desc' => __( 'Hero Section with an aligned image, dark text.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494346144583{padding-top: 110px !important;padding-bottom: 110px !important;background-color: #e6e6e6 !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column width="1/2" col_padding="3" col_padding_side="right"][vc_custom_heading text="Hello there" font_container="tag:h2|font_size:50px|text_align:left" use_theme_fonts="yes" css=".vc_custom_1494345504182{margin-bottom: 20px !important;}"][vc_column_text font_size="large"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbinus mattis, turpis.[/vc_column_text][vc_column_text css=".vc_custom_1494345103487{margin-bottom: 35px !important;}"]Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo.[/vc_column_text][vntd_button color="accent" color_hover="white" style="solid" display="inline"][vntd_button color="white" color_hover="accent" display="inline"][/vc_column][vc_column width="1/2" col_padding="3" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" img_size="700x700" style="vc_box_rounded" css=".vc_custom_1494345532027{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'hero_ximg_6' => array(
						'title' => __( 'Hero with Image 6', 'engage' ),
						'desc' => __( 'Hero Section with image, dark text.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" bg_gradient1="#ffafbd" bg_gradient2="#ffc3a0" css=".vc_custom_1494346089964{padding-top: 110px !important;padding-bottom: 110px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column width="1/2" col_padding="3" col_padding_side="right"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" img_size="700x700" style="vc_box_rounded" css=".vc_custom_1494345532027{margin-bottom: 0px !important;}"][/vc_column][vc_column width="1/2" col_padding="3" col_padding_side="left"][vc_custom_heading text="Hello there" font_container="tag:h2|font_size:50px|text_align:left" use_theme_fonts="yes" css=".vc_custom_1494345504182{margin-bottom: 20px !important;}"][vc_column_text font_size="large"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbinus mattis, turpis.[/vc_column_text][vc_column_text css=".vc_custom_1494346129847{margin-bottom: 35px !important;}"]Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo.[/vc_column_text][vntd_button color="dark" color_hover="white" style="solid" display="inline"][vntd_button color="dark" color_hover="dark" style="outline" display="inline"][/vc_column][/vc_row]',
					),
					'hero_xvid_1' => array(
						'title' => __( 'Hero with Video 1', 'engage' ),
						'desc' => __( 'Hero Section with extra video.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section', 'Video' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" color_scheme="white" bg_gradient1="#ac75d6" bg_gradient2="#7d9cdb" css=".vc_custom_1494347111388{padding-top: 120px !important;padding-bottom: 120px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column width="1/2" col_padding="3" col_padding_side="right"][video_lightbox style="img" link="https://www.youtube.com/watch?v=ty0SGNZi81U" img="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" border="round"][/vc_column][vc_column width="1/2" col_padding="3" col_padding_side="left"][vc_custom_heading text="Hello there" font_container="tag:h2|font_size:50px|text_align:left" use_theme_fonts="yes" css=".vc_custom_1494345504182{margin-bottom: 20px !important;}"][vc_column_text font_size="large"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbinus mattis, turpis.[/vc_column_text][vntd_button color="white" color_hover="white" style="solid" display="inline"][vntd_button color="white" style="outline" display="inline"][/vc_column][/vc_row]',
					),
					'hero_xvid_2' => array(
						'title' => __( 'Hero with Video 2', 'engage' ),
						'desc' => __( 'Hero Section with extra video, dark text.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section', 'Video' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494347177433{padding-top: 120px !important;padding-bottom: 120px !important;background-color: #f7f7f7 !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column width="1/2" col_padding="3" col_padding_side="right"][video_lightbox style="img" link="https://www.youtube.com/watch?v=ty0SGNZi81U" img="http://media.veented.com/wp-content/uploads/2017/09/full-12.jpg" border="round"][/vc_column][vc_column width="1/2" col_padding="3" col_padding_side="left"][vc_custom_heading text="Hello there" font_container="tag:h2|font_size:50px|text_align:left" use_theme_fonts="yes" css=".vc_custom_1494345504182{margin-bottom: 20px !important;}"][vc_column_text font_size="large"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbinus mattis, turpis.[/vc_column_text][vntd_button color="dark" color_hover="white" style="solid" display="inline"][/vc_column][/vc_row]',
					),
					'hero_text_center' => array(
						'title' => __( 'Hero Gradient Background', 'engage' ),
						'desc' => __( 'Hero Section with centered text, gradient.', 'engage' ), 
						'cat' => array( 'Content', 'Hero Section', 'Plain Text' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" parallax="content-moving-fade" color_scheme="white" bg_gradient1="#eecda3" bg_gradient2="#ef629f" css=".vc_custom_1494346688168{padding-top: 160px !important;padding-bottom: 160px !important;}"][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_custom_heading text="Hello there!" font_container="tag:h2|font_size:56px|text_align:center" use_theme_fonts="yes" css=".vc_custom_1494346737499{margin-bottom: 20px !important;}"][vc_column_text font_size="large" css=".vc_custom_1494346719422{margin-bottom: 40px !important;}"]
						<p style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donecore ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia.</p>
						[/vc_column_text][vntd_button color="white" style="solid" align="center"][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]',
					),
					'hero_slider_img' => array(
						'title' => __( 'Hero Slider Image', 'engage' ),
						'desc' => __( 'Hero Slider with two image slides.', 'engage' ), 
						'cat' => array( 'Hero Section', 'Slider' ),
						'sc' => '[vc_row][vc_column][vntd_hero_slider slides="%5B%7B%22image%22%3A%22http://media.veented.com/wp-content/uploads/2017/09/full-23.jpg%22%2C%22heading%22%3A%22Welcome%2C%20Stranger!%22%2C%22text%22%3A%22This%20is%20an%20example%20slide%20text%20content%2C%20feel%20free%20to%20change%20it.%22%2C%22btn_label%22%3A%22Learn%20More%22%2C%22btn_url%22%3A%22%23%22%2C%22align%22%3A%22center%22%2C%22color%22%3A%22white%22%2C%22bg_overlay%22%3A%22dark30%22%7D%2C%7B%22image%22%3A%22http://media.veented.com/wp-content/uploads/2017/09/full-21b.jpg%22%2C%22heading%22%3A%22Aligned%20Heading%22%2C%22text%22%3A%22This%20is%20another%20example%20slide%20text%20content%2C%20feel%20free%20to%20change%20it.%22%2C%22btn_label%22%3A%22Learn%20More%22%2C%22btn_url%22%3A%22%23%22%2C%22align%22%3A%22left%22%2C%22color%22%3A%22white%22%2C%22bg_overlay%22%3A%22dark20%22%7D%5D"][/vc_column][/vc_row]',
					),
					'hero_slider_img_align' => array(
						'title' => __( 'Hero Slider Image 2', 'engage' ),
						'desc' => __( 'Hero Slider with 2 image slides.', 'engage' ), 
						'cat' => array( 'Hero Section', 'Slider' ),
						'sc' => '[vc_row][vc_column][vntd_hero_slider slides="%5B%7B%22image%22%3A%22http://media.veented.com/wp-content/uploads/2017/09/full-17.jpg%22%2C%22heading%22%3A%22Aligned%20Heading%22%2C%22text%22%3A%22This%20is%20another%20example%20slide%20text%20content%2C%20feel%20free%20to%20change%20it.%22%2C%22btn_label%22%3A%22Learn%20More%22%2C%22btn_url%22%3A%22%23%22%2C%22align%22%3A%22left%22%2C%22color%22%3A%22white%22%2C%22bg_overlay%22%3A%22dark20%22%2C%22bg_color%22%3A%22%2356ccf2%22%2C%22bg_color2%22%3A%22%232f80ed%22%7D%2C%7B%22image%22%3A%222003%22%2C%22heading%22%3A%22Slide%20Heading%22%2C%22text%22%3A%22This%20is%20an%20example%20slide%20text%20content%2C%20feel%20free%20to%20change%20it.%22%2C%22btn_label%22%3A%22Learn%20More%22%2C%22btn_url%22%3A%22%23%22%2C%22align%22%3A%22center%22%2C%22color%22%3A%22white%22%2C%22bg_overlay%22%3A%22dark30%22%7D%5D"][/vc_column][/vc_row]',
					),
					'hero_slider_gd' => array(
						'title' => __( 'Hero Slider Gradient', 'engage' ),
						'desc' => __( 'Hero Slider with two gradient slides.', 'engage' ), 
						'cat' => array( 'Hero Section', 'Slider' ),
						'sc' => '[vc_row][vc_column][vntd_hero_slider slides="%5B%7B%22heading%22%3A%22Slide%20Heading%22%2C%22text%22%3A%22This%20is%20an%20example%20slide%20text%20content%2C%20feel%20free%20to%20change%20it.%22%2C%22btn_label%22%3A%22Learn%20More%22%2C%22btn_url%22%3A%22%23%22%2C%22align%22%3A%22center%22%2C%22color%22%3A%22white%22%2C%22bg_overlay%22%3A%22none%22%2C%22bg_color%22%3A%22%23b06ab3%22%2C%22bg_color2%22%3A%22%234568dc%22%7D%2C%7B%22heading%22%3A%22Second%20Heading%22%2C%22text%22%3A%22This%20is%20another%20example%20slide%20text%20content%2C%20feel%20free%20to%20change%20it.%22%2C%22btn_label%22%3A%22Learn%20More%22%2C%22btn_url%22%3A%22%23%22%2C%22align%22%3A%22left%22%2C%22color%22%3A%22white%22%2C%22bg_overlay%22%3A%22none%22%2C%22bg_color%22%3A%22%2356ccf2%22%2C%22bg_color2%22%3A%22%232f80ed%22%7D%5D"][/vc_column][/vc_row]',
					),
					'features1' => array(
						'title' => __( 'Features 1', 'engage' ),
						'desc' => __( 'Image surrounded by outline icon boxes.', 'engage' ), 
						'cat' => array( 'Content', 'Icon Boxes', 'With Image', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1487784847412{padding-top: 75px !important;padding-bottom: 70px !important;}"][vc_column][special_heading title="Discover Core Features" subtitle="We are a fairly small, flexible design studio that designs for print and web. Whether you need to create a brand from scratch, including marketing materials." c_margin_bottom="45px"][vc_row_inner css=".vc_custom_1493912815556{padding-top: 20px !important;}"][vc_column_inner width="1/3" css=".vc_custom_1493912933501{padding-top: 125px !important;padding-right: 20px !important;}"][icon_box title="Responsive Design" text="Engage is a fully responsive theme that scales well to all mobile devices." style="aligned-right-outline" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-display"][icon_box title="Page Builder" text="Build any layout you can think of with the most powerful page builder." style="aligned-right-outline" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-pen"][icon_box title="Styling Options" text="Adjust the looks of your theme with the extensive Styling panel." style="aligned-right-outline" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-diamond"][icon_box title="Demo Content" text="All presented demo pages are available with a mouse click." style="aligned-right-outline" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-megaphone"][/vc_column_inner][vc_column_inner width="1/3"][vc_single_image image="2065" alignment="center"][/vc_column_inner][vc_column_inner width="1/3" css=".vc_custom_1493912928631{padding-top: 125px !important;padding-left: 20px !important;}"][icon_box title="Powerful Options" text="Take a full control over all aspects of your theme with our truly extensive." style="aligned-left-outline" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-params"][icon_box title="Google Fonts" text="You may choose any font from the extensive Google Fonts library." style="aligned-left-outline" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-star"][icon_box title="Free Support" text="Our top-notch support team will be happy to answer any of your questions." style="aligned-left-outline" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-bubble"][icon_box title="Documentation" text="Everything you need to know about the theme is in the theme docs." style="aligned-left-outline" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-news"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'features2' => array(
						'title' => __( 'Features 2', 'engage' ),
						'desc' => __( 'Image surrounded by icon boxes.', 'engage' ), 
						'cat' => array( 'Content', 'Icon Boxes', 'With Image', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1487784847412{padding-top: 75px !important;padding-bottom: 70px !important;}"][vc_column][special_heading title="Discover Core Features" subtitle="We are a fairly small, flexible design studio that designs for print and web. Whether you need to create a brand from scratch, including marketing materials." c_margin_bottom="45px"][vc_row_inner css=".vc_custom_1493912815556{padding-top: 20px !important;}"][vc_column_inner width="1/3" css=".vc_custom_1493912933501{padding-top: 125px !important;padding-right: 20px !important;}"][icon_box title="Responsive Design" text="Engage is a fully responsive theme that scales well to all mobile devices." style="aligned-right-circle" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-display"][icon_box title="Page Builder" text="Build any layout you can think of with the most powerful page builder." style="aligned-right-circle" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-pen"][icon_box title="Styling Options" text="Adjust the looks of your theme with the extensive Styling panel." style="aligned-right-circle" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-diamond"][icon_box title="Demo Content" text="All presented demo pages are available with a mouse click." style="aligned-right-circle" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-megaphone"][/vc_column_inner][vc_column_inner width="1/3"][vc_single_image image="2065" alignment="center"][/vc_column_inner][vc_column_inner width="1/3" css=".vc_custom_1493912928631{padding-top: 125px !important;padding-left: 20px !important;}"][icon_box title="Powerful Options" text="Take a full control over all aspects of your theme with our truly extensive." style="aligned-left-circle" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-params"][icon_box title="Google Fonts" text="You may choose any font from the extensive Google Fonts library." style="aligned-left-circle" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-star"][icon_box title="Free Support" text="Our top-notch support team will be happy to answer any of your questions." style="aligned-left-circle" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-bubble"][icon_box title="Documentation" text="Everything you need to know about the theme is in the theme docs." style="aligned-left-circle" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-news"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'features3' => array(
						'title' => __( 'Features 3', 'engage' ),
						'desc' => __( 'Image surrounded by small icon boxes.', 'engage' ), 
						'cat' => array( 'Content', 'Icon Boxes', 'With Image', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1493915354445{padding-top: 78px !important;padding-bottom: 90px !important;}"][vc_column][special_heading title="Discover Core Features" subtitle="We are a fairly small, flexible design studio that designs for print and web. Whether you need to create a brand from scratch, including marketing materials." c_margin_bottom="45px"][vc_row_inner equal_height="yes" content_placement="middle" css=".vc_custom_1493915392084{padding-top: 20px !important;}"][vc_column_inner width="1/3" css=".vc_custom_1493915482883{padding-top: 35px !important;padding-right: 25px !important;}"][icon_box title="Responsive Design" text="Engage is a fully responsive theme that scales well to all mobile devices." style="aligned-right-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-display"][icon_box title="Page Builder" text="Build any layout you can think of with the most powerful page builder." style="aligned-right-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-pen"][icon_box title="Styling Options" text="Adjust the looks of your theme with the extensive Styling panel." style="aligned-right-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-diamond"][/vc_column_inner][vc_column_inner width="1/3"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" img_size="380x600" alignment="center" style="vc_box_rounded" css=".vc_custom_1493915302309{margin-bottom: 0px !important;}"][/vc_column_inner][vc_column_inner width="1/3" css=".vc_custom_1493915478474{padding-top: 35px !important;padding-left: 25px !important;}"][icon_box title="Powerful Options" text="Take a full control over all aspects of your theme with our truly extensive." style="aligned-left-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-params"][icon_box title="Google Fonts" text="You may choose any font from the extensive Google Fonts library." style="aligned-left-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-star"][icon_box title="Free Support" text="Our top-notch support team will be happy to answer any of your questions." style="aligned-left-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-bubble"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'features_img_3cols_1' => array(
						'title' => __( 'Features with images 1', 'engage' ),
						'desc' => __( 'Row of boxed features, gray bg.', 'engage' ), 
						'cat' => array( 'Content', 'With Image', 'Features/Services' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1494842959159{padding-top: 70px !important;padding-bottom: 60px !important;}"][vc_column][special_heading title="Our Features" c_margin_bottom="55px" subtitle_fs="20px"][vc_row_inner][vc_column_inner width="1/3"][vntd_content_box img="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" caption_style="boxed_no_border" text="Monec quis rhoncus augue. In fermentum eget neque tristique scelerisque. Morbi at elemen nisi. Quisque pellentesque at enim vitae."][/vc_column_inner][vc_column_inner width="1/3"][vntd_content_box img="2003" title="Cool Ideas" caption_style="boxed_no_border" text="Monec quis rhoncus augue. In fermentum eget neque tristique scelerisque. Morbi at elemen nisi. Quisque pellentesque at enim vitae."][/vc_column_inner][vc_column_inner width="1/3"][vntd_content_box img="2037" title="Premium Support" caption_style="boxed_no_border" text="Monec quis rhoncus augue. In fermentum eget neque tristique scelerisque. Morbi at elemen nisi. Quisque pellentesque at enim vitae."][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'features_img_3cols_2' => array(
						'title' => __( 'Features with images 2', 'engage' ),
						'desc' => __( 'Row of features with images.', 'engage' ), 
						'cat' => array( 'Content', 'With Image', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1494842669698{padding-top: 70px !important;padding-bottom: 70px !important;}"][vc_column][special_heading title="Our Features" c_margin_bottom="55px" subtitle_fs="20px"][vc_row_inner][vc_column_inner width="1/3"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="600x400"][vc_column_text css=".vc_custom_1494838670605{margin-bottom: 14px !important;}"]
						<h5>First Feature</h5>
						[/vc_column_text][vc_column_text css=".vc_custom_1494839845723{margin-bottom: 18px !important;}"]
						<p class="p1">Donec quis rhoncus augue. In fermentum eget neque tristique scelerisque. Morbi at elementum nisi. Quisque pellentesque at enim vitae bibendum.</p>
						[/vc_column_text][vntd_button label="Learn More" style="text-btn" icon_enabled="yes" icon_fontawesome="fa fa-angle-right" icon_style="right_side" url="#"][/vc_column_inner][vc_column_inner width="1/3"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/08/blake-parkinson-31143.jpg" img_size="600x400"][vc_column_text css=".vc_custom_1494838710775{margin-bottom: 14px !important;}"]
						<h5>Responsive Design</h5>
						[/vc_column_text][vc_column_text css=".vc_custom_1494842395611{margin-bottom: 18px !important;}"]
						<p class="p1">Donec quis rhoncus augue. In fermentum eget neque tristique scelerisque. Morbi at elementum nisi. Quisque pellentesque at enim vitae bibendum.</p>
						[/vc_column_text][vntd_button label="Learn More" style="text-btn" icon_enabled="yes" icon_fontawesome="fa fa-angle-right" icon_style="right_side" url="#"][/vc_column_inner][vc_column_inner width="1/3"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-6.jpg" img_size="600x400"][vc_column_text css=".vc_custom_1494838726908{margin-bottom: 14px !important;}"]
						<h5>Premium Support</h5>
						[/vc_column_text][vc_column_text css=".vc_custom_1494842404611{margin-bottom: 18px !important;}"]
						<p class="p1">Donec quis rhoncus augue. In fermentum eget neque tristique scelerisque. Morbi at elementum nisi. Quisque pellentesque at enim vitae bibendum.</p>
						[/vc_column_text][vntd_button label="Learn More" style="text-btn" icon_enabled="yes" icon_fontawesome="fa fa-angle-right" icon_style="right_side" url="#"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'features_img_3cols_3' => array(
						'title' => __( 'Features with images 3', 'engage' ),
						'desc' => __( 'Row of image and descriptions, centered.', 'engage' ), 
						'cat' => array( 'Content', 'With Image', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1494854759706{padding-top: 70px !important;padding-bottom: 50px !important;}"][vc_column][special_heading title="Incredible features." subtitle="Learn more about them. We have worked truly hard to make them perfect for every use." c_margin_bottom="60px"][vc_row_inner][vc_column_inner width="1/3" css=".vc_custom_1494854641565{padding-right: 20px !important;}"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-12.jpg" img_size="600x500"][vc_column_text css=".vc_custom_1494854714076{margin-bottom: 15px !important;}"]
						<h4 style="text-align: center;">Premium Support</h4>
						[/vc_column_text][vc_column_text]
						<p class="p1" style="text-align: center;">Proin interdum, ante ut sollicitudin commodo, tellus quam sagittis libero, at semper mauris velit a velit. Phasellus commodo turpis et lacinia posuere.</p>
						[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3" css=".vc_custom_1494854662963{padding-right: 20px !important;padding-left: 20px !important;}"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-6.jpg" img_size="600x500"][vc_column_text css=".vc_custom_1494854727910{margin-bottom: 15px !important;}"]
						<h4 style="text-align: center;">Satisfied Clients</h4>
						[/vc_column_text][vc_column_text]
						<p class="p1" style="text-align: center;">Proin interdum, ante ut sollicitudin commodo, tellus quam sagittis libero, at semper mauris velit a velit. Phasellus commodo turpis et lacinia posuere.</p>
						[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3" css=".vc_custom_1494854666876{padding-left: 20px !important;}"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="600x500"][vc_column_text css=".vc_custom_1494854731936{margin-bottom: 15px !important;}"]
						<h4 style="text-align: center;">Free Updates</h4>
						[/vc_column_text][vc_column_text]
						<p class="p1" style="text-align: center;">Proin interdum, ante ut sollicitudin commodo, tellus quam sagittis libero, at semper mauris velit a velit. Phasellus commodo turpis et lacinia posuere.</p>
						[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'img_icons_1' => array(
						'title' => __( 'Image with Icons 2', 'engage' ),
						'desc' => __( 'Icon boxes with image, fullwidth.', 'engage' ), 
						'cat' => array( 'Content', 'Icon Boxes', 'With Image', 'Features/Services' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" equal_height="yes" content_placement="middle" css=".vc_custom_1493916312513{padding-top: 0px !important;padding-bottom: 0px !important;background-color: #f8f8f8 !important;}"][vc_column width="1/2" col_padding="6" css=".vc_custom_1493916541916{padding-top: 65px !important;padding-bottom: 20px !important;}"][vc_row_inner][vc_column_inner width="1/2"][icon_box title="Responsive Design" text="Engage is a fully responsive theme that scales well to all mobile devices." style="aligned-left-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-display"][/vc_column_inner][vc_column_inner width="1/2"][icon_box title="Page Builder" text="Build any layout you can think of with the most powerful page builder." style="aligned-left-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-pen"][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner width="1/2"][icon_box title="Powerful Options" text="Take a full control over all aspects of your theme with our truly extensive." style="aligned-left-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-params"][/vc_column_inner][vc_column_inner width="1/2"][icon_box title="Free Support" text="Our top-notch support team will be happy to answer any of your questions." style="aligned-left-basic" icon_type="linecons" c_margin_bottom="45px" icon_linecons="vc_li vc_li-bubble"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/2" col_padding_side="left" css=".vc_custom_1493916270663{background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-10-1600x1200.jpg) !important;}"][/vc_column][/vc_row]',
					),
					'icon_boxes_2x4_c_a' => array(
						'title' => __( 'Services 1', 'engage' ),
						'desc' => __( 'Headig with two rows of icon boxes.', 'engage' ), 
						'cat' => array( 'Content', 'Icon Boxes', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1493821714807{padding-top: 75px !important;padding-bottom: 20px !important;}"][vc_column][special_heading title="Our Services" subtitle="Learn more about our great services." align="center" c_margin_bottom="60px" add_icon="true" icon_type="linecons" icon_linecons="vc_li vc_li-star"][vc_row_inner css=".vc_custom_1493821700763{padding-top: 10px !important;padding-bottom: 30px !important;}"][vc_column_inner width="1/4"][icon_box title="Powerful Options" text="Take a full control over all aspects of your theme with our truly extensive Options Panel." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Responsive Design" text="Engage is a fully responsive and retina ready theme that scales well to all mobile devices." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Page Builder" text="Build any layout you can think of with the most powerful page builder Visual Composer." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-pen"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Styling Options" text="Adjust the looks of your theme with the extensive Styling panel. Possibilities are endless." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-diamond"][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493821710484{padding-top: 10px !important;padding-bottom: 30px !important;}"][vc_column_inner width="1/4"][icon_box title="Demo Content" text="All of presented layouts and theme demo pages are available with a single mouse click." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-megaphone"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Free Support" text="Our top-notch support team will be more than happy to answer any of your questions." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-bubble"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Documentation" text="Everything you need to know about the theme is located in the extensive documentation." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-news"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Premium Sliders" text="Engage comes with two most powerful sliders out there: Revolution Slider and Layer Slider." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-camera"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'icon_boxes_3x3_a' => array(
						'title' => __( 'Services 2', 'engage' ),
						'desc' => __( 'Three rows of aligned icon boxes.', 'engage' ), 
						'cat' => array( 'Content', 'Icon Boxes', 'Features/Services' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1494858169779{padding-top: 70px !important;padding-bottom: 10px !important;}"][vc_column][special_heading title="Our Features" subtitle="You are going to love those." c_margin_bottom="60px"][vc_row_inner css=".vc_custom_1494858130762{margin-bottom: 40px !important;}"][vc_column_inner width="1/3"][icon_box title="Powerful Options" text="Take a full control over all aspects of your theme with our truly extensive Options Panel." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Responsive Design" text="Engage is a fully responsive and retina ready theme that scales well to all mobile devices." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Satisfied Clients" text="Build any layout you can think of with the most powerful page builder Visual Composer." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-pen"][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1494858134698{margin-bottom: 40px !important;}"][vc_column_inner width="1/3"][icon_box title="Demo Content" text="All of presented layouts and theme demo pages are available with a single mouse click." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-megaphone"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Free Support" text="Our top-notch support team will be more than happy to answer any of your questions." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-bubble"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Documentation" text="Everything you need to know about the theme is located in the extensive documentation." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-news"][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1494858145826{margin-bottom: 40px !important;}"][vc_column_inner width="1/3"][icon_box title="Google Fonts" text="You may choose any font from the extensive Google Fonts library with a single mouse click." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-star"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Premium Sliders" text="Engage comes with two most powerful sliders out there: Revolution Slider and Layer Slider." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-photo"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Styling Options" text="Adjust the looks of your theme with the extensive Styling panel. Possibilities are endless." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-diamond"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'icon_boxes_2x3_bg_img' => array(
						'title' => __( 'Services 3', 'engage' ),
						'desc' => __( 'Icon boxes over image background.', 'engage' ), 
						'cat' => array( 'Content', 'Icon Boxes', 'With Image', 'Features/Services' ),
						'sc' => '[vc_row parallax="content-moving" parallax_speed_bg="1.4" color_scheme="white" bg_overlay="dark40" css=".vc_custom_1494851803586{padding-top: 70px !important;padding-bottom: 60px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-23.jpg) !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][special_heading title="System Features" c_margin_bottom="65px"][vc_row_inner css=".vc_custom_1494851314158{margin-bottom: 30px !important;}"][vc_column_inner width="1/3"][icon_box title="Powerful Options" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Responsive Design" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Satisfied Clients" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-heart"][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner width="1/3"][icon_box title="Premium Support" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-news"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Extensive Documentation" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-study"][/vc_column_inner][vc_column_inner width="1/3"][icon_box title="Free Updates" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-paperplane"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'icon_boxes_1x4_bg' => array(
						'title' => __( 'Services 4', 'engage' ),
						'desc' => __( 'Icon boxes with colored bg.', 'engage' ), 
						'cat' => array( 'Icon Boxes', 'Features/Services' ),
						'sc' => '[vc_row color_scheme="white" css=".vc_custom_1494837848610{padding-top: 70px !important;padding-bottom: 50px !important;background-color: #2196f3 !important;}"][vc_column][special_heading title="Our Features" c_margin_bottom="55px" subtitle_fs="20px"][vc_row_inner][vc_column_inner width="1/4"][icon_box title="Powerful Options" text="" style="centered-basic" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Responsive Design" text="" style="centered-basic" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Page Builder" text="" style="centered-basic" icon_type="linecons" icon_linecons="vc_li vc_li-pen"][/vc_column_inner][vc_column_inner width="1/4"][icon_box title="Satisfied Clients" text="" style="centered-basic" icon_type="linecons" icon_linecons="vc_li vc_li-heart"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'services_plain_2x3' => array(
						'title' => __( 'Services Text 1', 'engage' ),
						'desc' => __( 'A list of services, plain text', 'engage' ), 
						'cat' => array( 'Features/Services', 'Plain Text' ),
						'sc' => '[vc_row css=".vc_custom_1495542037394{padding-top: 60px !important;padding-bottom: 30px !important;}"][vc_column][special_heading title="Our Services" subtitle="What we are great at." c_margin_bottom="60px" add_icon="true" icon_fontawesome="fa fa-flag-o"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][vc_column_text]<h5>Facilities</h5>Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]<h5>Best Equipment</h5>Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]<h5>Incredible Location</h5>Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][vc_column_text]<h5>Premium Support</h5>Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]<h5>Incredible Team</h5>Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]<h5>Incredible Location</h5>Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'services_plain_2x2' => array(
						'title' => __( 'Services Text 2', 'engage' ),
						'desc' => __( 'A list of services, plain text', 'engage' ), 
						'cat' => array( 'Features/Services', 'Plain Text' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1495542047594{padding-top: 70px !important;padding-bottom: 35px !important;}"][vc_column][special_heading title="Our Services" subtitle="What we are great at." c_margin_bottom="60px"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h4>Facilities</h4><p class="p1">Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero interdum at. Vivamus finibus laoreet tristique. Quisque pharetra, urna nec consequat pretium, est nisl maximus sem.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h4>Best Equipment</h4>Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero interdum at. Vivamus finibus laoreet tristique. Quisque pharetra, urna nec consequat pretium, est nisl maximus sem.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h4>Fantastic Location</h4><p class="p1">Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero interdum at. Vivamus finibus laoreet tristique. Quisque pharetra, urna nec consequat pretium, est nisl maximus sem.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h4>Best Equipment</h4>Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero interdum at. Vivamus finibus laoreet tristique. Quisque pharetra, urna nec consequat pretium, est nisl maximus sem.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'services_plain_2n3' => array(
						'title' => __( 'Services Text 3', 'engage' ),
						'desc' => __( 'A step list of services, plain text', 'engage' ), 
						'cat' => array( 'Features/Services', 'Plain Text' ),
						'sc' => '[vc_row css=".vc_custom_1495542072137{padding-top: 70px !important;padding-bottom: 35px !important;}"][vc_column][special_heading title="Our Services" subtitle="Duis eget quam tincidunt, blandit massa quis, dapibus diam. Integer ut interdum ex. Nullam vitae elit turpis." c_margin_bottom="60px"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text font_size="medium"]<h4>Facilities</h4><p class="p1">Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text font_size="medium"]<h4>Best Equipment</h4>Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][vc_column_text]<h5>Location</h5><p class="p1">Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]<h5>Premium Support</h5>Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]<h5>Awesome Team</h5>Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'services_plain_2x2_img' => array(
						'title' => __( 'Services with Image', 'engage' ),
						'desc' => __( 'List of services with an image.', 'engage' ), 
						'cat' => array( 'Features/Services', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" parallax="content-moving" parallax_speed_bg="1.3" css=".vc_custom_1494860920894{padding-top: 90px !important;padding-bottom: 90px !important;background-position: center;background-repeat: no-repeat;background-size:cover !important;}"][vc_column width="7/12" col_padding="2" col_padding_side="right" css=".vc_custom_1495542874381{padding-top: 35px !important;}"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h5>Location</h5><p class="p1">Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h5>Advanced Features</h5>Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1495542907558{padding-bottom: 0px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h5>Premium Support</h5><p class="p1">Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h5>Responsive Design</h5>Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="5/12" col_padding="2" col_padding_side="left"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/square-5.jpg" img_size="700x600" css=".vc_custom_1495543110874{margin-bottom: 0px !important;}"][/vc_column][/vc_row]',
					),
					'services_plain_3x2_img' => array(
						'title' => __( 'Services with Image 2', 'engage' ),
						'desc' => __( 'List of services with vertical image.', 'engage' ), 
						'cat' => array( 'Features/Services', 'Text + Image', 'With Image' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" parallax="content-moving" parallax_speed_bg="1.3" css=".vc_custom_1495543442812{padding-top: 90px !important;padding-bottom: 80px !important;}"][vc_column width="1/3" col_padding="2" col_padding_side="right" css=".vc_custom_1495543020396{padding-top: 0px !important;}"][vc_single_image image="http://media.veented.com/wp-content/uploads/2017/09/full-5.jpg" img_size="700x1100" css=".vc_custom_1495543326257{margin-bottom: 0px !important;}"][/vc_column][vc_column width="2/3" col_padding="2" col_padding_side="left" css=".vc_custom_1495543400531{padding-top: 55px !important;}"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h5>Premium Support</h5><p class="p1">Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h5>Great Team</h5>Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h5>Premium Support</h5><p class="p1">Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h5>Great Team</h5>
						Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h5>Premium Support</h5><p class="p1">Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan.</p>
						[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h5>Great Team</h5>Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'services_plain_2x2_side' => array(
						'title' => __( 'Services with Side Heading', 'engage' ),
						'desc' => __( 'List of services with side heading.', 'engage' ), 
						'cat' => array( 'Features/Services', 'Plain Text' ),
						'sc' => '[vc_row equal_height="yes" content_placement="top" parallax="content-moving" parallax_speed_bg="1.3" css=".vc_custom_1495543097611{padding-top: 95px !important;padding-bottom: 30px !important;background-position: 0 0;background-repeat: no-repeat !important;}"][vc_column width="1/4" col_padding="2" col_padding_side="right" css=".vc_custom_1495543020396{padding-top: 0px !important;}"][vc_column_text]<h3>Our Services</h3>[/vc_column_text][/vc_column][vc_column width="3/4" col_padding="2" col_padding_side="left"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h5>Premium Support</h5><p class="p1">Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h5>Great Team</h5>Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/2"][vc_column_text]<h5>Location</h5><p class="p1">Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt.</p>[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]<h5>Responsive Design</h5>Cras auctor consectetur pharetra. Phasellus sollicitudin diam purus, at sagittis diam elementum venenatis. Phasellus accumsan erat quis risus rhoncus, laoreet lobortis diam tincidunt.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'icon_boxes_1x4_c' => array(
						'title' => __( 'Icon Boxes Outline', 'engage' ),
						'desc' => __( 'A row of centered, outline icon boxes.', 'engage' ), 
						'cat' => array( 'Icon Boxes', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1493725837900{padding-top: 85px !important;padding-bottom: 45px !important;}"][vc_column width="1/4"][icon_box title="Powerful Options" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column][vc_column width="1/4"][icon_box title="Responsive Design" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column][vc_column width="1/4"][icon_box title="Page Builder" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." icon_type="linecons" icon_linecons="vc_li vc_li-pen"][/vc_column][vc_column width="1/4"][icon_box title="Satisfied Clients" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." icon_type="linecons" icon_linecons="vc_li vc_li-heart"][/vc_column][/vc_row]',
					),
					'icon_boxes_1x4_c_accent' => array(
						'title' => __( '1x4 Icon Boxes Accent', 'engage' ),
						'desc' => __( 'A row of centered icon boxes.', 'engage' ), 
						'cat' => array( 'Icon Boxes', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1493725830541{padding-top: 85px !important;padding-bottom: 45px !important;}"][vc_column width="1/4"][icon_box title="Powerful Options" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column][vc_column width="1/4"][icon_box title="Responsive Design" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column][vc_column width="1/4"][icon_box title="Page Builder" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-pen"][/vc_column][vc_column width="1/4"][icon_box title="Satisfied Clients" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-circle" icon_type="linecons" icon_linecons="vc_li vc_li-heart"][/vc_column][/vc_row]',
					),
					'icon_boxes_1x4_basic' => array(
						'title' => __( '1x4 Icon Boxes Basic', 'engage' ),
						'desc' => __( 'A row of basic centered icon boxes.', 'engage' ), 
						'cat' => array( 'Icon Boxes', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1493726240893{padding-top: 80px !important;padding-bottom: 45px !important;}"][vc_column width="1/4"][icon_box title="Powerful Options" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-basic" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column][vc_column width="1/4"][icon_box title="Responsive Design" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-basic" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column][vc_column width="1/4"][icon_box title="Page Builder" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-basic" icon_type="linecons" icon_linecons="vc_li vc_li-pen"][/vc_column][vc_column width="1/4"][icon_box title="Satisfied Clients" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-basic" icon_type="linecons" icon_linecons="vc_li vc_li-heart"][/vc_column][/vc_row]',
					),
					'icon_boxes_1x4_boxed' => array(
						'title' => __( '1x4 Icon Boxes Boxed', 'engage' ),
						'desc' => __( 'A row of basic centered icon boxes.', 'engage' ), 
						'cat' => array( 'Icon Boxes', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1493725837900{padding-top: 85px !important;padding-bottom: 45px !important;}"][vc_column width="1/4"][icon_box title="Powerful Options" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-boxed" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column][vc_column width="1/4"][icon_box title="Responsive Design" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-boxed" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column][vc_column width="1/4"][icon_box title="Page Builder" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-boxed" icon_type="linecons" icon_linecons="vc_li vc_li-pen"][/vc_column][vc_column width="1/4"][icon_box title="Satisfied Clients" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="centered-boxed" icon_type="linecons" icon_linecons="vc_li vc_li-heart"][/vc_column][/vc_row]',
					),
					'icon_boxes_1x4_aligned' => array(
						'title' => __( '1x3 Icon Boxes Aligned', 'engage' ),
						'desc' => __( 'A row of 3 aligned icon boxes.', 'engage' ), 
						'cat' => array( 'Icon Boxes', 'Features/Services' ),
						'sc' => '[vc_row css=".vc_custom_1493725837900{padding-top: 85px !important;padding-bottom: 45px !important;}"][vc_column width="1/3"][icon_box title="Powerful Options" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-params"][/vc_column][vc_column width="1/3"][icon_box title="Responsive Design" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-display"][/vc_column][vc_column width="1/3"][icon_box title="Satisfied Clients" text="Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo." style="aligned-left-basic" icon_type="linecons" icon_linecons="vc_li vc_li-heart"][/vc_column][/vc_row]',
					),
					'counters_gradient' => array(
						'title' => __( 'Counters Gradient', 'engage' ),
						'desc' => __( 'Counters on a gradient background.', 'engage' ), 
						'cat' => array( 'Counters' ),
						'sc' => '[vc_row parallax="content-moving" bg_overlay="dark20" bg_gradient1="#b06ab3" bg_gradient2="#4568dc" css=".vc_custom_1493833267010{padding-top: 95px !important;padding-bottom: 60px !important;background-color: #b06ab3 !important;}"][vc_column width="1/4"][counter title="Page Likes" number="1430" color="white" icon_type="linecons" icon_linecons="vc_li vc_li-like"][/vc_column][vc_column width="1/4"][counter title="Locations" number="64" color="white" icon_type="linecons" icon_linecons="vc_li vc_li-shop"][/vc_column][vc_column width="1/4"][counter title="Great Ideas" number="960" color="white" icon_type="linecons" icon_linecons="vc_li vc_li-bulb"][/vc_column][vc_column width="1/4"][counter title="Comments" number="420" color="white" icon_type="linecons" icon_linecons="vc_li vc_li-bubble"][/vc_column][/vc_row]',
					),
					'counters_accent' => array(
						'title' => __( 'Counters Accent', 'engage' ),
						'desc' => __( 'Counters on an accent background.', 'engage' ), 
						'cat' => array( 'Counters' ),
						'sc' => '[vc_row parallax="content-moving" bg_color_pre="bg-color-accent" css=".vc_custom_1493833319536{padding-top: 95px !important;padding-bottom: 60px !important;}"][vc_column width="1/4"][counter title="Page Likes" number="1430" color="white" icon_type="linecons" icon_linecons="vc_li vc_li-like"][/vc_column][vc_column width="1/4"][counter title="Locations" number="64" color="white" icon_type="linecons" icon_linecons="vc_li vc_li-shop"][/vc_column][vc_column width="1/4"][counter title="Great Ideas" number="960" color="white" icon_type="linecons" icon_linecons="vc_li vc_li-bulb"][/vc_column][vc_column width="1/4"][counter title="Comments" number="420" color="white" icon_type="linecons" icon_linecons="vc_li vc_li-bubble"][/vc_column][/vc_row]',
					),
					'counters' => array(
						'title' => __( 'Counters Light', 'engage' ),
						'desc' => __( 'Counters on a light background.', 'engage' ), 
						'cat' => array( 'Counters' ),
						'sc' => '[vc_row parallax="content-moving" css=".vc_custom_1493833438333{padding-top: 95px !important;padding-bottom: 60px !important;}"][vc_column width="1/4"][counter title="Page Likes" number="1430" icon_type="linecons" icon_linecons="vc_li vc_li-like"][/vc_column][vc_column width="1/4"][counter title="Locations" number="64" icon_type="linecons" icon_linecons="vc_li vc_li-shop"][/vc_column][vc_column width="1/4"][counter title="Great Ideas" number="960" icon_type="linecons" icon_linecons="vc_li vc_li-bulb"][/vc_column][vc_column width="1/4"][counter title="Comments" number="420" icon_type="linecons" icon_linecons="vc_li vc_li-bubble"][/vc_column][/vc_row]',
					),
					'pie_charts' => array(
						'title' => __( 'Pie Charts', 'engage' ),
						'desc' => __( 'A row of pie charts.', 'engage' ), 
						'cat' => array( 'Charts and Progress' ),
						'sc' => '[vc_row css=".vc_custom_1494858964069{padding-top: 90px !important;padding-bottom: 55px !important;}"][vc_column width="1/4"][vc_pie value="60" color="vista-blue" title="Features" units="%"][/vc_column][vc_column width="1/4"][vc_pie value="74" color="vista-blue" title="Design" units="%"][/vc_column][vc_column width="1/4"][vc_pie value="86" color="vista-blue" title="Development" units="%"][/vc_column][vc_column width="1/4"][vc_pie value="94" color="vista-blue" title="JavaScript" units="%"][/vc_column][/vc_row]',
					),
					'pie_charts_c' => array(
						'title' => __( 'Pie Charts Color', 'engage' ),
						'desc' => __( 'A row of pie charts on a blue bg.', 'engage' ), 
						'cat' => array( 'Charts and Progress' ),
						'sc' => '[vc_row color_scheme="white" css=".vc_custom_1494859004879{padding-top: 90px !important;padding-bottom: 50px !important;background-color: #3f51b5 !important;}"][vc_column width="1/4"][vc_pie value="60" color="white" title="Features" units="%"][/vc_column][vc_column width="1/4"][vc_pie value="74" color="white" title="Design" units="%"][/vc_column][vc_column width="1/4"][vc_pie value="86" color="white" title="Development" units="%"][/vc_column][vc_column width="1/4"][vc_pie value="94" color="white" title="JavaScript" units="%"][/vc_column][/vc_row]',
					),
					'progress_bars' => array(
						'title' => __( 'Progress Bars + Text', 'engage' ),
						'desc' => __( 'A row of pie charts on a blue bg.', 'engage' ), 
						'cat' => array( 'Charts and Progress' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494857494829{padding-top: 80px !important;padding-bottom: 55px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_column_text css=".vc_custom_1494857472580{margin-bottom: 18px !important;}"]
						<h3>Our Skills</h2>[/vc_column_text][vc_column_text]Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero interdum at. Mauris sodales felis luctus purus hendreri. Vivamus baram sapien era.[/vc_column_text][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_progress_bar values="%5B%7B%22label%22%3A%22Development%22%2C%22value%22%3A%2294%22%7D%2C%7B%22label%22%3A%22Design%22%2C%22value%22%3A%2286%22%7D%2C%7B%22label%22%3A%22Marketing%22%2C%22value%22%3A%2278%22%7D%2C%7B%22label%22%3A%22Webdev%22%2C%22value%22%3A%2272%22%7D%5D" bgcolor="accent" units="%"][/vc_column][/vc_row]',
					),
					'progress_bars_text_right' => array(
						'title' => __( 'Progress Bars + Text 2', 'engage' ),
						'desc' => __( 'A row of pie charts on a blue bg.', 'engage' ), 
						'cat' => array( 'Charts and Progress' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1494857613169{padding-top: 80px !important;padding-bottom: 55px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right"][vc_progress_bar values="%5B%7B%22label%22%3A%22Development%22%2C%22value%22%3A%2294%22%7D%2C%7B%22label%22%3A%22Design%22%2C%22value%22%3A%2286%22%7D%2C%7B%22label%22%3A%22Marketing%22%2C%22value%22%3A%2278%22%7D%2C%7B%22label%22%3A%22Webdev%22%2C%22value%22%3A%2272%22%7D%5D" bgcolor="accent" units="%"][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vc_column_text css=".vc_custom_1494857472580{margin-bottom: 18px !important;}"]
						<h3>Our Skills</h2>[/vc_column_text][vc_column_text]Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero interdum at. Mauris sodales felis luctus purus hendreri. Vivamus baram sapien era.[/vc_column_text][/vc_column][/vc_row]',
					),
					'sheading_1' => array(
						'title' => __( 'Heading 1', 'engage' ),
						'desc' => __( 'Centered heading with subtitle.', 'engage' ), 
						'cat' => array( 'Content', 'Headings' ),
						'sc' => '[vc_row][vc_column][special_heading title="Section Title" subtitle="And here goes a super fancy description of the section."][/vc_column][/vc_row]',
					),
					'sheading_highlight' => array(
						'title' => __( 'Heading Highlights', 'engage' ),
						'desc' => __( 'Centered heading with highlighted words.', 'engage' ), 
						'cat' => array( 'Content', 'Headings' ),
						'sc' => '[vc_row][vc_column][special_heading title="Section Title" subtitle="And here goes a super fancy description with a (b)highlighted(/b) word to make it even more cool and to (b)make(/b) some words standout more."][/vc_column][/vc_row]',
					),
					'sheading_border' => array(
						'title' => __( 'Heading with Border', 'engage' ),
						'desc' => __( 'Centered heading with a border below.', 'engage' ), 
						'cat' => array( 'Content', 'Headings' ),
						'sc' => '[vc_row][vc_column][special_heading title="Section Title" subtitle="And here goes a super fancy description of the section." border="below"][/vc_column][/vc_row]',
					),
					'sheading_icon' => array(
						'title' => __( 'Heading with Icon', 'engage' ),
						'desc' => __( 'Centered heading with an icon.', 'engage' ), 
						'cat' => array( 'Content', 'Headings' ),
						'sc' => '[vc_row][vc_column][special_heading title="Section Title" subtitle="And here goes a super fancy description of the section." add_icon="true" icon_fontawesome="fa fa-heart-o"][/vc_column][/vc_row]',
					),
					'sheading_icon_border' => array(
						'title' => __( 'Heading with Icon 2', 'engage' ),
						'desc' => __( 'Centered heading with an icon and border.', 'engage' ), 
						'cat' => array( 'Content', 'Headings' ),
						'sc' => '[vc_row][vc_column][special_heading title="Section Title" subtitle="And here goes a super fancy description of the section." border="below" add_icon="true" icon_fontawesome="fa fa-heart-o"][/vc_column][/vc_row]',
					),
					'sheading_left' => array(
						'title' => __( 'Heading Aligned', 'engage' ),
						'desc' => __( 'Heading with subtitle aligned left.', 'engage' ), 
						'cat' => array( 'Content', 'Headings' ),
						'sc' => '[vc_row][vc_column][special_heading title="Section Title" subtitle="And here goes a super fancy description of the section." align="left"][/vc_column][/vc_row]',
					),
					'video_embed' => array(
						'title' => __( 'Video Embedded', 'engage' ),
						'desc' => __( 'Embedded video for Vimeo, Youtube and more.', 'engage' ), 
						'cat' => array( 'Content', 'Video' ),
						'sc' => '[vc_row css=".vc_custom_1494860257444{padding-top: 80px !important;padding-bottom: 70px !important;}"][vc_column][special_heading title="Embedded Video" subtitle="This section has an embedded Vimeo video." c_margin_bottom="60px"][vc_video css=".vc_custom_1494860195254{padding-right: 10% !important;padding-left: 10% !important;}"][/vc_column][/vc_row]
						',
					),
					'video_lightbox_img_light' => array(
						'title' => __( 'Video Lightbox Img', 'engage' ),
						'desc' => __( 'Video lightbox with a placeholder image.', 'engage' ), 
						'cat' => array( 'Content', 'Video' ),
						'sc' => '[vc_row parallax="content-moving" css=".vc_custom_1493989844972{padding-top: 100px !important;padding-bottom: 120px !important;background-color: #f7f7f7 !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][special_heading title="Our Video" subtitle="Learn more about us from this video." c_margin_bottom="50px"][video_lightbox style="img" img="http://media.veented.com/wp-content/uploads/2017/09/full-14-1400x900.jpg" border="round" link="https://www.youtube.com/watch?v=ty0SGNZi81U"][/vc_column][/vc_row]',
					),
					'video_lightbox_img_gd' => array(
						'title' => __( 'Video Lightbox Img 2', 'engage' ),
						'desc' => __( 'Video lightbox with a placeholder image.', 'engage' ), 
						'cat' => array( 'Content', 'Video' ),
						'sc' => '[vc_row parallax="content-moving" color_scheme="white" bg_gradient1="#b06ab3" bg_gradient2="#4568dc" css=".vc_custom_1493989477376{padding-top: 100px !important;padding-bottom: 120px !important;background-color: #b06ab3 !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][special_heading title="Our Video" subtitle="Learn more about us from this video." c_margin_bottom="50px"][video_lightbox style="img" img="http://media.veented.com/wp-content/uploads/2017/09/full-14-1400x900.jpg" border="round" link="https://www.youtube.com/watch?v=ty0SGNZi81U"][/vc_column][/vc_row]',
					),
					'video_lightbox_img' => array(
						'title' => __( 'Video Lightbox', 'engage' ),
						'desc' => __( 'Video lightbox with a background image.', 'engage' ), 
						'cat' => array( 'Content', 'Video' ),
						'sc' => '[vc_row parallax="content-moving" bg_overlay="dark40" css=".vc_custom_1493982898825{padding-top: 130px !important;padding-bottom: 110px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-14.jpg) !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][video_lightbox title="Our Video" description="Learn a bit more about our awesome company from this short clip we recorded together this summer!" link="https://www.youtube.com/watch?v=ty0SGNZi81U"][/vc_column][/vc_row]',
					),
					'video_lightbox_accent' => array(
						'title' => __( 'Video Lightbox Accent', 'engage' ),
						'desc' => __( 'Video lightbox with an accent overlay.', 'engage' ), 
						'cat' => array( 'Content', 'Video' ),
						'sc' => '[vc_row parallax="content-moving" bg_overlay="accent" css=".vc_custom_1493982884769{padding-top: 130px !important;padding-bottom: 110px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-14.jpg) !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][video_lightbox title="Our Video" description="Learn a bit more about our awesome company from this short clip we recorded together this summer!" link="https://www.youtube.com/watch?v=ty0SGNZi81U"][/vc_column][/vc_row]',
					),
					'video_lightbox_gd' => array(
						'title' => __( 'Video Lightbox Gradient', 'engage' ),
						'desc' => __( 'Video lightbox with a gradient background.', 'engage' ), 
						'cat' => array( 'Content', 'Video' ),
						'sc' => '[vc_row parallax="content-moving" bg_gradient1="#b06ab3" bg_gradient2="#4568dc" css=".vc_custom_1493983022628{padding-top: 130px !important;padding-bottom: 110px !important;background-color: #b06ab3 !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][video_lightbox title="Our Video" description="Learn a bit more about our awesome company from this short clip we recorded together this summer!" link="https://www.youtube.com/watch?v=ty0SGNZi81U"][/vc_column][/vc_row]',
					),
					'text_two_cols_h' => array(
						'title' => __( 'Two columns of text.', 'engage' ),
						'desc' => __( 'Plain text in two columns with a heading.', 'engage' ), 
						'cat' => array( 'Content', 'Plain Text' ),
						'sc' => '[vc_row css=".vc_custom_1494837258934{padding-top: 75px !important;padding-bottom: 50px !important;}"][vc_column][vc_custom_heading text="We are truly unique. Wanna know why?" font_container="tag:h2|font_size:34px|text_align:center" google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:400%20regular%3A400%3Anormal" css=".vc_custom_1494837196348{margin-bottom: 45px !important;}"][vc_row_inner][vc_column_inner width="1/2" css=".vc_custom_1494837242649{padding-right: 25px !important;}"][vc_column_text]
						<p class="p1">Morbi pellentesque, nisl id semper bibendum, nibh sem fermentum magna, eget commodo leo velit sit amet velit. Aliquam fermentum, lorem quis posuere mattis, est justo porttitor magna, in commodo risus justo vitae nibh. Sed mollis sapien erat, id pellentesque libero interdum at.Mauris sodales felis luctus purus hendreri. Vivamus finibus laoreet tristique. Quisque pharetra, urna.</p>
						[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2" css=".vc_custom_1494837238398{padding-left: 25px !important;}"][vc_column_text]
						<p class="p1">Nunc id ante quis tellus faucibus dictum in eget metus. Duis suscipit elit sem, sed mattis tellus accumsan eget. Quisque consequat venenatis rutrum. Quisque posuere enim augue, in rhoncus diam dictum non. Etiam mollis pulvinar nisl, sed pharetra nunc elementum non. Mauris sodales felis luctus purus hendrerit, vel cursus odio pulvinar. In ullamcorper ultrices purus.</p>
						[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'text_video_bg_row' => array(
						'title' => __( 'Section with video background.', 'engage' ),
						'desc' => __( 'Plain text over a video background.', 'engage' ), 
						'cat' => array( 'Content', 'Video', 'Plain Text' ),
						'sc' => '[vc_row video_bg="yes" video_bg_url="https://www.youtube.com/watch?v=2QKQX7fbjnA" color_scheme="white" bg_overlay="dark40" css=".vc_custom_1494859962917{padding-top: 100px !important;padding-bottom: 80px !important;}"][vc_column][vc_row_inner css=".vc_custom_1494859640521{padding-right: 14% !important;padding-left: 14% !important;}"][vc_column_inner][special_heading title="Video Background" subtitle="This section has a video background." border="below" c_margin_bottom="40px"][vc_column_text font_size="large"]
						<p class="p1" style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id risus a orci rutrum scelerisque id ut nibh. Proin interdum, ante ut sollicitudin commodo, tellus quam sagittis libero, at semper mauris velit a velit. Phasellus commodo turpis et lacinia posuere.</p>
						[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'text_bg_color' => array(
						'title' => __( 'Centered text.', 'engage' ),
						'desc' => __( 'Centered plain text with a blue background.', 'engage' ), 
						'cat' => array( 'Content', 'Plain Text' ),
						'sc' => '[vc_row video_bg="yes" video_bg_url="" color_scheme="white" bg_overlay="dark40" css=".vc_custom_1494860444039{padding-top: 100px !important;padding-bottom: 80px !important;background-color: #3949ab !important;}"][vc_column][vc_row_inner css=".vc_custom_1494859640521{padding-right: 14% !important;padding-left: 14% !important;}"][vc_column_inner][special_heading title="Text Section" subtitle="Section with some big plain text." border="below" c_margin_bottom="40px"][vc_column_text font_size="large"]
						<p class="p1" style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id risus a orci rutrum scelerisque id ut nibh. Proin interdum, ante ut sollicitudin commodo, tellus quam sagittis libero, at semper mauris velit a velit. Phasellus commodo turpis et lacinia posuere.</p>
						[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'text_bg_accent' => array(
						'title' => __( 'Centered text, accent.', 'engage' ),
						'desc' => __( 'Centered plain text with an accent background.', 'engage' ), 
						'cat' => array( 'Content', 'Plain Text' ),
						'sc' => '[vc_row video_bg="yes" video_bg_url="" color_scheme="white" bg_color_pre="bg-color-accent" css=".vc_custom_1494862211517{padding-top: 90px !important;padding-bottom: 60px !important;}"][vc_column width="1/4"][/vc_column][vc_column width="2/4"][vc_column_text css=".vc_custom_1494860711961{margin-bottom: 25px !important;}"]
						<h2 style="text-align: center;">The best part?</h2>
						[/vc_column_text][vc_column_text font_size="large"]
						<p style="text-align: center;">We have been operating for over 30 years and are Members of The Federation of Master Builders. We work on projects big and small from small residential extensions to full house. We are so happy with this theme. Everyday it make our lives better.</p>
						[/vc_column_text][/vc_column][vc_column width="1/4"][/vc_column][/vc_row]',
					),
					'logos' => array(
						'title' => __( 'Logos Carousel', 'engage' ),
						'desc' => __( 'Simple client logos carousel.', 'engage' ), 
						'cat' => array( 'Content', 'Carousel', 'Clients' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1493990837872{padding-top: 30px !important;padding-bottom: 5px !important;}"][vc_column][client_logos images="603,601,602,599,600,598" cols="5" dots="false"][/vc_column][/vc_row]',
					),
					'cta_light' => array(
						'title' => __( 'Call to Action Light', 'engage' ),
						'desc' => __( 'Call to action section, light.', 'engage' ), 
						'cat' => array( 'Content', 'Call to Action' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" bg_color_pre="bg-color-1" css=".vc_custom_1494250676237{padding-top: 25px !important;padding-bottom: 20px !important;}"][vc_column][cta heading="Feeling convinced?" subheading="I am a subtitle, feel free to change me!" container="contain" align="center" text_color="dark" button1_color="accent" button1_style="solid" el_bg="none"][/vc_column][/vc_row]',
					),
					'cta_accent' => array(
						'title' => __( 'Call to Action Accent', 'engage' ),
						'desc' => __( 'Call to action section, accent bg.', 'engage' ), 
						'cat' => array( 'Content', 'Call to Action' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" bg_color_pre="bg-color-accent" css=".vc_custom_1494250403646{padding-top: 0px !important;}"][vc_column][cta heading="Feeling convinced?" subheading="I am a subtitle, feel free to change me!" container="contain" align="center" button1_color="white" el_bg="none"][/vc_column][/vc_row]',
					),
					'cta_gd' => array(
						'title' => __( 'Call to Action Gradient', 'engage' ),
						'desc' => __( 'Call to action section, gradient bg.', 'engage' ), 
						'cat' => array( 'Content', 'Call to Action' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" bg_gradient1="#b06ab3" bg_gradient2="#4568dc" css=".vc_custom_1494250711563{padding-top: 45px !important;padding-bottom: 40px !important;}"][vc_column][cta heading="Feeling convinced?" subheading="I am a subtitle, feel free to change me!" container="contain" align="center" button1_color="white" el_bg="none"][/vc_column][/vc_row]',
					),
					'cta_img' => array(
						'title' => __( 'Call to Action Image', 'engage' ),
						'desc' => __( 'Call to action section, image bg.', 'engage' ), 
						'cat' => array( 'Content', 'Call to Action' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" bg_color_pre="bg-color-accent" bg_overlay="dark30" css=".vc_custom_1494250497331{padding-top: 45px !important;padding-bottom: 40px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-14.jpg) !important;}"][vc_column][cta heading="Feeling convinced?" subheading="I am a subtitle, feel free to change me!" container="contain" align="center" button1_color="white" el_bg="none"][/vc_column][/vc_row]',
					),
					'cta_align_light' => array(
						'title' => __( 'Call to Action Aligned', 'engage' ),
						'desc' => __( 'Call to action, aligned, light.', 'engage' ), 
						'cat' => array( 'Content', 'Call to Action' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" bg_color_pre="bg-color-1" css=".vc_custom_1494250779120{padding-top: 0px !important;}"][vc_column][cta heading="Feeling convinced?" subheading="I am a subtitle, feel free to change me!" container="contain" text_color="dark" button1_color="accent" button1_style="solid" el_bg="none"][/vc_column][/vc_row]',
					),
					'cta_align_accent' => array(
						'title' => __( 'Call to Action Aligned 2', 'engage' ),
						'desc' => __( 'Call to action, aligned, accent bg.', 'engage' ), 
						'cat' => array( 'Content', 'Call to Action' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" css=".vc_custom_1494250266432{padding-top: 0px !important;}"][vc_column][cta heading="Feeling convinced?" subheading="I am a subtitle, feel free to change me!" container="contain" button1_color="white"][/vc_column][/vc_row]',
					),
					'cta_align_gd' => array(
						'title' => __( 'Call to Action Aligned 3', 'engage' ),
						'desc' => __( 'Call to action, aligned, gradient bg.', 'engage' ), 
						'cat' => array( 'Content', 'Call to Action' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" css=".vc_custom_1494250266432{padding-top: 0px !important;}"][vc_column][cta heading="Feeling convinced?" subheading="I am a subtitle, feel free to change me!" container="contain" button1_color="white" el_bg="custom" bg_color_custom="#b06ab3" bg_color_custom2="#4568dc"][/vc_column][/vc_row]',
					),
					'tabs_left' => array(
						'title' => __( 'Tabs Aligned', 'engage' ),
						'desc' => __( 'Tabs aligned left.', 'engage' ), 
						'cat' => array( 'Content', 'Switchable' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493993344324{padding-top: 60px !important;padding-bottom: 20px !important;background-color: #ffffff !important;}"][vc_column col_padding_side="right" css=".vc_custom_1493057150253{padding-bottom: 15px !important;}"][vc_tta_tabs style="engage_outline" active_section="1" css=".vc_custom_1493993473265{padding-top: 30px !important;}"][vc_tta_section title="Our Goal" tab_id="1493993190218-196a605e-727b"][vc_row_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo.agittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][vc_tta_section title="Our Methods" tab_id="1493993190806-9d610dc4-3b3c"][vc_row_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]Phasellus mattis efficitur sollicitudin. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Proin nec dui congue neque cursus ullamcorper. Sed ipsum risus, ultrices a posuere ac, molestie sed tortor. Aenean gravida enim velit, ut auctor eros porttitor in. Phasellus consectetur a est dictum aliquet. Nulla et neque efficitur, auctor purus vel, ornare tellus. Aenean non tellus elementum purus feugiat ullamcorper a ut urna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam fermentum tristique ante.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][vc_tta_section title="Our Results" tab_id="1493993191420-fb42ce34-d873"][vc_row_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][/vc_tta_tabs][/vc_column][/vc_row]',
					),
					'tabs_minimal' => array(
						'title' => __( 'Tabs Centered 1', 'engage' ),
						'desc' => __( 'Minimalistic centered tabs.', 'engage' ), 
						'cat' => array( 'Content', 'Switchable' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493993344324{padding-top: 60px !important;padding-bottom: 20px !important;background-color: #ffffff !important;}"][vc_column col_padding_side="right" css=".vc_custom_1493057150253{padding-bottom: 15px !important;}"][vc_tta_tabs style="engage_minimal" alignment="center" active_section="1" css=".vc_custom_1493993515857{padding-top: 30px !important;}"][vc_tta_section title="Our Goal" tab_id="1493993485970-ad7afcba-6bee"][vc_row_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][vc_tta_section title="Our Methods" tab_id="1493993486815-2e178a9e-da9a"][vc_row_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]Phasellus mattis efficitur sollicitudin. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Proin nec dui congue neque cursus ullamcorper. Sed ipsum risus, ultrices a posuere ac, molestie sed tortor. Aenean gravida enim velit, ut auctor eros porttitor in. Phasellus consectetur a est dictum aliquet. Nulla et neque efficitur, auctor purus vel, ornare tellus. Aenean non tellus elementum purus feugiat ullamcorper a ut urna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam fermentum tristique ante.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][vc_tta_section title="Our Results" tab_id="1493993487697-16414874-0af5"][vc_row_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][/vc_tta_tabs][/vc_column][/vc_row]',
					),
					'tabs_boxed' => array(
						'title' => __( 'Tabs Centered 2', 'engage' ),
						'desc' => __( 'Boxed centered tabs.', 'engage' ), 
						'cat' => array( 'Content', 'Switchable' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493993344324{padding-top: 60px !important;padding-bottom: 20px !important;background-color: #ffffff !important;}"][vc_column col_padding_side="right" css=".vc_custom_1493057150253{padding-bottom: 15px !important;}"][vc_tta_tabs style="engage_boxed" alignment="center" active_section="1" css=".vc_custom_1493993219194{padding-top: 30px !important;}"][vc_tta_section title="Our Goal" tab_id="1493993374428-f20ac2e4-09e0"][vc_row_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][vc_tta_section title="Our Methods" tab_id="1493993375240-ab8ae6ec-c461"][vc_row_inner][vc_column_inner width="1/2"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis. Maecenas tincidunt nibh a velit dapibus gravida. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Nulla sollicitudin lorem quis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text]Phasellus mattis efficitur sollicitudin. Aliquam nisi enim, viverra eu nulla sed, fermentum volutpat dui. Proin nec dui congue neque cursus ullamcorper. Sed ipsum risus, ultrices a posuere ac, molestie sed tortor. Aenean gravida enim velit, ut auctor eros porttitor in. Phasellus consectetur a est dictum aliquet. Nulla et neque efficitur, auctor purus vel, ornare tellus. Aenean non tellus elementum purus feugiat ullamcorper a ut urna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam fermentum tristique ante.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][vc_tta_section title="Our Results" tab_id="1493993376107-58a3deec-2171"][vc_row_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultrices nisl diam, vitae consequat massa lacinia consequat. Nullam cursus risus a sem pretium lacinia. Etiam facilisis laoreet justo. Morbi mattis, turpis quis volutpat fermentum, lacus ante sagittis eros, vitae imperdiet leo felis ut ligula. Nulla sollicitudin lorem quis tempor mattis.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_tta_section][/vc_tta_tabs][/vc_column][/vc_row]',
					),
					'contact_text_map' => array(
						'title' => __( 'Contact Details + Map', 'engage' ),
						'desc' => __( 'Contact details with a map.', 'engage' ), 
						'cat' => array( 'Content', 'Contact' ),
						'sc' => '[vc_row equal_height="yes" content_placement="middle" css=".vc_custom_1493997533098{padding-top: 85px !important;padding-bottom: 85px !important;}"][vc_column width="1/2" col_padding="2" col_padding_side="right" css=".vc_custom_1493997470153{padding-top: 20px !important;}"][vc_column_text]
						<h3>Contact Information</h3>
						I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][vc_column_text css=".vc_custom_1493997380291{margin-bottom: 18px !important;}"]Our contact details:[/vc_column_text][engage_contact_form][/vc_column][/vc_row]',
					),
					'contact_text_map_f' => array(
						'title' => __( 'Contact Details + Map', 'engage' ),
						'desc' => __( 'Contact details with a map, fullwidth.', 'engage' ), 
						'cat' => array( 'Content', 'Contact' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces" equal_height="yes" content_placement="middle" css=".vc_custom_1493997595807{padding-top: 0px !important;padding-bottom: 0px !important;}"][vc_column width="1/2" col_padding="7" css=".vc_custom_1493997669608{padding-top: 30px !important;}"][vc_column_text]
						<h3>Contact Information</h3>
						I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][vc_column_text css=".vc_custom_1493997380291{margin-bottom: 18px !important;}"]Our contact details:[/vc_column_text][engage_contact_form][/vc_column][vc_column width="1/2" col_padding="2" col_padding_side="left"][vntd_gmap height="720" map_style="light" markers="%5B%7B%22title%22%3A%22Map%20Marker%22%2C%22text%22%3A%22This%20is%20an%20example%20marker%20description.%22%2C%22color%22%3A%22indigo%22%2C%22location%22%3A%22center%22%2C%22location_custom%22%3A%2240.7302327%2C-74.0100041%22%7D%5D"][/vc_column][/vc_row]',
					),
					'contact_form_text' => array(
						'title' => __( 'Contact Form + Details', 'engage' ),
						'desc' => __( 'Contact form with text details.', 'engage' ), 
						'cat' => array( 'Content', 'Contact' ),
						'sc' => '[vc_row css=".vc_custom_1493998472841{padding-top: 75px !important;padding-bottom: 40px !important;}"][vc_column width="2/3" col_padding="2" col_padding_side="right"][vc_column_text css=".vc_custom_1493998522496{margin-bottom: 35px !important;}"]
						<h3>Contact Form</h3>
						[/vc_column_text][engage_contact_form][/vc_column][vc_column width="1/3" col_padding="2" col_padding_side="left"][vc_column_text]
						<h3>Contact Information</h3>
						I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][vc_column_text css=".vc_custom_1493997380291{margin-bottom: 18px !important;}"]Our contact details:[/vc_column_text][vntd_icon_list icons_color="accent" elements="%5B%7B%22icon_fontawesome%22%3A%22fa%20fa-map-o%22%2C%22text%22%3A%22Manchester%20St%20123-78B%2C%20Random%20713%2C%20UK%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-phone%22%2C%22text%22%3A%22%2B46%20123%20456%20789%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-headphones%22%2C%22text%22%3A%22%2B37%20431%20456%20789%22%7D%2C%7B%22icon_fontawesome%22%3A%22fa%20fa-envelope-o%22%2C%22text%22%3A%22hello%40sitename.com%22%7D%5D" border="off"][/vc_column][/vc_row]',
					),
					'contact_form_c' => array(
						'title' => __( 'Contact Form Centered', 'engage' ),
						'desc' => __( 'Centered contact form.', 'engage' ), 
						'cat' => array( 'Content', 'Contact' ),
						'sc' => '[vc_row css=".vc_custom_1493998472841{padding-top: 75px !important;padding-bottom: 40px !important;}"][vc_column width="1/6"][/vc_column][vc_column width="2/3" col_padding="2" col_padding_side="left"][special_heading title="Contact us" subtitle="Drop us an e-mail using the form below." add_icon="true" icon_type="linecons" icon_linecons="vc_li vc_li-paperplane"][engage_contact_form btn_align="center"][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]',
					),
					'map_f_light' => array(
						'title' => __( 'Map Fullwidth Light', 'engage' ),
						'desc' => __( 'Fullwidth Google Map, light.', 'engage' ), 
						'cat' => array( 'Content', 'Contact' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces"][vc_column][vntd_gmap height="540" map_style="light" markers="%5B%7B%22title%22%3A%22Map%20Marker%22%2C%22text%22%3A%22This%20is%20an%20example%20marker%20description.%22%2C%22color%22%3A%22indigo%22%2C%22location%22%3A%22center%22%2C%22location_custom%22%3A%2241.863774%2C-87.721328%22%7D%5D" marker1_color="teal" marker2_title="Secondary Marker"][/vc_column][/vc_row]',
					),
					'map_f_dark' => array(
						'title' => __( 'Map Fullwidth Dark', 'engage' ),
						'desc' => __( 'Fullwidth Google Map, dark.', 'engage' ), 
						'cat' => array( 'Content', 'Contact' ),
						'sc' => '[vc_row full_width="stretch_row_content_no_spaces"][vc_column][vntd_gmap height="540" map_style="dark" markers="%5B%7B%22title%22%3A%22Map%20Marker%22%2C%22text%22%3A%22This%20is%20an%20example%20marker%20description.%22%2C%22color%22%3A%22orange%22%2C%22location%22%3A%22center%22%2C%22location_custom%22%3A%2241.863774%2C-87.721328%22%7D%5D" marker1_color="teal" marker2_title="Secondary Marker"][/vc_column][/vc_row]',
					),
					'prices_gray_3' => array(
						'title' => __( 'Prices Gray 3 Cols', 'engage' ),
						'desc' => __( 'Three columns of pricing boxes.', 'engage' ), 
						'cat' => array( 'Content', 'Prices' ),
						'sc' => '[vc_row css=".vc_custom_1494253443261{padding-top: 80px !important;padding-bottom: 62px !important;}"][vc_column][special_heading title="Our Prices" subtitle="And here goes a super fancy description of the section." c_margin_bottom="55px"][vc_row_inner css=".vc_custom_1494253419308{padding-top: 10px !important;}"][vc_column_inner width="1/3"][pricing_box title="Simple" price="$39" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" style="minimal"][/vc_column_inner][vc_column_inner width="1/3"][pricing_box title="Classic" price="$49" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" featured="yes"][/vc_column_inner][vc_column_inner width="1/3"][pricing_box title="Premium" price="$69" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" style="minimal"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'prices_gray_4' => array(
						'title' => __( 'Prices Gray 4 Cols', 'engage' ),
						'desc' => __( 'Four columns of pricing boxes.', 'engage' ), 
						'cat' => array( 'Content', 'Prices' ),
						'sc' => '[vc_row css=".vc_custom_1494253443261{padding-top: 80px !important;padding-bottom: 62px !important;}"][vc_column][special_heading title="Our Prices" subtitle="And here goes a super fancy description of the section." c_margin_bottom="55px"][vc_row_inner css=".vc_custom_1494253419308{padding-top: 10px !important;}"][vc_column_inner width="1/4"][pricing_box title="Minimal" price="$29" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!"][/vc_column_inner][vc_column_inner width="1/4"][pricing_box title="Simple" price="$44" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!"][/vc_column_inner][vc_column_inner width="1/4"][pricing_box title="Classic" price="$49" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" featured="yes"][/vc_column_inner][vc_column_inner width="1/4"][pricing_box title="Premium" price="$69" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" style="minimal"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'prices_3' => array(
						'title' => __( 'Prices Light 3 Cols', 'engage' ),
						'desc' => __( 'Three columns of white pricing boxes.', 'engage' ), 
						'cat' => array( 'Content', 'Prices' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1494257863978{padding-top: 80px !important;padding-bottom: 62px !important;}"][vc_column][special_heading title="Our Prices" subtitle="And here goes a super fancy description of the section." c_margin_bottom="55px"][vc_row_inner css=".vc_custom_1494253419308{padding-top: 10px !important;}"][vc_column_inner width="1/3"][pricing_box title="Simple" price="$39" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" bg="white"][/vc_column_inner][vc_column_inner width="1/3"][pricing_box title="Classic" price="$49" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" featured="yes" bg="white"][/vc_column_inner][vc_column_inner width="1/3"][pricing_box title="Premium" price="$69" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" bg="white"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'prices_3_img' => array(
						'title' => __( 'Prices 3 Cols Image', 'engage' ),
						'desc' => __( 'Pricing boxes on image background.', 'engage' ), 
						'cat' => array( 'Content', 'Prices' ),
						'sc' => '[vc_row parallax="content-moving" color_scheme="white" bg_overlay="dark30" css=".vc_custom_1494257847327{padding-top: 90px !important;padding-bottom: 72px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-14.jpg) !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][special_heading title="Our Prices" subtitle="And here goes a super fancy description of the section." c_margin_bottom="55px"][vc_row_inner css=".vc_custom_1494253419308{padding-top: 10px !important;}"][vc_column_inner width="1/3"][pricing_box title="Simple" price="$39" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" bg="transparent_dark"][/vc_column_inner][vc_column_inner width="1/3"][pricing_box title="Classic" price="$49" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" featured="yes" bg="transparent_dark"][/vc_column_inner][vc_column_inner width="1/3"][pricing_box title="Premium" price="$69" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" bg="transparent_dark"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'prices_gd' => array(
						'title' => __( 'Prices 3 Cols Gradient', 'engage' ),
						'desc' => __( 'Pricing boxes on gradient background.', 'engage' ), 
						'cat' => array( 'Content', 'Prices' ),
						'sc' => '[vc_row parallax="content-moving" color_scheme="white" bg_gradient1="#b06ab3" bg_gradient2="#4568dc" css=".vc_custom_1494257955511{padding-top: 90px !important;padding-bottom: 72px !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][special_heading title="Our Prices" subtitle="And here goes a super fancy description of the section." c_margin_bottom="55px"][vc_row_inner css=".vc_custom_1494253419308{padding-top: 10px !important;}"][vc_column_inner width="1/3"][pricing_box title="Simple" price="$39" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" bg="transparent_light"][/vc_column_inner][vc_column_inner width="1/3"][pricing_box title="Classic" price="$49" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" featured="yes" bg="transparent_light"][/vc_column_inner][vc_column_inner width="1/3"][pricing_box title="Premium" price="$69" features="Awesome feature.,Incredible support.,Another stuff.,Free bonus!" add_icon="true" icon_fontawesome="fa fa-angle-right" bg="transparent_light"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'prices_3x3_border' => array(
						'title' => __( 'Prices Plain 1', 'engage' ),
						'desc' => __( 'Simple price grid.', 'engage' ), 
						'cat' => array( 'Prices' ),
						'sc' => '[vc_row css=".vc_custom_1495542037394{padding-top: 60px !important;padding-bottom: 30px !important;}"][vc_column][special_heading title="Our Prices" subtitle="List of our great services along with prices." c_margin_bottom="60px"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Great Service" label="$15" border="yes"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Beef Delicious" label="$12" border="yes"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Short Cut" label="$24" border="yes" label_fw="normal"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Quick Dig" label="$13" border="yes"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="King Size" label="$29" border="yes"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Enormous Bite" label="$32" border="yes" label_fw="normal" label_color="dark-grey"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Short Stuff" label="$9" border="yes"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Premium Type" label="$36" border="yes"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Luxurious Offer" label="$52" border="yes" label_fw="normal" label_color="dark-grey"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'prices_3x3_accent' => array(
						'title' => __( 'Prices Plain Accent', 'engage' ),
						'desc' => __( 'Simple price grid, accent label.', 'engage' ), 
						'cat' => array( 'Prices' ),
						'sc' => '[vc_row css=".vc_custom_1495542037394{padding-top: 60px !important;padding-bottom: 30px !important;}"][vc_column][special_heading title="Our Prices" subtitle="List of our great services along with prices." c_margin_bottom="60px"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Great Service" label="$15" border="yes" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Beef Delicious" label="$12" border="yes" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Short Cut" label="$24" border="yes" label_fw="normal" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Quick Dig" label="$13" border="yes" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="King Size" label="$29" border="yes" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Enormous Bite" label="$32" border="yes" label_fw="normal" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Short Stuff" label="$9" border="yes" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Premium Type" label="$36" border="yes" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Luxurious Offer" label="$52" border="yes" label_fw="normal" label_color="accent"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'prices_3x3' => array(
						'title' => __( 'Prices Plain No Border', 'engage' ),
						'desc' => __( 'Simple price grid, no border.', 'engage' ), 
						'cat' => array( 'Prices' ),
						'sc' => '[vc_row css=".vc_custom_1495542037394{padding-top: 60px !important;padding-bottom: 30px !important;}"][vc_column][special_heading title="Our Services" subtitle="List of our great services along with prices." c_margin_bottom="60px"][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Great Service" label="$15"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Beef Delicious" label="$12"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Short Cut" label="$24" label_fw="normal"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Quick Dig" label="$13"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="King Size" label="$29"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Enormous Bite" label="$32" label_fw="normal"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner css=".vc_custom_1493381079225{padding-bottom: 30px !important;}"][vc_column_inner width="1/3"][price_heading title="Short Stuff" label="$9"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Premium Type" label="$36"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][price_heading title="Luxurious Offer" label="$52" label_fw="normal"][vc_column_text]Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scale.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'testimo_center_light' => array(
						'title' => __( 'Testimonials Center Light', 'engage' ),
						'desc' => __( 'Centered testimonials, light version.', 'engage' ), 
						'cat' => array( 'Content', 'Clients' ),
						'sc' => '[vc_row css=".vc_custom_1494262026330{padding-top: 100px !important;padding-bottom: 75px !important;}"][vc_column][vc_icon type="linecons" icon_linecons="vc_li vc_li-bubble" color="vista_blue" size="lg" align="center"][vntd_testimonials posts_nr="5" bullet_nav="" arrow_nav="true"][/vc_column][/vc_row]',
					),
					'testimo_center_accent' => array(
						'title' => __( 'Testimonials Center Accent', 'engage' ),
						'desc' => __( 'Centered testimonials, accent background.', 'engage' ), 
						'cat' => array( 'Content', 'Clients' ),
						'sc' => '[vc_row color_scheme="white" bg_color_pre="bg-color-accent" css=".vc_custom_1494262034487{padding-top: 100px !important;padding-bottom: 75px !important;}"][vc_column][vc_icon type="linecons" icon_linecons="vc_li vc_li-bubble" color="white" size="lg" align="center"][vntd_testimonials posts_nr="5" bullet_nav="" arrow_nav="true"][/vc_column][/vc_row]',
					),
					'testimo_center_accent' => array(
						'title' => __( 'Testimonials Center Accent', 'engage' ),
						'desc' => __( 'Centered testimonials, accent background.', 'engage' ), 
						'cat' => array( 'Content', 'Clients' ),
						'sc' => '[vc_row color_scheme="white" bg_color_pre="bg-color-accent" css=".vc_custom_1494262034487{padding-top: 100px !important;padding-bottom: 75px !important;}"][vc_column][vc_icon type="linecons" icon_linecons="vc_li vc_li-bubble" color="white" size="lg" align="center"][vntd_testimonials posts_nr="5" bullet_nav="" arrow_nav="true"][/vc_column][/vc_row]',
					),
					'testimo_center_img' => array(
						'title' => __( 'Testimonials Center Image', 'engage' ),
						'desc' => __( 'Centered testimonials, image background.', 'engage' ), 
						'cat' => array( 'Content', 'Clients' ),
						'sc' => '[vc_row parallax="content-moving" color_scheme="white" bg_overlay="dark30" css=".vc_custom_1494262415344{padding-top: 100px !important;padding-bottom: 75px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-18.jpg) !important;}"][vc_column][vc_icon type="linecons" icon_linecons="vc_li vc_li-bubble" color="white" size="lg" align="center"][vntd_testimonials posts_nr="5" bullet_nav="" arrow_nav="true"][/vc_column][/vc_row]',
					),
					'testimo_light' => array(
						'title' => __( 'Testimonials Light', 'engage' ),
						'desc' => __( 'Testimonials, light version.', 'engage' ), 
						'cat' => array( 'Content', 'Clients' ),
						'sc' => '[vc_row css=".vc_custom_1494262501239{padding-top: 80px !important;padding-bottom: 75px !important;}"][vc_column][special_heading title="Testimonials" subtitle="What our awesome clients say about us." c_margin_bottom="55px"][vntd_testimonials style="simple" cols="3" posts_nr="5"][/vc_column][/vc_row]',
					),
					'testimo_accent_img' => array(
						'title' => __( 'Testimonials Accent Image', 'engage' ),
						'desc' => __( 'Testimonials, accent image background.', 'engage' ), 
						'cat' => array( 'Content', 'Clients' ),
						'sc' => '[vc_row color_scheme="white" bg_overlay="accent" css=".vc_custom_1494262668971{padding-top: 90px !important;padding-bottom: 75px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-18.jpg) !important;}"][vc_column][special_heading title="Testimonials" subtitle="What our awesome clients say about us." c_margin_bottom="55px"][vntd_testimonials style="simple" bg="white" cols="3" posts_nr="5"][/vc_column][/vc_row]',
					),
					'testimo_img' => array(
						'title' => __( 'Testimonials Image', 'engage' ),
						'desc' => __( 'Testimonials, image background.', 'engage' ), 
						'cat' => array( 'Content', 'Clients' ),
						//'info' => 'This sections uses custom post types. Make sure that you have some testimonials created under <a href="#" target="_blank">Testimonials</a> menu.',
						'sc' => '[vc_row color_scheme="white" bg_color_pre="bg-color-accent" bg_overlay="dark40" css=".vc_custom_1494262647231{padding-top: 90px !important;padding-bottom: 75px !important;background-image: url(http://media.veented.com/wp-content/uploads/2017/09/full-14.jpg) !important;}"][vc_column][special_heading title="Testimonials" subtitle="What our awesome clients say about us." c_margin_bottom="55px"][vntd_testimonials style="simple" bg="transparent" cols="2" posts_nr="5"][/vc_column][/vc_row]',
					),
					'team_boxed_desc' => array(
						'title' => __( 'Team Members Description', 'engage' ),
						'desc' => __( 'Team Members with a description.', 'engage' ), 
						'cat' => array( 'Content', 'Team Members' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1495446959270{padding-top: 75px !important;padding-bottom: 60px !important;}"][vc_column][special_heading title="Our Team" subtitle="Learn more about our fantastic team!" c_margin_bottom="60px"][vc_row_inner][vc_column_inner width="1/3"][vntd_person name="John Doe" position="Web Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/man-5.jpg" bio="Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper accumsan lectus, a tempor turpis interdum sed. Donec ac faucibus nunc." style="classic" boxed="boxed-border" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/3"][vntd_person name="Alice Doen" position="Web Developer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/woman-4.jpg" bio="Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper accumsan lectus, a tempor turpis interdum sed. Donec ac faucibus nunc." style="classic" boxed="boxed-border" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/3"][vntd_person name="Katie Holmes" position="Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/woman-5.jpg" bio="Ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies nisi at scelerisque pellentesque. Nunc feugiat felis vitae aliquet consequat. Aliquam ullamcorper accumsan lectus, a tempor turpis interdum sed. Donec ac faucibus nunc." style="classic" boxed="boxed-border" twitter="#" facebook="#"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'team_boxed' => array(
						'title' => __( 'Team Members Boxed', 'engage' ),
						'desc' => __( 'Team Members boxed version.', 'engage' ), 
						'cat' => array( 'Content', 'Team Members' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1495447264274{padding-top: 75px !important;padding-bottom: 40px !important;}"][vc_column][special_heading title="Our Team" subtitle="Learn more about our fantastic team!" c_margin_bottom="60px"][vc_row_inner css=".vc_custom_1495447259772{padding-bottom: 20px !important;}"][vc_column_inner width="1/4"][vntd_person name="John Doe" position="Web Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/man-5.jpg" boxed="boxed-solid" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/4"][vntd_person name="Alice Doen" position="Web Developer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/woman-4.jpg" boxed="boxed-solid" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/4"][vntd_person name="Bernard Smith" position="Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/man-4.jpg" boxed="boxed-solid" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/4"][vntd_person name="Katie Holmes" position="Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/woman-5.jpg" boxed="boxed-solid" twitter="#" facebook="#"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'team_min' => array(
						'title' => __( 'Team Members Minimal', 'engage' ),
						'desc' => __( 'Team Members boxed minimal version.', 'engage' ), 
						'cat' => array( 'Content', 'Team Members' ),
						'sc' => '[vc_row css=".vc_custom_1495446935969{padding-top: 75px !important;padding-bottom: 30px !important;}"][vc_column][special_heading title="Our Team" subtitle="Learn more about our fantastic team!" c_margin_bottom="60px"][vc_row_inner][vc_column_inner width="1/3"][vntd_person name="John Doe" position="Web Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/man-5.jpg" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/3"][vntd_person name="Alice Doen" position="Web Developer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/woman-4.jpg" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/3"][vntd_person name="Katie Holmes" position="Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/woman-5.jpg" twitter="#" facebook="#"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'team_1x4_min' => array(
						'title' => __( 'Team Members Minimal 2', 'engage' ),
						'desc' => __( 'Team Members minimal version.', 'engage' ), 
						'cat' => array( 'Content', 'Team Members' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1495447264274{padding-top: 75px !important;padding-bottom: 40px !important;}"][vc_column][special_heading title="Our Team" subtitle="Learn more about our fantastic team!" c_margin_bottom="60px"][vc_row_inner css=".vc_custom_1495447259772{padding-bottom: 20px !important;}"][vc_column_inner width="1/4"][vntd_person name="John Doe" position="Web Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/man-5.jpg" boxed="boxed-solid" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/4"][vntd_person name="Alice Doen" position="Web Developer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/woman-4.jpg" boxed="boxed-solid" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/4"][vntd_person name="Bernard Smith" position="Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/man-4.jpg" boxed="boxed-solid" twitter="#" facebook="#"][/vc_column_inner][vc_column_inner width="1/4"][vntd_person name="Katie Holmes" position="Designer" img="https://s3.us-east-2.amazonaws.com/engage.veented.com/img/team/woman-5.jpg" boxed="boxed-solid" twitter="#" facebook="#"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
					),
					'blog_border' => array(
						'title' => __( 'Blog Carousel 1', 'engage' ),
						'desc' => __( 'Blog carousel with dots navigation.', 'engage' ), 
						'cat' => array( 'Content', 'Carousel' ),
						'sc' => '[vc_row css=".vc_custom_1494328488142{padding-top: 65px !important;padding-bottom: 70px !important;}"][vc_column][special_heading title="Latest News" c_margin_bottom="60px"][blog_carousel][/vc_column][/vc_row]',
					),
					'blog_btn' => array(
						'title' => __( 'Blog Carousel 2', 'engage' ),
						'desc' => __( 'Blog carousel with a button.', 'engage' ), 
						'cat' => array( 'Content', 'Carousel' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1494328557833{padding-top: 65px !important;padding-bottom: 70px !important;}"][vc_column][special_heading title="Latest News" c_margin_bottom="55px"][blog_carousel style="boxed_solid" bullet_nav="" autoplay_timeout="8000"][vntd_button label="Visit our Blog" align="center" icon_enabled="yes" icon_fontawesome="fa fa-long-arrow-right" icon_style="right_side" margin_top="55px"][/vc_column][/vc_row]',
					),
					'blog_no_img' => array(
						'title' => __( 'Blog Carousel 3', 'engage' ),
						'desc' => __( 'Blog carousel without image.', 'engage' ), 
						'cat' => array( 'Content', 'Carousel' ),
						'sc' => '[vc_row bg_color_pre="bg-color-1" css=".vc_custom_1494328557833{padding-top: 65px !important;padding-bottom: 70px !important;}"][vc_column][special_heading title="Latest News" c_margin_bottom="55px"][blog_carousel style="boxed_solid" thumb="disable" bullet_nav="" autoplay_timeout="8000"][vntd_button label="Visit our Blog" align="center" icon_enabled="yes" icon_fontawesome="fa fa-long-arrow-right" icon_style="right_side" margin_top="55px"][/vc_column][/vc_row]',
					),
				);
				
				$templates_output = '';
				$lib_path = 'img/';
				$categories = array();
				$templates_count = 0;
				
				foreach( $templates as $template_id => $template ) {
				
					$img_path = plugins_url( $lib_path . $template_id . '_t.jpg', __FILE__ );
					$img_preview_path = plugins_url( 'img-full/' . $template_id . '.jpg', __FILE__ );
					
					$cats = '';
					
					foreach ( $template['cat'] as $cat ) {
						if ( $cat == 'Content' ) continue;
						$slug = strtolower( str_replace( '/', '_', str_replace( '+', '_', str_replace( ' ', '', $cat ) ) ) );
						$cats .= ' cat-' . $slug;
						if ( array_key_exists( $cat, $categories ) ) {
							$categories[ $cat ][ 'count' ] = $categories[ $cat ][ 'count' ] = $categories[ $cat ][ 'count' ] + 1;
						} else { // New cat
							$categories[ $cat ] = array( 
								'slug' => $slug,
								'count' => 1
							);
						}
						
					}
					
					$templates_output .= '<li class="vntd-template' . $cats . '" id="' . $template_id . '"><div class="vntd-template-inner">';
					$templates_output .= '<div class="vntd-template-img" data-preview-img="' . $img_preview_path . '"><img src="" data-img-src="' . $img_path . '" alt><span class="template-preview-btn"><i class="fa fa-search"></i></span></div>'; 
					$templates_output .= '<div class="vntd-template-text"><h5>' . esc_html( $template['title'] ) . '</h5>';
					if ( array_key_exists( 'desc', $template ) ) $templates_output .= '<p class="template-desc">' . esc_html( $template['desc'] ) . '</p>';
					if ( array_key_exists( 'info', $template ) ) $templates_output .= '<p class="template-info">' .$template['info'] . '</p>';
					$templates_output .= '<button class="vntd-load-template">' . esc_html__( 'Load template', 'engage' ) . '</button></div><span class="hidden vntd-template-sc">' . $template['sc'] . '</span></div></li>';
					$templates_count++;
				}
				
				$category['output'] .= '<div id="vntd-templates" class="vntd-templates"><div id="vntd-templates-main"><div class="vntd-templates-main-header"><h3>' . __( 'Engage Templates', 'engage' ) . '</h3><p>' . __( 'Save your time with Engage Templates! This is where you may load predefined, perfectly styled sections directly to your page.', 'engage' ) . '</p></div>';
				
				// Filtering menu
				
				$filtering_menu = '<div id="vntd-templates-filter" class="vntd-templates-filter"><ul>';
				
				
				$filtering_menu .= '<li class="show-all vntd-active"><button data-filter="all">' . __( 'Show All', 'engage' ) . ' <span>' . $templates_count . '</span></button></li>';
				
				$filters = array( 'Content', 'Icon Boxes' );
				
				foreach ( $categories as $name => $cat ) {
					$filtering_menu .= '<li><button data-filter="' . $cat[ 'slug' ] . '">' . esc_html( $name ) . '<span>' . $cat[ 'count' ] . '</span></a></li>';
				}
				
				$filtering_menu .= '</ul></div>';
				
				$category['output'] .= $filtering_menu;
				
				$category['output'] .= '<div class="vntd-templates-list-holder"><ul class="vntd-templates-list">';
				
				$category['output'] .= $templates_output;
				
				$category['output'] .= '</ul><div class="clear"></div></div></div>'; // End #vntd-templates-main				
				
				// Preview
				
				$template_preview = '<div id="vntd-template-preview" class="vntd-template-preview" data-template-id=""><div class="vntd-preview-header"><span class="vntd-preview-back"><i class="fa fa-long-arrow-left"></i> ' . esc_html__( 'Back to templates', 'engage' ) . '</span><span class="vntd-template-close"><i class="fa fa-close"></i></span>';
				
				$template_preview .= '<div class="vntd-templates-nav"><div class="vntd-template-prev"><i class="fa fa-angle-left"></i></div><div class="vntd-template-next"><i class="fa fa-angle-right"></i></div></div>';
				
				$template_preview .= '</div>';
				
				$template_preview .= '<div class="vntd-preview-content"><h3 id="template-preview-title">Template Preview</h3><p id="template-preview-desc">Description</p><p id="template-preview-info"><strong>Note:</strong> <span></span></p><div class="template-preview-img"><img src="" id="template-preview-img"></div><button id="template-preview-load" class="load-from-preview">' . esc_html__( 'Load template', 'engage' ) . '</button>';
				
				//$template_preview .= '<div class="vntd-templates-nav"><div class="vntd-template-prev"><i class="fa fa-angle-left"></i></div><div class="vntd-template-next"><i class="fa fa-angle-right"></i></div></div>';
				
				$template_preview .= '</div>';
			
				$template_preview .= '</div>';
				
				$category['output'] .= $template_preview;
				
				$category['output'] .= '</div></div>';
				
			}

			return $category;
		}

		/**
		 * Hook templates panel window rendering, if template type is templatera_templates render it
		 * @since 4.4
		 *
		 * @param $template_name
		 * @param $template_data
		 *
		 * @return string
		 */
		public function renderTemplateWindow( $template_name, $template_data ) {
			if ( self::$template_type === $template_data['type'] ) {
				return $this->renderTemplateWindowTemplateraTemplates( $template_name, $template_data );
			}

			return $template_name;
		}

		/**
		 * Rendering templatera template for panel window
		 * @since 4.4
		 *
		 * @param $template_name
		 * @param $template_data
		 *
		 * @return string
		 */
		public function renderTemplateWindowTemplateraTemplates( $template_name, $template_data ) {
			ob_start();
			if ( $this->isNewVcVersion( self::$vcWithTemplatePreview ) ) {
				$template_id = esc_attr( $template_data['unique_id'] );
				$template_id_hash = md5( $template_id ); // needed for jquery target for TTA
				$template_name = esc_html( $template_name );
				$delete_template_title = esc_attr( 'Delete template', 'templatera' );
				$preview_template_title = esc_attr( 'Preview template', 'templatera' );
				$add_template_title = esc_attr( 'Add template', 'templatera' );
				$edit_template_title = esc_attr( 'Edit template', 'templatera' );
				$template_url = esc_attr( admin_url( 'post.php?post=' . $template_data['unique_id'] . '&action=edit' ) );
				$edit_tr_html = '';
				if ( ! $this->isUserRoleAccessVcVersion() || ( $this->isUserRoleAccessVcVersion() && vc_user_access()->part( 'templates' )->checkStateAny( true, null )->get() ) ) {
					$edit_tr_html = <<<EDTR
				<a href="$template_url"  class="vc_general vc_ui-control-button" title="$edit_template_title" target="_blank">
					<i class="vc_ui-icon-pixel vc_ui-icon-pixel-control-edit-dark"></i>
				</a>
				<button type="button" class="vc_general vc_ui-control-button" data-vc-ui-delete="template-title" title="$delete_template_title">
					<i class="vc_ui-icon-pixel vc_ui-icon-pixel-control-trash-dark"></i>
				</button>
EDTR;
				}

				echo <<<HTML
			<button type="button" class="vc_ui-list-bar-item-trigger" title="$add_template_title"
					 	data-template-handler=""
						data-vc-ui-element="template-title">$template_name</button>
			<div class="vc_ui-list-bar-item-actions">
				<button type="button" class="vc_general vc_ui-control-button" title="$add_template_title"
					 	data-template-handler="">
					<i class="vc_ui-icon-pixel vc_ui-icon-pixel-control-add-dark"></i>
				</button>$edit_tr_html
				<button type="button" class="vc_general vc_ui-control-button" title="$preview_template_title"
					data-vc-container=".vc_ui-list-bar" data-vc-preview-handler data-vc-target="[data-template_id_hash=$template_id_hash]">
					<i class="vc_ui-icon-pixel vc_ui-preview-icon"></i>
				</button>
			</div>
HTML;
			} else {
				?>
				<div class="vc_template-wrapper vc_input-group"
					data-template_id="<?php echo esc_attr( $template_data['unique_id'] ); ?>">
					<a data-template-handler="true" class="vc_template-display-title vc_form-control"
						data-vc-ui-element="template-title"
						href="javascript:;"><?php echo esc_html( $template_name ); ?></a>
					<span class="vc_input-group-btn vc_template-icon vc_template-edit-icon"
						title="<?php esc_attr_e( 'Edit template', 'templatera' ); ?>"
						data-template_id="<?php echo esc_attr( $template_data['unique_id'] ); ?>"><a
							href="<?php echo esc_attr( admin_url( 'post.php?post=' . $template_data['unique_id'] . '&action=edit' ) ); ?>"
							target="_blank" class="vc_icon"></i></a></span>
					<span class="vc_input-group-btn vc_template-icon vc_template-delete-icon"
						title="<?php esc_attr_e( 'Delete template', 'templatera' ); ?>"
						data-template_id="<?php echo esc_attr( $template_data['unique_id'] ); ?>"><i
							class="vc_icon"></i></span>
				</div>
				<?php
			}

			return ob_get_clean();
		}

		/**
		 * Function used to replace old my templates with new templatera templates
		 * @since 4.4
		 *
		 * @param array $data
		 *
		 * @return array
		 */
		public function replaceCustomWithTemplateraTemplates( array $data ) {
			$templatera_templates = $this->getTemplateList();
			$templatera_arr = array();
			foreach ( $templatera_templates as $template_name => $template_id ) {
				$templatera_arr[] = array(
					'unique_id' => $template_id,
					'name' => $template_name,
					'type' => 'templatera_templates',
					// for rendering in backend/frontend with ajax);
				);
			}
			

			if ( ! empty( $data ) ) {
				$found = false;
				foreach ( $data as $key => $category ) {
					if ( 'my_templates' == $category['category'] ) {
						$found = true;
						$data[ $key ]['templates'] = $templatera_arr;
					}
				}
				if ( ! $found ) {
					$data[] = array(
						'templates' => $templatera_arr,
						'category' => 'my_templates',
						'category_name' => __( 'My Templates', 'js_composer' ),
						'category_description' => __( 'Append previously saved template to the current layout', 'js_composer' ),
						'category_weight' => 10,
					);
				}
			} else {
				$data[] = array(
					'templates' => $templatera_arr,
					'category' => 'my_templates',
					'category_name' => __( 'My Templates', 'js_composer' ),
					'category_description' => __( 'Append previously saved template to the current layout', 'js_composer' ),
					'category_weight' => 10,
				);
			}
			
			$data[] = array(
				'templates' => $templatera_arr,
				'category' => 'engage_templates',
				'category_name' => __( 'Engage Templates', 'js_composer' ),
				'category_description' => __( 'Append previously saved template to the current layout', 'js_composer' ),
				'category_weight' => 10,
			);

			return $data;
		}

		/**
		 * Maps Frozen row shortcode
		 */
		function createShortcode() {
			vc_map( array(
				'name' => __( 'Templatera', 'templatera' ),
				'base' => 'templatera',
				'icon' => 'icon-templatera',
				'category' => __( 'Content', 'templatera' ),
				'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => __( 'Select template', 'templatera' ),
						'param_name' => 'id',
						'value' => array( __( 'Choose template', 'js_composer' ) => '' ) + $this->getTemplateList(),
						'description' => __( 'Choose which template to load for this location.', 'templatera' ),
					),
					array(
						'type' => 'textfield',
						'heading' => __( 'Extra class name', 'templatera' ),
						'param_name' => 'el_class',
						'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'templatera' ),
					),
				),
				'js_view' => 'VcTemplatera',
			) );
			add_shortcode( 'templatera', array(
				$this,
				'outputShortcode',
			) );
		}

		/**
		 * Frozen row shortcode hook.
		 *
		 * @param $atts
		 * @param string $content
		 *
		 * @return string
		 */
		public function outputShortcode( $atts, $content = '' ) {
			$id = $el_class = $output = '';
			extract( shortcode_atts( array(
				'el_class' => '',
				'id' => '',
			), $atts ) );
			if ( empty( $id ) ) {
				return $output;
			}
			$my_query = new WP_Query( array(
				'post_type' => self::postType(),
				'p' => (int) $id,
			) );
			WPBMap::addAllMappedShortcodes();
			while ( $my_query->have_posts() ) {
				$my_query->the_post();
				if ( get_the_ID() === (int) $id ) {
					$output .= '<div class="templatera_shortcode' . ( $el_class ? ' ' . $el_class : '' ) . '">';
					ob_start();
					visual_composer()->addFrontCss();
					$content = get_the_content();
					echo $content;
					$output .= ob_get_clean();
					$output .= '</div>';
					$output = do_shortcode( $output );
				}
				wp_reset_postdata();
			}
			wp_enqueue_style( 'templatera_inline', $this->assetUrl( 'css/front_style.css' ), false, '2.1' );

			return $output;
		}

		/**
		 * Create meta box for self::$post_type, with template settings
		 */
		public function createMetaBox() {
			add_meta_box( 'vc_template_settings_metabox', __( 'Template Settings', 'templatera' ), array(
				$this,
				'sideOutput',
			), self::postType(), 'side', 'high' );
		}

		/**
		 * Used in meta box VcTemplateManager::createMetaBox
		 */
		public function sideOutput() {
			$data = get_post_meta( get_the_ID(), self::$meta_data_name, true );
			$data_post_types = isset( $data['post_type'] ) ? $data['post_type'] : array();
			$post_types = get_post_types( array( 'public' => true ) );
			echo '<div class="misc-pub-section">
            <div class="templatera_title"><b>' . __( 'Post types', 'templatera' ) . '</b></div>
            <div class="input-append">
                ';
			foreach ( $post_types as $type ) {
				if ( 'attachment' !== $type && ! $this->isSamePostType( $type ) ) {
					echo '<label><input type="checkbox" name="' . esc_attr( self::$meta_data_name ) . '[post_type][]" value="' . esc_attr( $type ) . '" ' . ( in_array( $type, $data_post_types ) ? ' checked="true"' : '' ) . '>' . ucfirst( $type ) . '</label><br/>';
				}
			}
			echo '</div><p>' . __( 'Select for which post types this template should be available. Default: Available for all post types.', 'templatera' ) . '</p></div>';
			$groups = get_editable_roles();
			$data_user_role = isset( $data['user_role'] ) ? $data['user_role'] : array();
			echo '<div class="misc-pub-section vc_user_role">
            <div class="templatera_title"><b>' . __( 'Roles', 'templatera' ) . '</b></div>
            <div class="input-append">
                ';
			foreach ( $groups as $key => $g ) {
				echo '<label><input type="checkbox" name="' . self::$meta_data_name . '[user_role][]" value="' . esc_attr( $key ) . '" ' . ( in_array( $key, $data_user_role ) ? ' checked="true"' : '' ) . '> ' . $g['name'] . '</label><br/>';
			}
			echo '</div><p>' . __( 'Select for user roles this template should be available. Default: Available for all user roles.', 'templatera' ) . '</p></div>';
		}

		/**
		 * Url to js/css or image assets of plugin
		 *
		 * @param $file
		 *
		 * @return string
		 */
		public function assetUrl( $file ) {
			return plugins_url( $this->plugin_dir . '/assets/' . $file, plugin_dir_path( dirname( __FILE__ ) ) );
		}

		/**
		 * Absolute path to assets files
		 *
		 * @param $file
		 *
		 * @return string
		 */
		public function assetPath( $file ) {
			return $this->dir . '/assets/' . $file;
		}

		/**
		 * @return bool
		 */
		public function isValidPostType() {
			$type = get_post_type();
			$post = ( isset( $_GET['post'] ) && $this->compareType( get_post_type( $_GET['post'] ) ) );
			$post_type = ( isset( $_GET['post_type'] ) && $this->compareType( $_GET['post_type'] ) );
			$post_type_id = ( isset( $_GET['post_id'] ) && $this->compareType( get_post_type( (int) $_GET['post_id'] ) ) );
			$post_vc_type_id = ( isset( $_GET['vc_post_id'] ) && $this->compareType( get_post_type( (int) $_GET['vc_post_id'] ) ) );

			return ( ( $type && $this->compareType( $type ) ) || ( $post ) || ( $post_type ) || ( $post_type_id ) || ( $post_vc_type_id ) );
		}

		/**
		 * @param $type
		 * @return bool
		 */
		public function compareType( $type ) {
			return in_array( $type, array_merge( vc_editor_post_types(), array( 'templatera' ) ) );
		}

		/**
		 * Load required js and css files
		 */
		public function assets() {
			if ( $this->isValidPostType() ) {
				$this->assetsFe();
				$this->assetsBe();
			}
		}

		/**
		 *
		 */
		public function assetsFe() {
			if ( $this->isValidPostType() && ( vc_user_access()->part( 'frontend_editor' )->can()->get() ) ) {
				$this->addGridScripts();
				$dependency = array( 'vc-frontend-editor-min-js' );
				wp_register_script( 'vc_plugin_inline_templates', $this->assetUrl( 'js/templates_panels.js' ), $dependency, WPB_VC_VERSION, true );
				wp_register_script( 'vc_plugin_templates', $this->assetUrl( 'js/templates.js' ), array(), time(), true );
				wp_localize_script( 'vc_plugin_templates', 'VcTemplateI18nLocale', array(
					'please_enter_templates_name' => __( 'Please enter template name', 'templatera' ),
				) );
				wp_register_style( 'vc_plugin_template_css', $this->assetUrl( 'css/style.css' ), false, '1.1.0' );
				wp_enqueue_style( 'vc_plugin_template_css' );
				$this->addTemplateraJs();
			}
		}

		/**
		 *
		 */
		public function assetsBe() {
			if ( $this->isValidPostType() && ( vc_user_access()->part( 'backend_editor' )->can()->get() || $this->isSamePostType() ) ) {
				$this->addGridScripts();
				$dependency = array( 'vc-backend-min-js' );
				wp_register_script( 'vc_plugin_inline_templates', $this->assetUrl( 'js/templates_panels.js' ), $dependency, WPB_VC_VERSION, true );
				wp_register_script( 'vc_plugin_templates', $this->assetUrl( 'js/templates.js' ), array(), time(), true );
				wp_localize_script( 'vc_plugin_templates', 'VcTemplateI18nLocale', array(
					'please_enter_templates_name' => __( 'Please enter template name', 'templatera' ),
				) );
				wp_register_style( 'vc_plugin_template_css', $this->assetUrl( 'css/style.css' ), false, '1.1.0' );
				wp_enqueue_style( 'vc_plugin_template_css' );
				$this->addTemplateraJs();
			}
		}

		/**
		 * @return bool|false|string
		 */
		public function getPostType() {
			if ( $this->current_post_type ) {
				return $this->current_post_type;
			}
			$post_type = false;
			if ( isset( $_GET['post'] ) ) {
				$post_type = get_post_type( $_GET['post'] );
			} else if ( isset( $_GET['post_type'] ) ) {
				$post_type = $_GET['post_type'];
			}
			$this->current_post_type = $post_type;

			return $this->current_post_type;
		}

		/**
		 * Create templates button on navigation bar of the Front/Backend editor.
		 *
		 * @param $buttons
		 *
		 * @return array
		 */
		public function createButtonFrontBack( $buttons ) {
			if ( $this->isUserRoleAccessVcVersion() && ! vc_user_access()->part( 'templates' )->can()->get() ) {
				return $buttons;
			}
			if ( 'vc_grid_item' === $this->getPostType() ) {
				return $buttons;
			}

			$new_buttons = array();

			foreach ( $buttons as $button ) {
				if ( 'templates' !== $button[0] ) {
					// disable custom css as well but only in templatera page
					if ( ! $this->isSamePostType() || ( $this->isSamePostType() && 'custom_css' !== $button[0] ) ) {
						$new_buttons[] = $button;
					}
				} else {
					if ( $this->isPanelVcVersion() ) {
						// @since 4.4 button is available but "Save" Functionality in form is disabled in templatera post.
						$new_buttons[] = array(
							'custom_templates',
							'<li class="vc_add_templatera"><a href="#" class="vc_icon-btn"  id="vc-templatera-editor-button" title="' . __( 'Templates', 'js_composer' ) . '"><i class="vc-composer-icon vc-c-icon-add_template"></i></a></li>',
						);
					} else {
						if ( $this->isSamePostType() ) {
							// in older version we doesn't need to display templates window in templatera post
						} else {
							$new_buttons[] = array(
								'custom_templates',
								'<li class="vc_add_templatera"><a href="#" class="vc_icon-btn" id="vc-templatera-editor-button" title="' . __( 'Templates', 'js_composer' ) . '"><i class="vc-composer-icon vc-c-icon-add_template"></i></a></li>',
							);
						}
					}
				}
			}

			return $new_buttons;
		}

		/**
		 * Add javascript to extend functionality of templates editor panel or new panel(since 4.4)
		 */
		public function addEditorTemplates() {
			return;
		}

		/**
		 * Used to add js in backend/frontend to init template UI functionality
		 */
		public function addTemplateraJs() {
			wp_enqueue_script( 'vc_plugin_inline_templates' );
			wp_enqueue_script( 'vc_plugin_templates' );
		}

		/**
		 * Used to save new template from ajax request in new panel window
		 * @since 4.4
		 *
		 */
		public function saveTemplate() {
			if ( ! vc_verify_admin_nonce() || ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) ) {
				die();
			}
			$title = vc_post_param( 'template_name' );
			$content = vc_post_param( 'template' );
			$template_id = $this->create( $title, $content );
			$template_title = get_the_title( $template_id );
			if ( $this->isNewVcVersion( self::$vcWithTemplatePreview ) ) {
				echo visual_composer()->templatesPanelEditor()->renderTemplateListItem( array(
					'name' => $template_title,
					'unique_id' => $template_id,
					'type' => self::$template_type,
				) );
			} else {
				echo $this->renderTemplateWindowTemplateraTemplates( $template_title, array( 'unique_id' => $template_id ) );
			}
			die();
		}

		/**
		 * Gets list of existing templates. Checks access rules defined by template author.
		 * @return array
		 */
		protected function getTemplateList() {
			global $current_user;
			wp_get_current_user();
			$current_user_role = isset( $current_user->roles[0] ) ? $current_user->roles[0] : false;
			$list = array();
			$templates = get_posts( array(
				'post_type' => self::postType(),
				'numberposts' => - 1,
			) );
			$post = get_post( isset( $_POST['post_id'] ) ? $_POST['post_id'] : null );
			foreach ( $templates as $template ) {
				$id = $template->ID;
				$meta_data = get_post_meta( $id, self::$meta_data_name, true );
				$post_types = isset( $meta_data['post_type'] ) ? $meta_data['post_type'] : false;
				$user_roles = isset( $meta_data['user_role'] ) ? $meta_data['user_role'] : false;
				if ( ( ! $post || ! $post_types || in_array( $post->post_type, $post_types ) ) && ( ! $current_user_role || ! $user_roles || in_array( $current_user_role, $user_roles ) ) ) {
					$list[ $template->post_title ] = $id;
				}
			}

			return $list;
		}

		/**
		 * Creates new template.
		 * @static
		 *
		 * @param $title
		 * @param $content
		 *
		 * @return int|WP_Error
		 */
		protected static function create( $title, $content ) {
			return wp_insert_post( array(
				'post_title' => $title,
				'post_content' => $content,
				'post_status' => 'publish',
				'post_type' => self::postType(),
			) );
		}

		/**
		 * Used to delete template by template id
		 *
		 * @param int $template_id - if provided used, if not provided used vc_post_param('template_id')
		 */
		public function delete( $template_id = null ) {
			if ( ! vc_verify_admin_nonce() || ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) ) {
				die();
			}
			$post_id = $template_id ? $template_id : vc_post_param( 'template_id' );
			if ( ! is_null( $post_id ) ) {
				$post = get_post( $post_id );

				if ( ! $post || ! $this->isSamePostType( $post->post_type ) ) {
					die( 'failed to delete' );
				} else if ( wp_delete_post( $post_id ) ) {
					die( 'deleted' );
				}
			}
			die( 'failed to delete' );
		}

		/**
		 * Saves post data in databases after publishing or updating template's post.
		 *
		 * @param $post_id
		 *
		 * @return bool
		 */
		public function saveMetaBox( $post_id ) {
			if ( ! $this->isSamePostType() ) {
				return true;
			}
			if ( isset( $_POST[ self::$meta_data_name ] ) ) {
				$options = isset( $_POST[ self::$meta_data_name ] ) ? (array) $_POST[ self::$meta_data_name ] : array();
				update_post_meta( (int) $post_id, self::$meta_data_name, $options );
			} else {
				delete_post_meta( (int) $post_id, self::$meta_data_name );
			}

			return true;
		}

		/**
		 * @param $value
		 *
		 * @todo make sure we need this?
		 * @return string
		 */
		public function setJsStatusValue( $value ) {
			return $this->isSamePostType() ? 'true' : $value;
		}

		/**
		 * Used in templates.js:changeShortcodeParams
		 * @todo make sure we need this
		 * Output some template content
		 * @todo make sure it is secure?
		 */
		public function loadHtml() {
			if ( ! vc_verify_admin_nonce() || ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) ) {
				die();
			}
			$id = vc_post_param( 'id' );
			$post = get_post( (int) $id );
			if ( ! $post ) {
				die( __( 'Wrong template', 'templatera' ) );
			}
			if ( $this->isSamePostType( $post->post_type ) ) {
				echo $post->post_content;
			}
			die();
		}

		/**
		 *
		 */
		public function addGridScripts() {
			if ( $this->isSamePostType() ) {
				wp_enqueue_script( 'wpb_templatera-grid-id-param-js', $this->assetUrl( 'js/templatera-grid-id-param.js' ), array( 'wpb_js_composer_js_listeners' ), WPB_VC_REQUIRED_VERSION, true );
			}
		}

		/**
		 * @param string $type
		 * @return bool
		 */
		protected function isSamePostType( $type = '' ) {
			return $type ? self::postType() === $type : get_post_type() === self::postType();
		}
	}
}
