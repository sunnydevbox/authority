<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'authority');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(n?z&;+0 GjO<-iK(e3&7Vvox#y,^jj@QF(Wbx+;Jdz?Gv^/`+_Q#,LS?aU!4DaS');
define('SECURE_AUTH_KEY',  'P2zZ5A6{djx,9OJH`5?hQ<^(!ycTQ[(fQ<T_v2b8+iJ=1S}]cXfU.+#4+:monJ8O');
define('LOGGED_IN_KEY',    'KbWuDEOyDpZK)<!V5FFT,;$&|| vs,HDr0P U<0Yh[Zr:r8aslcsJCcx?o]Uqzn%');
define('NONCE_KEY',        'AWM,!a,9mpm!P&S!~-}()Y}Agk4,n]$tb=@)Ni3bPvB+Y]B:N8>8PVZL&1U*&/Hs');
define('AUTH_SALT',        '[p?s.a$`1-Xj&?yP|=w&<ie:nLWa``x@A_qis^dy$J+Pm{U=U4^{r8LQswD&^G$4');
define('SECURE_AUTH_SALT', 'UgHs/*ccP7:iQ Jybk_VM~S eI*nx3Yf>72;zn2:D&M;seU)N!l|;Fe[5w-8Vv:H');
define('LOGGED_IN_SALT',   '4>U7A%H(elgGyxU?HO a2.c)=Va.)iv|O~f|;YxjY?m9im%?(BU E+g_St5JV>$p');
define('NONCE_SALT',       'yTJst3W=X+=rKj0Ni&& -6@MlS%-K.%n_Jvo{1TAx6 #C,-FZ{Y9RVu)3j{$me52');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
